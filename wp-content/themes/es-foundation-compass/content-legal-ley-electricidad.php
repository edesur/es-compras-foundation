<section class="es-site-section-wrapper small-12 columns" data-equalizer>

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title"><?php the_title(); ?></h3>
    </div>
  </div>

  <div class="es-site-section-wrapper-background small-12 columns">
    <div class="row">

      <ul class="small-block-grid-2">

      <li class="text-center">
        <a href="<?php echo site_url(); ?>/wp-content/uploads/2015/07/ley-general-electricidad-modificada-ley-186-07.doc" class="th"><img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-legal-ley-general-electricidad-icon.jpg' ?>"/> </a>
        <h6><a href="<?php echo site_url(); ?>/wp-content/uploads/2015/07/ley-general-electricidad-modificada-ley-186-07.doc">Ley General de Electricidad, Modificada Ley 186-07</a></h6>
      </li>

      <li class="text-center">
        <a href="<?php echo site_url(); ?>/wp-content/uploads/2015/07/ley-general-electricidad-reglamento-de-aplicacion-modificado-por-decretos-749-02-y-494-07.doc" class="th"><img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-legal-ley-general-electricidad-reglamento-de-aplicacion-icon.jpg' ?>"/> </a>
        <h6><a href="<?php echo site_url(); ?>/wp-content/uploads/2015/07/ley-general-electricidad-reglamento-de-aplicacion-modificado-por-decretos-749-02-y-494-07.doc">Reglamento de Aplicacion de la LGE Modificado por los Decretos 749-02 y 494-07</a></h6>
      </li>

     </ul>
    </div>
  </div>

 <div class="row">
   <div class="small-12 columns">



   </div>
 </div>
</section>