<!-- Reveal Modals begin -->

<div id="es-call-center-promo" class="reveal-modal small" data-reveal>
  <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/2015-03-03-es-call-center-promo-poster.jpg" alt="">
  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="es-call-center-chat" class="reveal-modal small" data-reveal style="max-width: 490px;">
  <iframe src="http://192.168.1.29/code/webchatLogin.php" frameborder="0" width="430" height="450" style="margin: 0 auto;"></iframe>
  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="es-interrupciones-programdas" class="reveal-modal text-center  " data-reveal>
  <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/10/es-site-interrupciones-bg.jpg" alt="Interrupciones por mantenimiento - Torres de transmisión ">

  <!-- <a href="<?php echo site_url(); ?>/wp-content/uploads/2015/03/es-site-interrupciones-2015-03-27.pdf" class="button">Descargar</a> -->

  <?php
      /**
       * The WordPress Query class.
       * @link http://codex.wordpress.org/Function_Reference/WP_Query
       *
       */
      $args_interrupcion = array(
        'post_type'   => 'interrupcion',
        'post_status' => 'publish',
        'order'               => 'DESC',
        'orderby'             => 'date',
        'posts_per_page'         => 1,
      );

    $es_interrupcion_query = new WP_Query( $args_interrupcion );
  ?>

  <?php if ( $es_interrupcion_query->have_posts() ) : ?>

    <?php while ( $es_interrupcion_query->have_posts() ) : $es_interrupcion_query->the_post(); ?>

      <?php
        $args_interrupcion_files = array(
           'post_type' => 'attachment',
           'numberposts' => 1,
           'post_parent' => $post->ID
        );

        $es_interrupcion_files = get_posts( $args_interrupcion_files );
        $es_interrupcion_files_url = wp_get_attachment_url( $es_interrupcion_files[0]->ID );
        $es_interrupcion_files_img = wp_get_attachment_image( $es_interrupcion_files[0]->ID, 'full' );

        // var_dump($es_interrupcion_files[0]->guid);

      ?>

      <?php if ($es_interrupcion_files): ?>

        <!-- <a href="<?php echo $es_interrupcion_files_url; ?>" class="button" target="blank">Descargar</a> -->
      <?php endif ?>

      <?php /*the_content();*/ ?>
      <?php echo $es_interrupcion_files_img; ?>

      <?php if ($es_interrupcion_files): ?>
        <!-- <a href="<?php echo $es_interrupcion_files_url; ?>" class="button" target="blank" style="margin-top:1rem;" >Descargar</a> -->



      <?php endif ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <p><?php _e( 'No se encontraron interrupciones' ); ?></p>
  <?php endif; ?>

  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="es-puntos-de-pago" class="reveal-modal" data-reveal>
  <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/02/es-puntos-de-pago-map.jpg" alt="">
  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="es-horario-de-servcios" class="reveal-modal medium" data-reveal>


  <?php
      /**
       * The WordPress Query class.
       * @link http://codex.wordpress.org/Function_Reference/WP_Query
       *
       */
      $args_horario = array(
        'post_type'   => 'horario',
        'post_status' => 'publish',
        'order'               => 'DESC',
        'orderby'             => 'date',
        'posts_per_page'         => 1,
      );

    $es_horario_query = new WP_Query( $args_horario );
  ?>

  <?php if ( $es_horario_query->have_posts() ) : ?>

    <?php while ( $es_horario_query->have_posts() ) : $es_horario_query->the_post(); ?>

      <?php
        $args_horario_files = array(
           'post_type' => 'attachment',
           'numberposts' => 1,
           'post_parent' => $post->ID
        );

        $es_horario_files = get_posts( $args_horario_files );
        $es_horario_files_url = wp_get_attachment_url( $es_horario_files[0]->ID );
        $es_horario_files_img = wp_get_attachment_image( $es_horario_files[0]->ID,  'full' );
      ?>

      <?php if ($es_horario_files): ?>

        <?php echo $es_horario_files_img; ?>
        <a class="close-reveal-modal">&#215;</a>

      <?php else: ?>

        <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/2015-03-17-es-horario-de-servcios-comerciales.jpg" alt="Testing condition">
        <a class="close-reveal-modal">&#215;</a>


      <?php endif ?>

      <?php /*the_content();*/ ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <p><?php _e( 'No se encontraron Horarios de servicio' ); ?></p>
  <?php endif; ?>

</div>

<?php /*

  <div id="es-modal-amor-primera-mordida" class="reveal-modal small" data-reveal>
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/02/es-amor-a-primera-mordida.jpg" alt="">
    <a class="close-reveal-modal">&#215;</a>
  </div>
*/?>

<?php /*
  <div id="es-modal-promotions-banner" class="reveal-modal small" data-reveal>
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/08/es-modal-banner-horario-jumbo.jpg" alt="">
    <a class="close-reveal-modal">&#215;</a>
  </div>
*/ ?>


<?php /*
  <div id="es-modal-promotions-banner" class="reveal-modal small" data-reveal>
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/08/es-modal-banner-horario-jumbo.jpg" alt="">
    <a class="close-reveal-modal">&#215;</a>
  </div>
*/ ?>


<div id="es-modal-promotions-banner" class="reveal-modal small text-center" data-reveal>

  <?php
    /*
  <a class="es-modal-promotions-banner-no-img" href="https://www.edesur.com.do/ov/" target="_blank">
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/10/es-modal-banner-promo-ocean-world-no-img.jpg" alt="Gánate un pasadía a Ocean World al pagar por la Oficina Virtual- EDESUR Dominicana">
  </a>
  <div class="es-modal-promotions-video flex-video widescreen">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/9pxcAyBGO-Q?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
  </div>

   */
   ?>



   <?php

   // $es_modal_promotions_banner_img = '/wp-content/uploads/2015/10/es-modal-banner-promo-ocean-world-2015-10-28.jpg';

    if (is_front_page()) {
      $es_modal_promotions_banner_img = '/wp-content/uploads/2016/02/es-modal-banner-promo-brigadas-cobro-jumbo-2016-02-16.jpg';
      $es_modal_promotions_banner_img_alt = 'Brigadas de Cobro Jumbo';
    } elseif (is_post_type_archive( 'proceso_compra' )) {
      // $es_modal_promotions_banner_img = '/wp-content/uploads/2015/12/es-modal-banner-promo-compras-2015-12-08.jpg';
      // $es_modal_promotions_banner_img_alt = 'Comunicado a Proveedores';
    }

   ?>

   <?php if (is_front_page()): ?>
     <!-- <a class="es-modal-promotions-banner-thumb th" href="https://www.edesur.com.do/ov/" target="_blank"> -->
   <?php endif ?>

      <img src="<?php echo site_url() . $es_modal_promotions_banner_img; ?>" alt="<?php echo $es_modal_promotions_banner_img_alt ?>">

    <?php if (is_front_page()): ?>
     <!--  </a>
      <a href="https://www.edesur.com.do/ov/" class="es-modal-promotions-button button radius info" target="_blank">Oficina Virtual</a> -->
    <?php endif ?>





  <a class="close-reveal-modal">&#215;</a>
</div>



<?php /*
  <!-- http://localhost/edesur/es-foundation-compass-wp/wp-content/uploads/2015/02/es-amor-a-primera-mordida.jpg   data-reveal-id="es-modal-amor-primera-mordida" -->
*/ ?>



  <div id="es-subsidio-iframe-modal" class="reveal-modal tiny text-center" data-reveal>
    <iframe src="http://edesurdominicana.com/subsidio-iframe/" frameborder="0" width="300" height="624" style="margin: 0 auto;"></iframe>
    <a class="close-reveal-modal">&#215;</a>
  </div>


<div id="es-mantenimiento-modal" class="reveal-modal medium text-center" data-reveal>
  <!-- <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/05/es-site-mantenimiento-banner.jpg" alt=""> -->

  <!-- <iframe src="https://www.google.com.do/maps/@18.4504549,-69.956614,13z/data=!3m1!4b1!4m2!6m1!1szycXXaMzVkcg.kpMiUaU42dXQ?hl=es" width="640" height="480"></iframe> -->

  <!-- <iframe src="https://www.google.com/maps/d/embed?mid=zycXXaMzVkcg.kpMiUaU42dXQ" width="640" height="480" frameborder="0" style="margin: 0 auto;"></iframe> -->
  <a class="close-reveal-modal">&#215;</a>
</div>