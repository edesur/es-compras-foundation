<section class="es-site-section-interrupciones es-site-section-wrapper small-12 large-10 large-offset-1 columns" data-equalizer>



  <div class="es-site-row">

    <div class="small-12 columns">

    </div>
  </div>

  <div class="row">
    <div class="es-site-section-wrapper-background small-12 columns">

    <?php
        /**
         * The WordPress Query class.
         * @link http://codex.wordpress.org/Function_Reference/WP_Query
         *
         */
        $args_interrupcion = array(
          'post_type'   => 'interrupcion',
          'post_status' => 'publish',
          'order'               => 'DESC',
          'orderby'             => 'date',
          'posts_per_page'         => 1,
        );


      $es_interrupcion_query = new WP_Query( $args_interrupcion );
    ?>

    <?php if ( $es_interrupcion_query->have_posts() ) : ?>


    <div class="row small-collapse">
      <div class="small-12 columns">
        <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/10/es-site-interrupciones-bg.jpg" alt="Mantenimientos Programados - Torres de transmisión ">
      </div>
    </div>




      <?php while ( $es_interrupcion_query->have_posts() ) : $es_interrupcion_query->the_post(); ?>

        <div class="row">
          <div class="es-site-section-interrupciones-heading small-12 large-11 medium-centered columns">

            <?php

              $es_interrupcion_date = DateTime::createFromFormat('Ymd', get_field('es_interrupcion_date'));
              // Jan 1: results in: '%e%1%' (%%, e, %%, %e, %%)

              if ($es_interrupcion_date) {
                $es_day_format = '%e';

                // Check for Windows to find and replace the %e
                // modifier correctly
                if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
                    $es_day_format = preg_replace('#(?<!%)((?:%%)*)%e#', '\1%#d', $es_day_format);
                }

                $es_interrupcion_date_format = __(strftime("%A", $es_interrupcion_date->getTimestamp())) . ' ' . strftime($es_day_format, $es_interrupcion_date->getTimestamp()) . ' de ' . __(strftime("%B", $es_interrupcion_date->getTimestamp())) . ' ' .  strftime("%Y", $es_interrupcion_date->getTimestamp());
                }


            ?>

            <h2>
              <?php
                // setlocale(LC_TIME,"es_ES");
                echo $es_interrupcion_date_format;

              ?><br>
              <span>Mantenimientos Programados</span>
            </h2>

          </div>
        </div>

        <div class="es-site-section-interrupciones-schedule row">
          <div class="small-12 large-11 medium-centered columns">

          <p>Informamos a nuestros clientes, que con el objetivo de realizar trabajos de mantenimiento, el servicio eléctrico será interrumpido este <?php echo $es_interrupcion_date_format; ?>:</p>
            <?php if( have_rows('es_interrupcion_provincia') ): ?>

              <ul class="small-block-grid-1">

              <?php while( have_rows('es_interrupcion_provincia') ): the_row();

                // vars
                $es_interrupcion_provincia_single = get_sub_field('es_interrupcion_provincia_single');

                ?>

                <li>
                  <h3><?php echo $es_interrupcion_provincia_single; ?></h3>

                    <?php if( have_rows('es_interrupcion_schedule_area') ): ?>

                      <ul class="small-block-grid-1">

                      <?php while( have_rows('es_interrupcion_schedule_area') ): the_row();

                        // vars
                        $es_interrupcion_time_start = get_sub_field('es_interrupcion_time_start');
                        $es_interrupcion_time_end = get_sub_field('es_interrupcion_time_end');
                        $es_interrupcion_affected_area = get_sub_field('es_interrupcion_affected_area');

                        ?>

                        <li>
                          <h4 class="es-site-section-interrupciones-time"><?php echo $es_interrupcion_time_start; ?> a <?php echo $es_interrupcion_time_end; ?></h4>
                          <p><?php echo $es_interrupcion_affected_area; ?></p>


                        </li>

                      <?php endwhile; ?>

                      </ul>

                    <?php endif; ?>
                </li>

              <?php endwhile; ?>

              </ul>

            <?php endif; ?>
          </div>
        </div>



      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

    <?php else : ?>
      <p><?php _e( 'No se encontraron Interrupciones' ); ?></p>
    <?php endif; ?>

      <?php


      $big = 999999999; // need an unlikely integer

      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'prev_text'          => __('« Anterior'),
        'next_text'          => __('Siguiente »')
      ) );
      ?>
    </div>
  </div>
</section>