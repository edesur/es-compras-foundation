<section class="es-site-faqs es-site-section-wrapper small-12 columns" data-equalizer>

 <div class="row">
   <div class="small-12 columns">


      <dl class="es-site-faqs-tabs tabs" data-tab>

          <dd class="es-site-faqs-tabs-general es-site-faqs-tabs-single active"><a href="#es-faqs-content-general">Preguntas Generales</a>
          </dd>

          <dd class="es-site-faqs-tabs-telemedida  es-site-faqs-tabs-single"><a href="#es-faqs-content-telemedida">Telemedida</a></dd>

          <dd class="es-site-faqs-tabs-glosario es-site-faqs-tabs-single"><a href="#es-faqs-content-telemedida-glosario">Glosario Telemedida</a>
          </dd>

          <dd class="es-site-faqs-tabs-servicio-prepago es-site-faqs-tabs-single"><a href="#es-faqs-content-servicio-prepago">Servicio Prepago</a>
          </dd>

          <dd class="es-site-faqs-tabs-medicion-neta es-site-faqs-tabs-single"><a href="#es-faqs-content-medicion-neta">Medición Neta</a>
          </dd>

          <?php /*
              <dd class="es-site-faqs-tabs-contratacion-express es-site-faqs-tabs-single"><a href="#es-faqs-content-contratacion-express">Contratación Express</a>
              </dd>
          */ ?>
      </dl>



     <div class="es-site-faqs-tabs-content tabs-content">

        <div class="content active" id="es-faqs-content-general">
          <dl class="accordion" data-accordion="">

            <dd class="accordion-navigation active">
              <a href="#es-faqs-content-general-01">1- ¿Cómo sé cuáles efectos eléctricos generan mayor consumo?</a>
              <div id="es-faqs-content-general-01" class="content active">
                <p>Los que contienen resistencia al paso de la energía, como por ejemplo:<br>
                La nevera, el aire acondicionado, el televisor, los abanicos, lavadoras, hornos microondas,  lava vajillas, entre otros.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-02">2- ¿Qué método puedo utilizar para ahorrar energía?</a>
              <div id="es-faqs-content-general-02" class="content">
                <h5>Consejos prácticos</h5>
                <h6>Aire Split</h6>
                <ul>
                  <li>Mantenga cerradas las puertas y ventanas mientras lo utilice.</li>
                  <li>No es cierto que el equipo enfría más rápido si se coloca la temperatura menor.</li>
                  <li>Asegúrese que el termostato funciona y colóquelo en un lugar que no le afecte el sol.</li>
                </ul>
                <h6>Bomba de Agua</h6>
                <ul>
                  <li>Asegúrese que el sensor  de presión funcione correctamente.</li>
                  <li>Verifique que la válvula (cheque) de la cisterna funcione correctamente.</li>
                  <li>Elimine los escapes de agua a través de llaves y tuberías.</li>
                </ul>
                <h6>Inversor</h6>
                <ul>
                  <li>Cuando esté funcionando, ahorre energía. Recuerde que al cargarse, consumirá la energía que suministró mientras trabajaba.</li>
                  <li>Utilice inversores que el sistema cargador del mismo, disminuya su consumo en la medida que las baterías se van cargando.</li>
                </ul>
                <h6>Bombillos</h6>
                <ul>
                  <li>Cambie bombillos incandescentes por bombillos de bajo consumo, duran 8 veces más y ahorran hasta un 70% de energía.</li>
                  <li>Evite encender lámparas durante el día, utilice al máximo la luz natural.</li>
                  <li>Apague las luces que no esté utilizando.</li>
                  <li>Pinte su casa con colores claros, los colores oscuros absorben la luz, por lo que necesitaría mayor iluminación.</li>
                </ul>
                <h6>Plancha</h6>
                <ul>
                  <li>Trate de planchar la ropa en una sola sesión. Conectar muchas veces la plancha gasta más energía.</li>
                  <li>Planche primero la ropa que requiere menos calor y continúe con la que necesita más.</li>
                </ul>
                <h6>Nevera</h6>
                <ul>
                  <li>No utilice la parte trasera de la nevera para secar paños o ropa, esto provoca un aumento del consumo.</li>
                  <li>No introduzca alimentos calientes, los alimentos deben enfriarse primero.</li>
                  <li>No deje la puerta abierta, no la abra con frecuencia, ni por más de 10 segundos.</li>
                </ul>
                <h6>Lavadora</h6>
                <ul>
                  <li>Llene la lavadora con la carga de ropa apropiada de acuerdo a su capacidad.</li>
                  <li>Disminuya las frecuencias de lavado.</li>
                </ul>
                <h6>Abanico</h6>
                <ul>
                  <li>Mantenga su abanico limpio y en buen estado, no lo deje encendido innecesariamente.</li>
                  <li>Vigile la instalación de los abanicos de techo, ya que si está inadecuada y el abanico &#34;cabecea&#34;, consume más energía.</li>
                  <li>Verifique que no tenga ruidos o chirridos por roces que forcé su operación.</li>
                </ul>
                <h6>Consolas</h6>
                <ul>
                  <li>No deje sus videos juegos conectados, aún cuando estén apagados.</li>
                </ul>
                <h6>DVD</h6>
                <ul>
                  <li>Si termina de usar el DVD, desconéctelo, el  reloj que sigue encendido en la pantalla, está consumiendo energía.</li>
                </ul>
                <h6>Celulares</h6>
                <ul>
                  <li>Cuando ponga a cargar su celular no deje la fuente conectada, si ya la batería está cargada.</li>
                </ul>
                <h6>Computadora</h6>
                <ul>
                  <li>Si tiene computadora, manténgala encendida sólo mientras la esté utilizando; trate de dejarla “suspendida” lo menos posible.</li>
                </ul>
                <h6>Televisor</h6>
                <ul>
                  <li>Apague y desconecte el televisor cuando no lo estés atendiendo.</li>
                  <li>Si su televisor utiliza regulador de voltaje, apáguelo también cuando deje de ver la televisión.</li>
                </ul>
                <h6>Stereo</h6>
                <ul>
                  <li>Evite mantener encendidos innecesariamente los equipos de sonido que no se estén utilizando, ya que además de desperdiciar energía, los equipos tendrán un envejecimiento más rápido y  acabarían por no servir.</li>
                </ul>
                <h6>Licuadora</h6>
                <ul>
                  <li>Pique la comida antes de licuarla, afile las aspas de la licuadora periódicamente y cámbielas si se rompen. Desconéctela cuando termine de usarla.</li>
                </ul>
                <h6>Microondas</h6>
                <ul>
                  <li>Use el horno microondas para calentar porciones pequeñas de alimentos, para un  máximo de 4 personas. Elimine los residuos de alimentos en el horno de microondas, prolonga su duración, reduce el consumo de energía y contribuye a la seguridad de la familia.</li>
                </ul>
                <h6>Horno</h6>
                <ul>
                  <li>Utilice el horno eléctrico para cantidades pequeñas, elimine los residuos de alimentos, esto prolonga su duración y reduce el consumo de energía.</li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-03">3- ¿Qué debo hacer cuando no recibo mi factura física o por correo electrónico?</a>
              <div id="es-faqs-content-general-03" class="content">
                <p>Llamar a la OT24Horas, al <a href="tel://+18096839393" class="label radius warning" itemprop="telephone">(809) 683-9393</a> ext. 2 y registrar su dirección de mail o <span class="label radius warning">dirección domiciliar</span> para que se le remita la factura; o ingresar a la página virtual de EDESUR y crear su cuenta y descargarla o visitar nuestras oficinas comerciales y obtener un duplicado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-04">4- ¿Dentro de qué plazo debo realizar el pago de mi factura?</a>
              <div id="es-faqs-content-general-04" class="content">
                <p>El vencimiento de la factura será a los treinta (30) días a partir de su fecha de emisión, por lo que debe realizar el pago dentro de este período.  Cabe recordar que la no recepción de la factura, no evita la suspensión del servicio.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-05">5- ¿Qué debo realizar al momento de mudarme o cambiar de inmueble?</a>
              <div id="es-faqs-content-general-05" class="content">
                <p>Debe presentarse en una oficina comercial la cancelación  del contrato actual, pagando la deuda pendiente a la fecha y luego solicitar el contrato de energía en el nuevo inmueble,  presentando los documentos requeridos que acrediten la titularidad del inmueble.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-06">6- ¿Qué necesito para suscribir un contrato de energía eléctrica?</a>
              <div id="es-faqs-content-general-06" class="content">
                <p>Copia de la cédula de identidad y electoral  del solicitante. En caso de ser extranjero, una copia del pasaporte o residencia legal.</p>
                <p><span class="label radius warning">La documentación que le otorgue la titularidad legal del inmueble o de la instalación para  la cual se solicita el suministro, entre los cuales tenemos:</span></p>
                <ul>
                  <li>El título de propiedad del inmueble o certificación de la Dirección General de Catastro o contrato de alquiler notariado o una autorización otorgada por el propietario del inmueble, si el solicitante habita en calidad de préstamo.</li>
                </ul>
                <p class="label radius warning">En caso de solicitud de inmuebles en trámites de compra:</p>
                <ul>
                  <li>Certificación del banco o inmobiliaria que acredite que el solicitante está comprando el inmueble u otro documento que acredite el uso legítimo del mismo.</li>
                  <li>En caso de que el titular  del inmueble se deba hacer representar, deberá hacerlo mediante poder especial debidamente notariado, aportar una copia de las cédulas de ambos.</li>
                </ul>
                <h6>En caso de que el titular  del inmueble se deba hacer representar, deberá hacerlo mediante poder especial debidamente notariado, aportar una copia de las cédulas de ambos.</h6>
                <ul>
                  <li>La documentación que acredite la titularidad del inmueble o de la instalación para la cual solicita el suministro (título de propiedad o certificación de la Dirección General de Catastro o contrato de alquiler).</li>
                  <li>Copia certificada de los documentos constitutivos de la empresa.</li>
                  <li>Copia Tarjeta Identificación Tributaria con el  RNC.</li>
                  <li>Acta de Consejo vigente o de la asamblea que avale al solicitante como representante legal de la empresa o entidad, y una copia de la cédula del representante legal.</li>
                  <li>Copia del Certificado de Registro Mercantil (vigente).</li>
                  <li>Carta membretada y sellada de la razón social solicitando el servicio.</li>
                  <li>Sello físico para estampar el contrato de energía eléctrica.</li>
                </ul>
                <h6>Para áreas comunes</h6>
                <p class="label radius warning">Los que están constituidos deberán depositar:</p>
                <ul>
                  <li>Acta de constitución del edificio.</li>
                  <li>Copia Tarjeta Identificación Tributaria con RNC.</li>
                  <li>Sello físico del edificio con el nombre de éste.</li>
                  <li>Acta asamblea si la tiene, poder legal de representación, así como copia de la cédula del representante.</li>
                </ul>
                <p class="label radius warning">Los que no estén constituidos deberán depositar:</p>
                <ul>
                  <li>Acta notarial de un representante legal, firmado por todos los propietarios del edificio y copia de las cédulas de identidad y electoral de los mismos.</li>
                  <li>Copia de títulos de propiedad o contratos de compra.</li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-07">7- ¿Cómo puedo reclamar sin ser el titular del contrato?</a>
              <div id="es-faqs-content-general-07" class="content">
                <p>Con un poder notariado donde el titular autorice a realizar dicha reclamación; en caso que el titular haya fallecido, debe presentar título de propiedad o contrato de alquiler, acta de defunción y declaración de herederos para realizar dicha reclamación.</p>
                <p>En caso de no conocer al titular actual, debe llevar documentos que avalen el derecho de la nueva titularidad como propietario o inquilino  del inmueble, más la copia de su cédula.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-08">8- ¿Qué necesito y cómo puedo realizar un Acuerdo de Pago?</a>
              <div id="es-faqs-content-general-08" class="content">
                <p>En caso de ser persona física, se necesita ser el titular del contrato o un representante con poder notariado que autorice a realizar dicha transacción y la cédula de identidad en ambos casos, además como mínimo el 30% de inicial de la deuda acordada.</p>
                <p>En caso de ser persona jurídica, debe venir un representante con poder notariado que autorice la transacción a realizar y su cédula de identidad, además del Registro Nacional de Contribuyente (RNC), Registro Mercantil o copia de la última Asamblea actualizada y sello físico de la empresa.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-09">9- ¿Qué necesito para cancelar el contrato de energía eléctrica?</a>
              <div id="es-faqs-content-general-09" class="content">
                <h5>Requisitos:</h5>
                <h6>Persona Física:</h6>
                <ul>
                  <li>Copia de la cédula de identidad y electoral. En caso de ser extranjero una copia del pasaporte o residencia legal.</li>
                  <li>Saldar la deuda pendiente.</li>
                  <li>En caso de que el titular del contrato se deba hacer representar, deberá hacerlo mediante un poder notariado, y aportar una copia de las cédulas de ambos.</li>
                </ul>
                <h6>Personas Jurídicas: </h6>
                <ul>
                  <li>Carta con membrete y sellada solicitando la cancelación del contrato.</li>
                  <li>Copia de Tarjeta Identificación Tributaria con el RNC.</li>
                  <li>Acta de consejo vigente o de la asamblea que avale al solicitante como representante legal de la empresa o entidad.</li>
                  <li>En caso que no sea el representante legal, debe otorgar un poder notariado donde autoriza para su representación.</li>
                  <li>Copia de las cédulas del que firma la autorización y del autorizado.</li>
                  <li>Sello físico de la empresa.</li>
                </ul>
                <h5>Requisitos para reembolso de fianza.</h5>
                <h6>Personas Físicas:</h6>
                <ul>
                  <li>El titular del contrato al momento de retirar la fianza presentará el formulario de solicitud de cancelación de contrato y su cédula de identidad y electoral.</li>
                  <li>En caso de que no sea el titular quien solicite la fianza, debe entregar un poder notariado y copia de la cédula de identidad de ambos.</li>
                </ul>
                <h6>Personas Jurídicas:</h6>
                <ul>
                  <li>Carta con membrete y sellada solicitando el reembolso de la fianza.</li>
                  <li>Formulario de “Declaración jurada de cancelación de contrato”, debidamente firmado y sellado por el representante de EDESUR.</li>
                  <li>En caso de que no sea el representante legal, debe hacerse representar mediante un poder notariado, aportar copia de las cédulas del que firma la autorización y del autorizado.</li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-10">10- ¿Qué debo hacer para que el contrato de energía pase a mi nombre?</a>
              <div id="es-faqs-content-general-10" class="content">
                <p>En caso de que el cambio de titular sea con consentimiento y presencia de ambos, titular originario y el solicitante, deben apersonarse a una oficina comercial, mostrando documentos donde lo acredite como nuevo titular, realizar el proceso de cambio de titularidad y firmar documento  “Declaración de Solicitud de Cambio de Titular” en donde autoriza, desiste, cede y traspasa el contrato al nuevo titular,  incluyendo el depósito de garantía (fianza).</p>
                <p>Debe presentar copia de la <b>cédula de identidad</b> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</p>
                <p>La documentación que <b>acredite la titularidad del inmueble</b>, entre los cuales tenemos:</p>
                <ul>
                  <li>Contrato de alquiler a nombre del solicitante debidamente registrado en el departamento de alquileres del Banco Agrícola.</li>
                  <li>Acto de sesión  en uso o préstamo, autorización otorgada por el propietario del inmueble y copia de la cédula de identidad del propietario</li>
                  <li>La certificación de la Dirección General de Catastro que acredite la posesión o tenencia del inmueble.</li>
                  <li>Solicitud por escrito del contrato de suministro debidamente firmada por el titular del inmueble.</li>
                </ul>
                <h6>En caso de solicitud de inmuebles en trámites de compra</h6>
                <ul>
                  <li>Carta de un banco o inmobiliaria que acredite que el solicitante está comprando el inmueble.</li>
                  <li>Otro documento que acredite el uso legítimo del inmueble.</li>
                </ul>
                <h6>Persona jurídica, además de los requisitos anteriormente especificados se requerirá:</h6>
                <ul>
                  <li>Copia certificada de los documentos constitutivos de la empresa.</li>
                  <li>Copia tarjeta Registro Nacional de Contribuyente (RNC).</li>
                  <li>Acta de Consejo vigente o de la asamblea que avale al solicitante como representante legal de la empresa o entidad.</li>
                  <li>Copia del registro industrial a nombre de la persona jurídica solicitante otorgado por la Secretaría de Industria y Comercio. (Obligatorio cuando solicita una tarifa industrial)</li>
                  <li>Copia del registro industrial a nombre de la persona jurídica solicitante otorgado por la Secretaría De Industria Y Comercio. (Obligatorio cuando solicita una tarifa industrial)</li>
                  <li>Carta membretada y sellada de la razón social solicitando el suministro.</li>
                </ul>
                <p>En caso de que el cambio de titular sea en ausencia del titular originario, el solicitante deberá traer documentación que lo acredite como solicitante del servicio de energía del inmueble en calidad de titular y firmar el documento <b>Declaración de Solicitud de Cambio de Titular</b> en ausencia del titular originario, que especificará la causa de la ausencia del titular y declara que solicita el cambio a su nombre y acepta cada una de las obligaciones del contrato actual.  La fianza pertenece al titular originario.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-11">11- ¿Qué hacer cuando no estoy de acuerdo con mi facturación?</a>
              <div id="es-faqs-content-general-11" class="content">
                <p>Dirigirse a una oficina comercial para verificar y analizar su factura.  En caso de que aplique, corresponde realizar una reclamación para verificar y validar las condiciones de su suministro de energía.  Debe recordar que es necesario ser el titular del contrato y abonar la cantidad según corresponda para evitar suspensión del servicio mientras se resuelva su reclamación.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-12">12- ¿Dónde y cómo puedo pagar mi factura?</a>
              <div id="es-faqs-content-general-12" class="content">
                <ul>
                  <li>Oficinas Comerciales y Puntos Expresos</li>
                  <li>OT24Horas (oficina telefónica)</li>
                  <li>Bancos Comerciales</li>
                  <li>Farmacias</li>
                  <li>T-pago</li>
                  <li>Página WEB <a href="http://www.edesur.com.do/">http://www.edesur.com.do/</a></li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-13">13- ¿Qué hacer cuando no tengo energía en mi inmueble?</a>
              <div id="es-faqs-content-general-13" class="content">
                <p>Verificar si es solamente mi vivienda o el sector  completo y realizar un reporte a la Oficina Telefónica OT24 Horas al teléfono <a href="tel://+18096839393" class="label radius warning" itemprop="telephone">(809) 683-9393</a></p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-14">14- ¿Qué debo hacer para solicitar que mi circuito sea 24-horas?</a>
              <div id="es-faqs-content-general-14" class="content">
                <p>Gestionar y concientizar al sector de ese circuito,  de la necesidad de tener el pago al día de su factura de energía con el fin de que reduzcan al mínimo aceptable, las pérdidas no técnicas del sector.</p>
                <p>Depositar una comunicación en la oficina comercial más cercana, solicitando la inclusión del sector en un circuito 24H, autorizado por los residentes de la comunidad</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-15">15- ¿Qué sucedería si la empresa me encuentra en el suministro o aparato de medida un fraude intencional?</a>
              <div id="es-faqs-content-general-15" class="content">
                <p>El equipo de la Procuraduría General Adjunta para el Sistema Eléctrico (PGASE) recolecta las evidencias necesarias in situ y levanta un acta de irregularidad intencional en la que se determina los kWh consumidos no facturados para emitir la factura correspondiente. Es necesario recordar que el cliente que reincide en esa práctica, puede ir a prisión por robo de energía.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-16">16- ¿Qué es una factura complementaria y dónde debo dirigirme?</a>
              <div id="es-faqs-content-general-16" class="content">
                <p>Una  facturación complementaria es una energía consumida por el cliente que no fue facturada en el periodo correspondiente por irregularidades no intencional en el registro del medidor y que requiere ser facturada en un recibo complementario.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-17">17- ¿Qué sucede cuando no realizo el pago de mi factura dentro del plazo de los 30 días?</a>
              <div id="es-faqs-content-general-17"class="content">
                <p>Se suspende el servicio de energía y se aplica la penalidad de RD $330.00 de cargos por re-conexión.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-18">18- ¿Cuál es el proceso que debo seguir, en caso de que se vea afectado un efecto eléctrico producto de una irregularidad en el suministro de energía eléctrica?</a>
              <div id="es-faqs-content-general-18" class="content">
                <p>Es necesario que el titular del contrato se dirija a la oficina comercial más cercana, llevando una comunicación formal en donde exprese lo sucedido, se genera la reclamación en el sistema y se entrega “Requisitos para reclamos por Daños a las Instalaciones y Artefactos Eléctricos del cliente”.</p>
                <p>Se procede a investigar lo ocurrido, elaborando un informe técnico que muestre la responsabilidad del siniestro.  En caso de ser la responsabilidad de Edesur, se contacta al cliente para que deposite cotizaciones o las facturas de compra de los Efectos reportados como averiados u otros requisitos indicados en el formulario.  Luego se procede a informar al cliente la propuesta de negociación y debe pasar a firmar la misma para elaborar el cheque y contactar al cliente para la entrega y descargo.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-19">19- ¿Por qué cuando supero los 700 kilovatios hora mi factura se duplica?</a>
              <div id="es-faqs-content-general-19" class="content">
                <p>Porque el precio de los kWh consumidos hasta 700,  se subsidian en rangos escalonados; en caso mayor de 700, todos los kWh consumidos se cobran a un mismo precio sin subsidio.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-20">20- ¿Qué debo hacer para evitar la suspensión del servicio al momento de interponer una reclamación?</a>
              <div id="es-faqs-content-general-20" class="content">
                <p>Para fines de evitar suspensión del servicio eléctrico en caso de reclamaciones que involucren facturaciones corrientes, el cliente deberá abonar como pago del mes el equivalente al promedio de las últimas tres (3) facturas pagadas, sin incluir la(s) factura(s) objeto de reclamación. Para el caso de clientes sin histórico de consumo, el mismo pagará el equivalente al 33% de la factura objeto de reclamación.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-21">21- ¿Qué tengo que hacer para realizar el cambio de titularidad en caso de que el inmueble sea una herencia?</a>
              <div id="es-faqs-content-general-21" class="content">
                <p>Debe aportar los siguientes documentos que acrediten la autorización para realizar el cambio de titular a su nombre:</p>
                <ul>
                  <li>Acta de defunción del titular actual</li>
                  <li>Título de propiedad</li>
                  <li>Declaración jurada de herederos (que indique que es el único heredero del inmueble). De existir más herederos un poder notariado donde ellos autoricen que es esa persona que habitara el inmueble. </li>
                  <li>Copia de cédula de los implicados.</li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-22">22- ¿Dónde o cómo puedo reportar un fraude?</a>
              <div id="es-faqs-content-general-22" class="content">
                <p>A través de la OT24Horas,  <a href="tel://+18096839393" class="label radius warning" itemprop="telephone">(809) 683-9393</a>, la página WEB <a href="<?php echo get_page_link(18); ?>">Denuncia de Fraude</a>, Las Redes Sociales Facebook <a href="https://www.facebook.com/EdesurDominicana">Edesur Dominicana</a> y Twitter <a href="https://twitter.com/edesurrd">@edesurrd</a> ó en la Oficina comercial más cercana.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-23">23- ¿Qué debo hacer cuando visualice una avería eléctrica en una localidad o avenida mientras estoy desplazándome?</a>
              <div id="es-faqs-content-general-23" class="content">
                <p>Llamar a la oficina telefónica OT24Horas para reportar la avería o escribirnos por nuestras Redes Sociales <a href="https://www.facebook.com/EdesurDominicana">Facebook</a> y <a href="https://twitter.com/edesurrd">Twitter</a>.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-24">24-  ¿Dónde y cómo puedo reportar una brigada que esté actuando de manera ilícita? </a>
              <div id="es-faqs-content-general-24" class="content">
                <p>Llamar a la oficina telefónica OT24, por nuestras redes Sociales <a href="https://www.facebook.com/EdesurDominicana">Facebook</a> y <a href="https://twitter.com/edesurrd">Twitter</a> ó  dirigirse a una oficina comercial más cercana.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-25">25- ¿Sabes cuál es el objetivo de la fianza?</a>
              <div id="es-faqs-content-general-25" class="content">
                <p>Sirve como garantía del cumplimiento de todas las obligaciones económicas que surjan de la firma del contrato del suministro y quedará determinada por el valor resultante del cálculo de la estimación de dos (2) meses de consumo en función de la potencia, las horas de utilización y la tarifa contratada, de acuerdo a  la tabla homologada de consumos avalada por la SIE.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-26">26- ¿Sabes qué es el servicio pre-pago?</a>
              <div id="es-faqs-content-general-26" class="content">
                <p>Opción alterna de consumo de energía prepagada (de manera anticipada) a clientes que se encuentran en zonas de difícil gestión y cuya finalidad es ajustar su consumo a su presupuesto.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-27">27- Cuando solicito un nuevo contrato ¿cuál es la finalidad de la inspección de nuevo servicio?</a>
              <div id="es-faqs-content-general-27" class="content">
                <p>Verificar las instalaciones del cliente previo a la contratación para certificar que todo esté en condiciones hábiles de instalar el servicio de energía eléctrica en el punto solicitado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-28">28- ¿Qué sucede si incumplo un acuerdo a plazo?</a>
              <div id="es-faqs-content-general-28" class="content">
                <p>El acuerdo a plazos queda sin efecto y se procede a suspender el servicio de energía al cliente.  Hasta que no salde la deuda, el servicio permanecerá desconectado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-29">29- ¿Qué necesito saber para solicitar una remoción de poste (Retranqueo)?</a>
              <div id="es-faqs-content-general-29" class="content">
                <p>Debe depositar una carta con la solicitud de la remoción con la dirección exacta y números de contactos, un croquis  del área y pagar $1,000.00 pesos.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-30">30- ¿Cómo  puedo solicitar un aumento de voltaje y qué necesito llevar?</a>
              <div id="es-faqs-content-general-30" class="content">
                <p>Debe presentarse el titular del contrato con su cédula de identidad a realizar la solicitud en la oficina comercial más cercana y se procederá a generar un expediente para verificar que la acometida del cliente esté adecuada para los fines.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-31">31- ¿Qué debo hacer cuando existe una deuda en el inmueble donde estoy solicitando un contrato?</a>
              <div id="es-faqs-content-general-31" class="content">
                <p>Dirigirse a la oficina comercial más cercana con la documentación legal requerida que acredite la titularidad del inmueble y que ésta sea veraz, correcta y suficiente.</p>
                <p>Se le comunicará al cliente que se procederá a realizar una indagación del inmueble y de la deuda y en caso de descubrirse responsabilidad por los consumos o vínculos legales con el deudor se accionará legalmente para el cobro de la deuda, sus intereses y los gastos legales en que la empresa incurra para el cobro de los mismos.  Se procederá a rescindir el contrato.</p>
                <p>En caso de que el titular de inmueble se deba hacer representar deberá hacerlo mediante poder especial debidamente notariado El representante también deberá aportar copia de su cédula.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-32">32- ¿Qué hacer cuando encuentro energía eléctrica en el inmueble donde voy a residir?</a>
              <div id="es-faqs-content-general-32" class="content">
                <p>Debo informar a través de la OT24 Horas o dirigirme a una oficina comercial más cercana para hacer de conocimiento y presentar el documento que le acredita la titularidad del inmueble para solicitar el contrato.</p>
                <p><b>Nota:</b> La energía que se consuma a partir de la fecha de su contrato de alquiler o propiedad del inmueble le corresponde al nuevo inquilino. Debe realizar contrato para evitar suspensión del servicio eléctrico o posibles irregularidades en el proceso.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-33">33- ¿Cómo puedo realizar sugerencias respecto a la atención al cliente?</a>
              <div id="es-faqs-content-general-33" class="content">
              <p>En el buzón de sugerencia disponible y ubicado en cada Oficina Comercial, por las redes sociales o vía telefónica por OT24H al <a href="tel://+18096839393" class="label radius warning" itemprop="telephone">(809) 683-9393</a>.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-34">34- ¿Cómo puedo solicitar un taller de concientización de uso y ahorro de energía para la comunidad?</a>
              <div id="es-faqs-content-general-34" class="content">
                <p>Debe realizar una comunicación de manera formal y entregarla en el punto de atención más cercano para ser remitido a la unidad correspondiente y luego contactarse con el responsable en la comunidad.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-35">35- ¿Conoces tu factura?</a>
              <div id="es-faqs-content-general-35" class="content">
                  <p><a href="<?php echo content_url(); ?>/uploads/es-faq-factura.png" class="th"><img src="<?php echo content_url(); ?>/uploads/es-faq-factura.png" class="th"></a></p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-general-36">36- ¿Conoces los trabajos que realiza EDESUR?</a>
              <div id="es-faqs-content-general-36" class="content">
                <p>Gestión social, rehabilitación de redes, circuitos 24h, concientización sobre el ahorro de energía a los niños y a las localidades.</p>
              </div>
            </dd>
          </dl>
        </div>

        <div class="content" id="es-faqs-content-telemedida">
          <dl class="accordion" data-accordion="">

            <div class="es-faqs-content-telemedida-intro small-12 columns">
              <p class="es-faqs-desc-telemedida">Nos complace informarle que nuestra empresa desarrolla un programa de instalación de medidores con tecnología de tele-medición.</p>
              <p class="es-faqs-desc-telemedida">La Tele-medición es un moderno sistema de recolección de datos en línea que nos permitirá establecer una comunicación a distancia con su medidor de manera confiable y segura.</p>
              <p class="es-faqs-desc-telemedida">EDESUR Dominicana aplica esta innovación con la finalidad de ofrecer un mejor servicio a sus clientes por medio de un sistema de tecnología avanzada y mayores prestaciones de servicio y gestión.</p>
              <p class="es-faqs-desc-telemedida">A la fecha, Edesur tiene más de 100 mil medidores Telemedidos instalados.</p>
            </div>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-01">1- ¿Por qué me cambiaron el medidor?</a>
              <div id="es-faqs-content-telemedida-01" class="content active">
                <p>EDESUR Dominicana comprometida con la mejora continua, para ofrecer nuevos servicios y mayor calidad a nuestros clientes, ha venido implementando en los últimos años un programa de renovación de los medidores utilizados para facturar. Los medidores que estamos instalando son de tercera generación (tele-medidos) y cumplen con las normas internacionales y nacionales como son las normas ISO, ANSI, IEC y las Normas y Sistema de Calidad (DIGENOR).</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-02">2- ¿Por qué me cambiaron el medidor a mí y a mi vecino no?</a>
              <div id="es-faqs-content-telemedida-02" class="content">
                <p>Esta iniciativa está dirigida a un segmento específico de clientes y ha sido dividida en varias etapas. Principalmente dependiendo del consumo de energía mensual, en base a este criterio, la primera etapa fue dirigida a los clientes que mensualmente consumen sobre los 1,000 KWh, la segunda etapa fue dirigida a los clientes de 400 KWh a 1,000 KWh, y la tercera etapa está dirigida a clientes que presentan un consumo mensual mayor de 200  KWh.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-03">3- ¿La decisión del cambio de medidor a un cliente es aleatoria o cumple con algún criterio?</a>
              <div id="es-faqs-content-telemedida-03" class="content">
                <p>No es aleatoria, está fundamentada en un programa integral acorde con la Ley 1-12 Sobre Estrategia Nacional de Desarrollo 2030 y el plan estratégico para la recuperación del sector eléctrico Dominicano 2013 – 2016.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-04">4- ¿Qué tiene este medidor que el anterior no tenga?</a>
              <div id="es-faqs-content-telemedida-04" class="content">
                <p>Los medidores que estamos instalando son de tercera generación (tele-medidos), los cuales tienen la tecnología para recolectar y transferir automáticamente registros de consumo, diagnósticos y eventos a una base de datos central para realizar procesos de facturación, resolución de problemas y análisis. El medidor que estamos retirando no tienes estas opciones</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-05">5- ¿En qué me beneficia el nuevo medidor?</a>
              <div id="es-faqs-content-telemedida-05" class="content">
                <ul>
                    <li>Mayor Precisión, más del doble de la precisión requerida por ley. Nuevos servicios (reconexión automatizada, envío de teleconsumo, proyección mensual de consumo, monitoreo en tiempo real de fallas, entre otras). Futuros servicios (opción de modalidad Pre-pago, estabilizar la carga de circuitos por clientes, Integrable con redes inteligentes). En la actualidad los clientes no conocen el evolutivo de su consumo, la única información con la que cuentan es la facturación mensual. La tele medición permite que los clientes se empoderen de la información asociada a su consumo, al recibir reportes diarios con proyecciones al cierre de cada ciclo de facturación, así como otorga las herramientas necesarias para que el departamento comercial pueda responder las reclamaciones de una forma clara y en un tiempo prudente.</li>
                    <li>Evitar irregularidades del medio ambiente en el proceso de facturación. El proyecto espera sustituir la facturación física, de los clientes objetivos, por una facturación sin intervención humana. La participación de varios actores en el proceso de gestión y procesamiento incrementan las posibilidades de que se generen errores no intencionales e intencionales al momento de facturar; la tele medida elimina estos actores y permite que los sistemas realicen con los debidos controles todo el proceso.</li>
                    <li>Optimizar la operativa para normalizar las averías e irregularidades que afecten la continuidad del servicio y la medición del mismo. La tele medición a clientes se complementa con los sistemas de gestión de avisos e incidencias de la empresa, con la finalidad de  reducir el tiempo de atención al cliente, así como permite que los centros técnicos puedan normalizar las averías en los equipos de medición sin tener que esperar una verificación rutinaria o la fecha de facturación, como se hace en la actualidad con los clientes objetivos.</li>
                  </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-06">6- ¿Qué es el servicio de Teleconsumo?</a>
              <div id="es-faqs-content-telemedida-06" class="content">
                <p>Servicio ofrecido por EDESUR a través del cual el cliente recibe diaria, semanal, quincenal o mensualmente por correo electrónico, un reporte pormenorizado detallándole su: Consumo diario, Consumo acumulado del ciclo en curso, Histórico de consumo. Promedio en fines de semana. Promedio en días de semana. Consumo máximo y Proyección de consumo.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-07">7- ¿Qué beneficios ofrece este servicio?</a>
              <div id="es-faqs-content-telemedida-07" class="content">
                <p>Le permite al cliente estimar el valor de su factura gracias a la proyección de consumo que ofrece el reporte, también a  hacer un uso más racional de la energía, reduciendo así su consumo, ahorrándole dinero en su factura eléctrica y contribuyendo a minimizar el impacto ambiental de la generación eléctrica nacional. Este servicio sirve para educar al cliente sobre la relación entre su consumo y la factura.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-08">8- ¿Cómo puedo yo obtener el servicio de Teleconsumo?</a>
              <div id="es-faqs-content-telemedida-08" class="content">
                <p>Siempre y cuando tenga un medidor Telemedido, solo tiene que enviar un correo a miconsumo@edesur.com.do con su NIC.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-09">9- ¿El cambio a este medidor tiene algún costo para el cliente?</a>
              <div id="es-faqs-content-telemedida-09" class="content">
                <p>No.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-10">10- ¿Cuántos clientes están Telemedidos?</a>
              <div id="es-faqs-content-telemedida-10" class="content">
                <p>Aproximadamente unos 100 mil, con miras a duplicar esa cifra para finales del presente año.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-11">11- ¿Cuantos circuitos están siendo Telemedidos?</a>
              <div id="es-faqs-content-telemedida-11" class="content">
                <p>TODOS los circuitos (161) cuentan con tecnología de Telemedición aunque no todos sus clientes como ya hemos explicado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-12">12- ¿Dónde instalaran los medidores telemedidos que faltan?</a>
              <div id="es-faqs-content-telemedida-12" class="content">
                <p>Actualmente se intenta completar los circuitos de subestaciones específicas, haciendo barridos en zonas completas.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-13">13- ¿Qué sectores hay telemedidos fuera de la provincia Santo Domingo?</a>
              <div id="es-faqs-content-telemedida-13" class="content">
                <p>La mayor concentración está en el DN y Santo Domingo, pero también hay Clientes telemedidos en todas las provincias bajo concesión de EDESUR.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-14">14- ¿Es cierto que estos medidores "corren más" que los regulares?</a>
              <div id="es-faqs-content-telemedida-14" class="content">
                <p>No, todos los medidores que instala EDESUR Dominicana cumplen con las normas nacionales e internacionales, además estos equipos pasan por un estricto control de calidad institucional, así como por el control de normas reguladas: (DIGENOR, ISO, ANSI y IEC).</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-15">15- ¿Si no estoy de acuerdo con la lectura del nuevo medidor, que debo hacer?</a>
              <div id="es-faqs-content-telemedida-15" class="content">
                <p>Solicitar una verificación de la instalación por los canales autorizados. Oficina Comercial Tel.: <a href="tel://+18096839393" class="label radius warning" itemprop="telephone">(809) 683-9393</a></p>
              </div>
            </dd>

          </dl>
        </div>


        <div class="content" id="es-faqs-content-telemedida-glosario">
          <dl class="accordion" data-accordion="">

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-01">1- Cambio de medidor</a>
              <div id="es-faqs-content-telemedida-glosario-01" class="content active">
                <p>Identifica los consumos pertenecientes al registro de un nuevo medidor, resultantes del cambio del medidor activo para la última facturación del cliente.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-02">2- Consumo acumulado a la fecha</a>
              <div id="es-faqs-content-telemedida-glosario-02" class="content">
                <p>Consumo registrado por el medidor desde la última fecha de facturación hasta la fecha de emisión del Reporte de TeleConsumo Residencial.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-03">3- Consumo diario no disponible</a>
              <div id="es-faqs-content-telemedida-glosario-03" class="content">
                <p>Identifica el o los días en los cuales por razones técnicas u operativas el sistema de lectura remota, habiendo registrado las lecturas del medidor, no ha podido procesar el consumo correspondiente.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-04">4- Consumo facturado</a>
              <div id="es-faqs-content-telemedida-glosario-04" class="content">
                <p>Consumo de energía facturado en la última factura mensual emitida al cliente.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-05">5- Detalle de consumo</a>
              <div id="es-faqs-content-telemedida-glosario-05" class="content">
                <p>Presenta los valores de lectura y consumo de los dos días anteriores a la fecha de emisión del reporte de TeleConsumo Residencial.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-06">6- Detalle de última facturación</a>
              <div id="es-faqs-content-telemedida-glosario-06" class="content">
                <p>Presenta el valor de lectura registrado por el medidor en la última factura mensual emitida al cliente</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-07">7- Histórico de consumos diarios (kWh)</a>
              <div id="es-faqs-content-telemedida-glosario-07" class="content">
                <p>Representación gráfica de los consumos diarios de energía, expresados en kWh, durante el período analizado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-08">8- Máximo consumo diario</a>
              <div id="es-faqs-content-telemedida-glosario-08" class="content">
                <p>Indica el mayor consumo de energía registrado posterior a la última fecha de facturación, así como su fecha de ocurrencia.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-09">9- Máxima potencia</a>
              <div id="es-faqs-content-telemedida-glosario-09" class="content">
                <p>Indica el mayor registro de potencia demandada posterior a la última fecha de facturación, así como su fecha de ocurrencia.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-10">10- Período analizado</a>
              <div id="es-faqs-content-telemedida-glosario-10" class="content">
                <p>Intervalo de tiempo comprendido entre la fecha de emisión del Reporte de TeleConsumo Residencial y los treinta y un días anteriores a ésta.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-11">11- Promedio de lunes a viernes</a>
              <div id="es-faqs-content-telemedida-glosario-11" class="content">
                <p>Promedio ponderado de la energía diaria consumida durante los días comprendidos entre lunes y viernes del período analizado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-12">12- Promedio diario</a>
              <div id="es-faqs-content-telemedida-glosario-12" class="content">
                <p>Promedio ponderado de la energía consumida diariamente durante la totalidad del período analizando.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-13">13- Promedio en sábados y domingos</a>
              <div id="es-faqs-content-telemedida-glosario-13" class="content">
                <p>Promedio ponderado de la energía consumida los días sábados y domingos del período analizado</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-14">14- Proyección de consumo para el ciclo actual</a>
              <div id="es-faqs-content-telemedida-glosario-14" class="content">
                <p>Estimación del consumo de energía mensual del cliente de mantenerse el comportamiento de consumo deenergía registrado durante el período analizado.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-telemedida-glosario-15">15- Última fecha de facturación</a>
              <div id="es-faqs-content-telemedida-glosario-15" class="content">
                <p>Fecha final del período de facturación detallado en la factura mensualmente emitida al cliente.</p>
              </div>
            </dd>
          </dl>
        </div>

        <div class="content" id="es-faqs-content-servicio-prepago">
          <dl class="accordion" data-accordion="">

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-01">1- ¿Qué es el servicio prepago de energía?</a>
              <div id="es-faqs-content-servicio-prepago-01" class="content active">
                <p>El servicio de energía prepago es un moderno sistema que permite a nuestros clientes administrar su consumo a través de recargas, debido a que puede comprar la energía que desee consumir, cuando lo requiera y por el importe (RD$) que desee. Con este servicio se garantiza 24 horas de energía, eliminación de facturación mensual y mayor transparencia en la medición de su servicio.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-02">2- ¿Cómo funciona el servicio de energía prepago?</a>
              <div id="es-faqs-content-servicio-prepago-02" class="content">
                <p>El funcionamiento es similar al servicio prepago para celulares, cuando se tiene balance de minutos disponibles se puede llamar, cuando se agota no se puede llamar hasta comprar una tarjeta de recarga. En el caso del sistema de energía prepagada, el cliente posee un balance de energía (KWH), y según vaya consumiendo energía, ésta es descontada del balance, cuando el balance es igual a cero la energía es suspendida de forma automática, en este momento el cliente debe comprar su recarga y cuando éste balance es recargado el servicio es reconectado automáticamente hasta agotar el balance disponible nuevamente.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-03">3- ¿Qué necesito para contratar el servicio prepago?</a>
              <div id="es-faqs-content-servicio-prepago-03" class="content">
                <p>Solo se necesita lo siguiente:</p>
                <ol>
                  <li>Un documento de ID legalmente valido (Cédula, Pasaporte, RNC).</li>
                  <li>Copias de Contrato de Alquiler o Título de Propiedad.</li>
                  <li>Poseer cuentas en cero “0” con la distribuidora.</li>
                  <li>4 Realizar la solicitud a través de las Oficinas Comerciales de EDESUR o canalizar la solicitud inicial a través del servicio de atención al cliente del Call Center (809-683-9393) disponibles las 24 Horas.</li>
                </ol>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-04">4- ¿Puedo optar por el servicio prepago independientemente de lugar donde viva?</a>
              <div id="es-faqs-content-servicio-prepago-04" class="content">
                <p>No, por el momento solo es posible optar por este tipo de servicio dentro de las demarcaciones donde ya se encuentra instalada este tipo de tecnología, hasta el momento no es posible instalar este tipo de tecnología de forma individual.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-05">5- ¿Dónde puedo comprar recargas para mi servicio prepago?</a>
              <div id="es-faqs-content-servicio-prepago-05" class="content">
                <p>Puede realizar este tipo de transacciones a través de las estafetas autorizadas de nuestra red de Canales de Pago o a través de las Oficinas Comerciales de EDESUR.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-06">6- ¿Por qué en ocasiones me venden los KWH más caros de lo normal?</a>
              <div id="es-faqs-content-servicio-prepago-06" class="content">
                <p>La tarifa de precios de energía es regulada por la Superintendencia de Electricidad (SIE) y está amparada por la Ley General de Electricidad, no contempla un precio fijo, sino que, contempla diferentes precios de forma escalonada en función a la compra, debido a que parte del precio es subsidiado por el Estado para los clientes con consumos inferiores a los 700 KWH dentro de un mismo mes, en caso de exceder este importe, el cliente debe devolver el subsidio previamente entregado debido a que por lo antes explicado no le corresponde, de esta manera mientras menos se consume menos cuesta la energía.</p>
                <p>Por ejemplo, para un cliente residencial, los escalones y precios de la energía se comporta de la siguiente manera:</p>

                <table>
                  <thead>
                    <tr>
                      <th>Numero de Escalón</th>
                      <th>Escalón de Tarifa</th>
                      <th>Energía Comprendida (KWH)</th>
                      <th>Precio / KWH</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>0 - 200 KWH</td>
                      <td>200</td>
                      <td>RD$ 4.44</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>201 - 300 KWH</td>
                      <td>100</td>
                      <td>RD$ 6.97</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>301 - 700 KWH</td>
                      <td>400</td>
                      <td>RD$ 10.86</td>
                    </tr>
                    <tr>
                      <td>Devolución Subsidio >700</td>
                      <td>700 - 701 KWH</td>
                      <td>1</td>
                      <td>RD$ 1,852.10</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>>701 KWH</td>
                      <td>--</td>
                      <td>RD$ 11.10</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-07">7- ¿Dónde puedo reportar si tengo una avería, reclamación o solicitar información sobre el servicio prepago?</a>
              <div id="es-faqs-content-servicio-prepago-07" class="content">
                <p>Para  cualesquiera de estos casos puede llamar al Call Center (809-683-9393) disponibles las 24 horas o dirigirse a una Oficina Comercial de EDESUR.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-servicio-prepago-08">8- ¿Cuáles son las condiciones para cancelar el servicio prepago?</a>
              <div id="es-faqs-content-servicio-prepago-08" class="content">
                <p>Para realizar el proceso de Cancelación, debe Cumplir con lo siguiente:</p>
                <ol>
                  <li>Haber permanecido al menos 12 meses luego de la firma de Contrato.</li>
                  <li>Solo en casos de mudanza es posible realizar la cancelación antes de tiempo establecido en el punto anterior, y debe solicitar en el momento el servicio de energía en la nueva residencia.</li>
                  <li>Debe entregar en la Oficina Comercial el Display o Cajita entregada al momento de la instalación del servicio prepago.</li>
                  <li>Canalizar la solicitud a través de una Oficina Comercial de EDESUR.</li>
                </ol>
              </div>
            </dd>
          </dl>
        </div>

        <div class="content" id="es-faqs-content-medicion-neta">
          <dl class="accordion" data-accordion="">

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-01">1- ¿Qué es la Medición Neta?</a>
              <div id="es-faqs-content-medicion-neta-01" class="content active">
                <p>Proceso de medir de forma simultánea la energía consumida por el cliente de las redes de Distribuidor y la energía exportada por él a las redes del distribuidor, generada con Fuentes Renovables de Energía.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-02">2- ¿En qué consiste el Programa de Medición Neta?</a>
              <div id="es-faqs-content-medicion-neta-02" class="content">
                <p>Es un servicio provisto por el Distribuidor a los clientes con sistema de generación propia, que utilicen fuentes Renovables de energía, conectado a sus redes de distribución de acuerdo a lo dispuesto en el artículo 20 de la Ley 57-07.  Este servicio permite el flujo de electricidad hacia y desde las instalaciones del cliente a través del medidor de facturación bidireccional.  Al fin del periodo de facturación, el Distribuidor cobrará el consumo neto del cliente, o acreditará el exceso por exportación de energía.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-03">3- ¿Cuáles son los requisitos para ingresar al programa de Medición Neta?</a>
              <div id="es-faqs-content-medicion-neta-03" class="content">
                <p>El cliente debe entregar su solicitud para ingresar al programa de medición neta conjuntamente con los siguientes documentos:</p>
                <ul>
                  <li>Certificación de cumplimiento del equipo con los requisitos mínimos de eficiencia que establezca la Comisión Nacional de Energía (CNE) y la Superintendencia de Electricidad (SIE), según aplique.</li>
                  <li>Certificación de que la instalación la realizó un ingeniero colegiado</li>
                  <li>Evidencia de que el equipo estará garantizado por cinco años o más por el fabricante o distribuidor.</li>
                  <li>Copia del Acuerdo de interconexión vigente.</li>
                </ul>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-04">4- Si el cliente no cumple con todos los requisitos al momento de su solicitud, ¿cuánto tiempo tiene para completar la misma?</a>
              <div id="es-faqs-content-medicion-neta-04" class="content">
                <p>Si el cliente no cumple con lo requerido, se le entregará una notificación con lo faltante y a partir de dicha notificación, tendrá 30 días para completar su solicitud. De lo contrario deberá presentar una nueva solicitud.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-05">5- ¿En qué se basa la facturación para un cliente de Medición Neta?</a>
              <div id="es-faqs-content-medicion-neta-05" class="content">
                <p>La facturación de un cliente de medición neta se realizará en base a la energía neta, es decir, es el resultado de restar la energía recibida por el cliente y la energía entregada por éste al sistema.</p>
              </div>
            </dd>

            <dd class="accordion-navigation">
              <a href="#es-faqs-content-medicion-neta-06">6- ¿Qué sucede cuando la energía suministrada a la red por el cliente es mayor a la consumida?</a>
              <div id="es-faqs-content-medicion-neta-06" class="content">
                <p>Esta energía  se irá acreditando al cliente mes tras mes y si al final del cierre del periodo de facturación el crédito no ha sido utilizado, se compensará al cliente el 75% del crédito acumulado antes del 31 de enero de cada año.</p>
                <p>Para dicho pago se tomará en cuenta el precio del primer rango de consumo de energía del Bloque Tarifario BTS1 correspondiente a las tarifas aplicables a los usuarios del servicio público vigente para el mes de diciembre del año transcurrido y emitida por la SIE.</p>
              </div>
            </dd>
          </dl>
        </div>

        <?php
          /*<div class="content" id="es-faqs-content-contratacion-express">
          <?php /*get_template_part( 'includes/es-comercial/es-comercial-contratacion-express-faq' );*/ ?>
        </div>*/
         ?>



        <?php
          /*
            <!-- <div class="content" id="es-faqs-content-[type-question-content]">
              <dl class="accordion" data-accordion="">

                <dd class="accordion-navigation">
                  <a href="#es-faqs-content-[type-question-content]-00"></a>
                  <div id="es-faqs-content-[type-question-content]-00" class="content"></div>
                </dd>
              </dl>
            </div> -->
          */
        ?>

     </div>
   </div>
 </div>
</section>