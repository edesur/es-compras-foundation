<section class="es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title">Abrir un Nuevo Contrato</h3>
    </div>
  </div>

  <div class="row">
    <div class="es-site-section-wrapper-background small-12 columns">
      <p>Para obtener el servicio de electricidad, el solicitante debe ser la Persona Física o Jurídica que acredite titularidad del inmueble o que demuestre que ha estado usufructuando y/o pagando el servicio, o la persona o entidad que se beneficiará o usufructuará del suministro.</p>

      <p>El solicitante deberá suscribir un contrato de suministro de energía eléctrica y pagar el depósito de fianza o garantía, establecido en el contrato. Este depósito será rembolsado o aplicado al pago de los balances adeudados al término del contrato.</p>

      <p>El solicitante no debe tener deudas pendientes con EDESUR ni con otras Empresas de Distribución. Cuando el solicitante presente un historial de pago irregular, EDESUR podrá requerir un fiador o garante solidario.</p>

      <h4>Documentos requeridos para obtener el servicio:</h4>

      <p><strong>Para Persona Física:</strong></p>
      <ol>
        <li>Copia de la <strong>cédula de identidad</strong> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>

        <li>La documentación que <strong>acredite la titularidad del inmueble</strong> entre los cuales tenemos: El título de propiedad del inmueble o certificación de la Dirección General de Catastro que acredite la Posesión o tenencia del inmueble.</li>

        <li>Contrato de alquiler a nombre del solicitante debidamente notariado.</li>

        <li>En caso de existir deuda se le podría solicitar (opcional) que el contrato de alquiler este registrado ante el Banco Agrícola, así como la copia del título de propiedad de ese inmueble.</li>

        <li>Contrato  de préstamo de vivienda debidamente notariado.</li>

        <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta vigente debidamente notariada.</li>

        <li>En caso de inmuebles construidos en terrenos del Estado Dominicana: Declaración jurada ante siete (7) testigos debidamente notariada y registrada ante el registro civil, documento que acredite el uso legítimo del inmueble.</li>

        <li>En caso de que el titular de inmueble se deba hacer representar deberá hacerlo mediante poder especial debidamente notariado.  El representante también deberá aportar copia de su cédula.</li>
      </ol>

      <p><strong>Personas Jurídicas, además de los requisitos anteriormente especificados se requerirá:</strong></p>
      <p>Copia certificada de los documentos constitutivos de la empresa.  Entre estos:</p>

      <ol>
        <li>Acta del Consejo que avale al representante legal.</li>

        <li>Certificación o Tarjeta del Registro Nacional de Contribuyente (RNC).</li>

        <li>Certificación del registro mercantil al día</li>

        <li>Copia de la <strong>cédula de identidad</strong> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal de la persona que actué como representante.</li>

        <li>En caso de apoderado, el poder debidamente notariado y copia de cedula.</li>

        <li>Carta membretada y sellada de la razón social solicitando el suministro.</li>

        <li>Sello físico de la empresa o institución</li>
      </ol>

      <p>
        <strong>Áreas comunes:</strong><br>
        <strong>Los que estén constituidos:</strong>
      </p>

      <ol>
        <li>Acta de constitución del edificio.</li>
        <li>Certificación del Registro Nacional de contribuyente (RNC).</li>
        <li>Ultima acta de Asamblea.</li>
        <li>Copia de cedula del solicitante.</li>
        <li>Copia de la cédula de identidad del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>
        <li>Poder de representación debidamente notariada en caso de hacerse representar, y copia de la cedula del mismo.</li>
        <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta vigente debidamente notariada.</li>
      </ol>

      <p><strong>Los que no estén constituidos:</strong></p>

      <ol>
        <li>Acta de representación debidamente notariada y firmada por cada uno de los moradores del edificio.</li>

        <li>Título de propiedad, contrato de venta o alquiler de los propietarios y el solicitante del servicio para el área común.</li>

        <li>Copia de la cédula de identidad del solicitante y cada uno de los firmantes en el acta de asamblea. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>

        <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta vigente debidamente notariada.</li>
      </ol>
    </div>
  </div>

</section>