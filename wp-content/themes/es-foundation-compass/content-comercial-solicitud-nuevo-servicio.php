<div class="es-site-comercial-nuevo-servicio es-site-section-wrapper small-12 columns">

<div class="row medium-collapse">
  <h3>Solicitud de Nuevo Servicio</h3>
</div>

<div class="row">
   <div class="es-site-section-wrapper-background small-12 columns">

     <?php
       if (file_exists(ABSPATH . 'local-config.php')) {
        if ( function_exists( 'ninja_forms_display_form' ) ) {ninja_forms_display_form( 35 ); }
       } else {
         if ( function_exists( 'ninja_forms_display_form' ) ) {ninja_forms_display_form( 26 ); }
       }

     ?>

     <?php get_template_part('/includes/es-comercial/es-comercial-nuevo-servicio-req') ?>

   </div>
</div>


</div>