<section class="es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title">Historia</h3>
    </div>
  </div>

  <div class="row">
    <div class="es-site-empresa-historia es-site-section-wrapper-background small-12 medium-12 columns">

        <div class="row">
          <div class="small-12 large-8 columns">
            <p>EDESUR, S.A., es una de las empresas estatales distribuidoras de electricidad de República Dominicana.</p>
            <p>Tiene un área de concesión que se inicia en la acera oeste de la Avenida Máximo Gómez, en el Distrito Nacional y termina en la provincia fronteriza de Elías Piña. Tiene su origen en el conjunto de medidas que adoptó el presidente Leonel Fernández en su primera administración para superar la crisis eléctrica que afectaba el desarrollo de la vida nacional.</p>
            <p>El 24 de junio de 1997, el Jefe del Estado promulgó la ley 141-97, de Reforma de la Empresa Pública. El servicio de energía eléctrica, originalmente manejado por empresas extranjeras, desde que fue adquirido por el Estado en 1955, había sido administrado como un instrumento político, condicionado a prácticas permisivas y clientelistas que distorsionaron su naturaleza comercial.</p>
            <p>Apoyado en la nueva legislación y la ley Orgánica de la Corporación Dominicana de Electricidad, la número 4115 del 21 de abril de 1955, el presidente Fernández emitió el decreto 464-98 del 13 de diciembre de 1999, en virtud del cual autorizó a la Corporación Dominicana de Electricidad a aportar los activos de su propiedad, seleccionados por la Comisión de Reforma de la Empresa Pública, para la integración del capital pagado de las cinco nuevas sociedades anónimas a ser constituidas de conformidad con las disposiciones de la nueva Ley General de Reforma de la Empresa Pública, es decir: Empresa Generadora de Electricidad HAINA, S.A.; Empresa Generadora de Electricidad ITABO, S.A.; Empresa Distribuidora de Electricidad del Norte, S.A.; Empresa Distribuidora de Electricidad del Sur, S.A.; y Empresa Distribuidora de Electricidad del Este, S.A. En septiembre de 2003, el Gobierno del presidente Hipólito Mejía compró las acciones de Unión Fenosa en EDESUR y EDENORTE, lo que provocó un retroceso que agravó la situación de las compañías, pues se establecieron los apagones financieros.</p>
            <p>Las empresas distribuidoras estatizadas aumentaron el flujo negativo de caja y el Gobierno no suministró los recursos requeridos para cubrir el déficit. En consecuencia, el 16 de agosto de 2004, casi la mitad de la electricidad que se producía se perdía o no se pagaba. La recuperación de la calidad del servicio se evidencia en los circuitos 24 horas que garantizan energía permanente a los sectores que colaboran con la empresa hasta lograr reducir las pérdidas en por lo menos un 85 por ciento. En 2004 no había ningún circuito 24 horas. En la actualidad hay cientos de circuitos del área de concesión de EDESUR que reciben energía las 24 horas del día. Esto ha sido posible con acuerdos entre EDESUR y las comunidades que asumen la responsabilidad de pagar su consumo de energía y rechazar las distintas formas de fraude.</p>
          </div>
          <aside class="es-site-empresa-historia-images small-12 large-4 columns">
          <a class="th" href="<?php echo get_template_directory_uri(); ?>/images/es-site-history.jpg" title="Edesur Dominicana - Torre Serrano"><img src="<?php echo get_template_directory_uri(); ?>/images/es-site-history.jpg" alt="Edesur Dominicana - Torre Serrano"></a>
          </aside>
        </div>


    </div>
  </div>

</section>