<?php /*get_template_part( 'content', 'mercadeo-prende-bombillo-new-layout' );*/ ?>
<?php /*get_template_part( 'content', 'mercadeo-prende-bombillo-static' );*/ ?>
<?php /*get_template_part( 'content', 'mercadeo-prende-bombillo-acf' );*/ ?>

<div class="es-site-mercadeo-prende-bombillo-info column th">

  <div class="small-12 medium-6 columns text-center">
  <img src="<?php echo get_template_directory_uri() . '/images/es-site-mercadeo-prende-bombillo-logo.png' ?>">

  </div>
  <div class="small-12 medium-6 columns">
    <h2><span>Prende el Bombillo</span></h2>
    <h3 class="text-center"><a href="#es-site-mercadeo-prende-bombillo-form-anchor" class="button round">Inscríbete, Crea y Gana con EDESUR</a></h3>
    <p>Si eres estudiante universitario de las carreras de publicidad y mercadeo, ven demuestra tu talento y creatividad desarrollando una campaña que ayude a impulsar uno de los tres indicadores estratégicos de EDESUR y tendrás la oportunidad de ganar <span class="es-site-mercadeo-prende-bombillo-prize">RD$250,000</span>.</p>
  </div>
</div>

<div class="es-site-mercadeo-prende-bombillo row">

  <div class="es-site-mercadeo-prende-bombillo-sidebar small-12 medium-3 columns">
    <img class="th" src="<?php echo get_template_directory_uri(); ?>/images/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
    <p>Inscríbete, crea y gana hasta <span class="es-site-mercadeo-prende-bombillo-prize">RD$250,000</span>. Demuestra tu talento y creatividad desarrollando una campaña para EDESUR!</p>

    <p>EDESUR, toda nuestra energía para que vivas mejor.</p>
  </div>

  <div class="small-12 medium-9 columns">
    <h3 id="es-site-mercadeo-prende-bombillo-form-anchor" class="text-center">Formulario de inscripción</h3>

    <?php if( function_exists( 'ninja_forms_display_form' ) ){ninja_forms_display_form( 20 ); } ?>

  </div>
</div>

<?php /*get_template_part( 'content', 'mercadeo-prende-bombillo-acf' );*/ ?>

<div class="row">
  <div class="small-12 columns">

    <h3>¡FELICIDADES!</h3>
    <p>¡Ya estas participando! Tu número de equipo es XXX. Te retamos a ganar ¡Prende el Bombillo!</p>

    <?php /*the_content();*/ ?>

  </div>
</div>