<section class="es-site-section-consejo-admin es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title">Consejo de Administración</h3>
    </div>
  </div>

  <div class="row">
    <div class="es-site-section-wrapper-background small-12 columns">
      <p>El Consejo de Administración es el órgano que tiene a su cargo, la dirección administrativa de la Sociedad y está integrado por siete miembros: un Presidente, un Vice-Presidente, un Secretario y cuatro Vocales.</p>
      <p>Los miembros del Consejo de Administración son elegidos cada año y serán designados por la Asamblea General Ordinaria; pudiendo ser reelegidos una o más veces.</p>
      <p>En virtud de lo establecido en los Estatutos Sociales, las atribuciones, poderes y facultades que les son conferidas a los miembros del Consejo de Administración, en el desempeño de las funciones, podemos citar entre las más relevantes las siguientes:</p>

      <ol>
        <li>Autorizar aquellos contratos que no sean los destinados al normal desenvolvimiento de los negocios de la Sociedad, y que sean convenientes a sus propósitos;</li>
        <li>Manejar los fondos disponibles de conformidad con lo previsto en el Contrato de Administración;</li>
        <li>Delegar, a su sola discreción, por medio del Contrato de Administración previsto en el artículo 14 de la Ley 141-97 o por cualquier otro instrumento, a una o varias personas, la totalidad o parte de los poderes que le competen y, aun aquellos que estos Estatutos atribuyen a alguno de sus funcionarios.</li>
        <li>Designar los funcionarios de alta jerarquía de la Sociedad, tales como contadores, técnicos, asesores y administradores locales;</li>
        <li>Presentar a la Asamblea General de Accionistas el Informe de Gestión Anual;</li>
        <li>Adquirir por compra, permuta, arrendamiento o de cualquier otro modo, bienes muebles e inmuebles para los fines de la Sociedad;</li>
        <li>Decidir sobre la conveniencia de dictar un Reglamento de Licitación y la designación de un Comité de Compras para regular las compras de bienes que superen un monto por ellos establecido;</li>
        <li>Vender los bienes muebles o inmuebles de la Sociedad, así como darlos en arrendamiento, con las limitaciones establecidas en el artículo 38 de estos Estatutos;</li>
        <li>Delegar en otras personas su derecho de asistencia y voto en el Consejo de Administración. La delegación que se realice deberá constar en un documento debidamente legalizado por notario;</li>
        <li>Celebrar contratos u operaciones que impliquen gravamen, hipoteca o prenda, o cualquier otra afectación, sobre activos o derechos de la Sociedad;</li>
        <li>Celebrar contratos que impliquen prestación de servicios, préstamo de bienes o productos, u otros relacionados con el objeto social de la Sociedad;</li>
        <li>Decidir la colocación de los fondos iniciales de la Sociedad, resultantes del aporte del Accionista - Inversionista Estratégico y/o la Sociedad Suscriptora, en el desarrollo de la actividad eléctrica a la cual se dedique la sociedad de conformidad con el art. 2 de estos estatutos;</li>
        <li>Aprobar anualmente el plan general de inversión y el presupuesto, correspondientes a cada año, lo cual será obligatorio para que los administradores puedan realizar válidamente las ejecuciones presupuestarias.</li>
      </ol>
    </div>
  </div>

</section>