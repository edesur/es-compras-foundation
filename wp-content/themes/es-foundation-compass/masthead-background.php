<?php

  /**
 * Template Name: Masthead Background
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  if (is_page('prende-el-bombillo')) {
    acf_form_head();
  }

  get_header();

 ?>

<section class="es-site-masthead small-12 columns">

  <div class="row collapse">
   <div class="es-site-masthead-title small-12 columns">
     <h2><?php the_title(); ?></h2>
   </div>
  </div>

  <div class="row">
    <div class="es-site-masthead-form small-12 columns">

      <?php

        switch ($post->ID) {

          case is_page( 15 ):
           get_template_part( 'content', 'compras-proveedores-form' );
           break;

          case is_page( 18 ):
           get_template_part( 'content', 'servicios-fraude-form' );
           break;

          case is_page( 367 ):
           get_template_part( 'content', 'despacho-administrador' );
           break;

           case is_page( 'prende-el-bombillo' ):
            get_template_part( 'content', 'mercadeo-prende-bombillo' );
            break;

            case is_page( 'actualizacion-de-datos' ):
             get_template_part( 'content', 'comercial-actualizacion-datos' );
             break;

          default:
           # code...
           break;
        }
      ?>
    </div>
  </div>

 <?php get_template_part('/includes/es-bar-app') ?>
</section>

 <?php get_footer(); ?>