<div id="es-site-footer-contact" class="row small-collapse medium-uncollapse">
  <div class="small-12 columns">
    <h4>Contacto</h4>
  </div>


  <div class="small-12 medium-8 large-5 columns">

    <?php if( function_exists( 'ninja_forms_display_form' ) ){ninja_forms_display_form( 6 ); } ?>

  </div>

  <div class="es-site-footer-address medium-4 large-3 columns large-push-4" itemscope itemtype="http://schema.org/GovernmentService">
    <div itemprop="address">
      <!-- <h5>Dirección</h5> -->
      <ul class="no-bullet">
        <li><h5>Oficina Administrativa</h5></li>
        <li>Av. Tiradentes # 47</li>
        <li>Esq. Carlos Sánchez y Sánchez</li>
        <li>Torre Serrano</li>
        <li>Ensanche Naco</li>
        <li>Santo Domingo D.N.</li>
        <li>República Dominicana</li>
        <li itemprop="telephone"><a href="tel://+18096839292" >(809) 683-9292</a></li>
        <!-- <li>
          <time itemprop="openingHours" datetime="Mo-Fri">Lunes a Viernes</time>
        </li> -->
        <li>
          <time itemprop="openingHours" datetime="08:00-17:00">8:00 a.m. - 5:00 p.m.</time>
        </li>
        <li itemprop="telephone"><h5>Call Center</h5>
          <a href="tel://+18096839393" >(809) 683-9393</a>
        </li>
        <li>
          <time itemprop="openingHours" datetime="00:00-23:29">24 Horas</time>
        </li>
        <li><h5>Redes Sociales</h5></li>
        <li class="es-site-nav-addthis">
          <!-- Go to www.addthis.com/dashboard to customize your tools -->
          <div class="addthis_horizontal_follow_toolbox"></div>
        </li>
      </ul>

    </div>
  </div>

  <div class="es-site-footer-map-container medium-12 large-4 columns large-pull-3">
    <div id="es-site-footer-map-canvas" class="es-site-footer-map small-12 columns"></div>
  </div>

</div>