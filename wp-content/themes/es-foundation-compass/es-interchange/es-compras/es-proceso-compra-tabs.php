<ul class="es-site-compra-proceso-single-tabs tabs radius button-group" data-tab>
  <li class="tab-title active"><a class="button small" href="#<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_id][id]; ?>">Pliego</a></li>

  <?php if ($es_proceso_compra_files): ?>
    <li class="tab-title"><a class="button small" href="#<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_files][id]; ?>">Adjuntos</a></li>
  <?php endif ?>

  <?php if ($es_proceso_compra_faqs): ?>
    <li class="tab-title"><a class="button small" href="#<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_faqs][id]; ?>">Preguntas y respuestas</a></li>
  <?php endif ?>

  <?php if ($es_proceso_compra_amendment): ?>
    <li class="tab-title"><a class="button small" href="#<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_amendment][id]; ?>">Enmiendas/Adendas</a></li>
  <?php endif ?>
  <?php if ($es_proceso_compra_status != "En proceso" && $es_proceso_compra_status != "Enmendado"): ?>
    <li class="tab-title"><a class="button small" href="#<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_status][id]; ?>">
      <?php echo $es_proceso_compra_status_tab; ?>
    </a></li>
  <?php endif ?>

</ul>

<div class="tabs-content">
  <div class="content active" id="<?php /*echo $es_proceso_compra_fields[es_compras_proceso_compra_id][id];*/ ?>">

    <div class="row">
      <div class="large-3 columns">
        <h5>Código de proceso</h5>
      </div>
      <div class="large-9 columns">
        <h5><?php echo the_field( 'es_compras_proceso_compra_id' ); ?></h5>
      </div>
    </div>

    <div class="row">
      <div class="large-3 columns">
        <h5>Descripción detallada</h5>
      </div>
      <div class="large-9 columns">
        <p><?php echo $es_proceso_compra_desc ?></p>
      </div>
    </div>

    <div class="row">
      <div class="large-3 columns">
        <h5>Fecha de publicación</h5>
      </div>
      <div class="large-9 columns">
        <p>
          <?php
        // echo $es_proceso_compra_date_published->format('j \d\e F\, Y');
        echo $es_proceso_compra_date_published->format('d/m/Y'); ?>
        </p>
      </div>
    </div>

    <div class="row">
      <div class="large-3 columns">
        <h5>Fecha de Adjudicación</h5>
      </div>
      <div class="large-9 columns">
        <p>
          <?php
        echo $es_proceso_compra_date_allocation->format('d/m/Y'); ?>
        </p>
      </div>
    </div>

    <div class="row">
      <div class="large-3 columns">
        <h5>Contacto</h5>
      </div>
      <div class="large-9 columns">
        <p><?php echo $es_proceso_compra_contact ?></p>
      </div>
    </div>

    <div class="row">
      <div class="large-3 columns">
        <h5>Email</h5>
      </div>
      <div class="large-9 columns">
        <?php if( have_rows('es_compras_proceso_compra_email') ): ?>

          <ul class="inline-list">

          <?php while( have_rows('es_compras_proceso_compra_email') ): the_row();

            $es_proceso_compra_contact_email_address = get_sub_field( 'es_compras_proceso_compra_email_address' );
            ?>

            <li >
              <a class="button small secondary" href="mailto:<?php echo antispambot($es_proceso_compra_contact_email_address ); ?>"><?php echo antispambot($es_proceso_compra_contact_email_address ); ?></a>
            </li>

          <?php endwhile; ?>

          </ul>

        <?php endif; ?>

      </div>
    </div>


  </div>
  <div class="content" id="<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_files][id]; ?>">

    <?php if( have_rows($es_proceso_compra_fields[es_compras_proceso_compra_files][name]) ): ?>

      <ul class="es-site-compra-proceso-list small-block-grid-2" data-equalizer>
        <?php while( have_rows($es_proceso_compra_fields[es_compras_proceso_compra_files][name]) ): the_row();

          $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );

          // print_r($es_proceso_compra_file_single);
          ?>

          <li data-equalizer-watch>
            <a class="" href=""><?php echo $es_proceso_compra_file_single[title]; ?></a>
          </li>

        <?php endwhile; ?>
      </ul>

    <?php endif; ?>


    <p>This is the second panel of the basic tab example. This is the second panel of the basic tab example.</p>
  </div>
  <div class="content" id="<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_faqs][id]; ?>">
    <p>This is the third panel of the basic tab example. This is the third panel of the basic tab example.</p>
  </div>
  <div class="content" id="<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_amendment][id]; ?>">
    <p>This is the fourth panel of the basic tab example. This is the fourth panel of the basic tab example.</p>
  </div>
  <div class="content" id="<?php echo $es_proceso_compra_fields[es_compras_proceso_compra_status][id]; ?>">
    <p>This is the fourth panel of the basic tab example. This is the fourth panel of the basic tab example.</p>
  </div>

</div>