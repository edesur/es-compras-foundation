<h1><?php echo $_SERVER['DOCUMENT_ROOT'].'/edesur/es-foundation-compass-wp/wp-load.php'; ?></h1>

<?php require($_SERVER['DOCUMENT_ROOT'].'/edesur/es-foundation-compass-wp/wp-load.php'); ?>
<?php echo get_template_directory_uri(); ?>

<p data-interchange="[<?php echo get_template_directory_uri();?>/es-interchange/es-compras/es-proceso-compra-accordion.php, (small)], [<?php echo get_stylesheet_directory_uri();?>/es-interchange/es-compras/es-proceso-compra-tabs.php, (large)]"></p>

<ul class="es-site-compra-proceso-single-tabs radius button-group stack accordion" data-accordion>
  <li class="accordion-navigation active">
    <a class="es-accordion-nav-btn button small" href="#<?php echo $es_proceso_compra_fields['es_compras_proceso_compra_id']['id']; ?>">Pliego</a>

    <div class="content active" id="<?php echo $es_proceso_compra_fields['es_compras_proceso_compra_id']['id']; ?>">

      <div class="row">
        <div class="large-3 columns">
          <h5>Código de proceso</h5>
        </div>
        <div class="large-9 columns">
          <h5><?php echo the_field( 'es_compras_proceso_compra_id' ); ?></h5>
        </div>
      </div>

      <div class="row">
        <div class="large-3 columns">
          <h5>Descripción detallada</h5>
        </div>
        <div class="large-9 columns">
          <p><?php echo $es_proceso_compra_desc ?></p>
        </div>
      </div>

      <div class="row">
        <div class="large-3 columns">
          <h5>Fecha de publicación</h5>
        </div>
        <div class="large-9 columns">
          <p>
            <?php
          // echo $es_proceso_compra_date_published->format('j \d\e F\, Y');
          echo $es_proceso_compra_date_published->format('d/m/Y'); ?>
          </p>
        </div>
      </div>

      <div class="row">
        <div class="large-3 columns">
          <h5>Fecha de Adjudicación</h5>
        </div>
        <div class="large-9 columns">
          <p>
            <?php
          echo $es_proceso_compra_date_allocation->format('d/m/Y'); ?>
          </p>
        </div>
      </div>

      <div class="row">
        <div class="large-3 columns">
          <h5>Contacto</h5>
        </div>
        <div class="large-9 columns">
          <p><?php echo $es_proceso_compra_contact ?></p>
        </div>
      </div>

      <div class="row">
        <div class="large-3 columns">
          <h5>Email</h5>
        </div>
        <div class="large-9 columns">
          <?php if( have_rows('es_compras_proceso_compra_email') ): ?>

            <ul class="inline-list">

            <?php while( have_rows('es_compras_proceso_compra_email') ): the_row();

              $es_proceso_compra_contact_email_address = get_sub_field( 'es_compras_proceso_compra_email_address' );
              ?>

              <li>
                <a class="es-accordion-content-btn button small secondary" href="mailto:<?php echo antispambot($es_proceso_compra_contact_email_address ); ?>"><?php echo antispambot($es_proceso_compra_contact_email_address ); ?></a>
              </li>

            <?php endwhile; ?>

            </ul>

          <?php endif; ?>

        </div>
      </div>


    </div>
  </li>
  <li class="accordion-navigation">
    <a class="es-accordion-nav-btn button small" href="#panel2a">Accordion 2</a>
    <div id="panel2a" class="content">
      Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
  </li>
  <li class="accordion-navigation">
    <a class="es-accordion-nav-btn button small" href="#panel3a">Accordion 3</a>
    <div id="panel3a" class="content">
      Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
  </li>
</ul>