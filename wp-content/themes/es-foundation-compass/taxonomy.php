<?php

  /**
 * The template for displaying all single pages.
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  get_header();

  // get_template_part( '/includes/masthead-page' );

   get_template_part( 'es-compras/_content', 'tax-type' );

 ?>

<?php get_footer(); ?>