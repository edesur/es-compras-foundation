<?php

  $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
  $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
  $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
  $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

  $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

  $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

  $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

  $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');

?>