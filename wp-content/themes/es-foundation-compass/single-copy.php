<?php
  get_header();
  get_template_part( '/includes/masthead-page' );
  the_post();
?>

<section class="es-site-noticias-articulo small-12 medium-12 large-12 columns">

  <div class="row">
    <article class="small-8 medium-8 large-8 columns">
      <?php the_content(); ?>
    </article>

    <aside class="es-site-noticias-relacionadas small-4 medium-4 large-4 columns">

      <h3><a href="#">Noticias Relacionadas</a></h3>
      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-acuerdo-neiba.jpg" alt="Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand"></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">EDESUR y la Coalición Neiba firman un acuerdo para mejorar el servicio</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-utiles-deportivos-sur.jpg" alt="Edesur ofrece orientaciones a clientes sobre áreas técnicas"></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">EDESUR obsequia útiles deportivos a clubes del Sur</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-rehab-redes-dn.jpg" alt="Edesur invertirá 121 millones en Rehabilitación de Redes en el Distrito Nacional"></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Edesur invertirá 121 millones en Rehabilitación de Redes en el Distrito Nacional</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-reduce-perdidas.jpg" alt="Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand"></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand</a></h4>
          </div>
        </div>
      </article>
    </aside>

    <aside class="es-site-noticias-relacionadas small-4 medium-4 large-4 columns">

      <h3><a href="#">Videos de Edesur</a></h3>
      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-video-redes-independencia.jpg" alt=""></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Edesur rehabilita redes en barrios Avenida Independencia</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-video-circuito-24.jpg" alt=""></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Edesur inaugura circuitos 24 horas</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-video-engombe-uasd.jpg" alt=""></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Circuito 24 horas, Engombe UASD</a></h4>
          </div>
        </div>
      </article>

      <article class="es-site-noticias-relacionadas-articulo">
        <div class="row">
          <div class="small-4 medium-4 large-4 columns">
            <a href="#"><img src="images/es-site-noticias-video-los-coroneles.jpg" alt=""></a>
          </div>
          <div class="small-8 medium-8 large-8 columns">
            <h4><a href="#">Circuito 24 Horas, Los Coroneles de Herrera</a></h4>
          </div>
        </div>
      </article>
    </aside>
  </div>

</section>

<?php get_footer(); ?>