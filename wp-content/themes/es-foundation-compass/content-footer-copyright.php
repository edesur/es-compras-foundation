<div class="row small-collapse medium-uncollapse">

  <div class="es-site-footer-copyright large-12 columns text-center">

    <h2>EDESUR Dominicana</h2>

    <div class="es-site-footer-address medium-12 columns" itemscope="" itemtype="http://schema.org/GovernmentService" style="padding-top: 21px;">
        <div itemprop="address">
          <ul class="no-bullet">
            <li><h4>Empresa Distribuidora de Electricidad del Sur (EDESUR)</h4></li>
            <li>Av. Tiradentes #47 Esq. Carlos Sánchez y Sánchez, Torre Serrano, Ensanche Naco,</li>
            <li>Santo Domingo, D.N., República Dominicana <a href="tel://+18096839292" itemprop="telephone">(809) 683-9292</a>, Call Center <a href="tel://+18096839393">(809) 683-9393</a></li>

              <?php function auto_copyright($year = 'auto'){ ?>
                 <?php if(intval($year) == 'auto'){ $year = date('Y'); } ?>
                 <?php if(intval($year) == date('Y')){ echo intval($year); } ?>
                 <?php if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); } ?>
                 <?php if(intval($year) > date('Y')){ echo date('Y'); } ?>
              <?php } ?>
            <li>
              &copy;<?php auto_copyright(); ?> Todos los derechos reservados
            </li>
          </ul>

        </div>
      </div>
  </div>
</div>