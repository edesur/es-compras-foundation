<div class="es-site-admin-office row">
  <div class="small-12 medium-4 columns">
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/es-site-administrador-ruben-montaz.jpg" alt="">
  </div>
  <div class="small-12 medium-8 columns">
    <blockquote>
      <p>Con el lanzamiento de esta nueva página web de servicio al cliente, nuestra empresa EDESUR Dominicana, desea reafirmar el compromiso de reforzar los lazos fraternos que nos unen a nuestros clientes.</p>

      <p>Esta nueva página brindará a todos ustedes la oportunidad de ejecutar desde la comodidad de sus casas o trabajos, las operaciones básicas que hoy en día realizan en nuestras oficinas comerciales y nuestro norte, es que en un futuro no lejano, puedan realizar todas las transacciones a través de nuestro portal de internet.</p>

      <p>Este nuevo producto, se enmarca en uno de nuestro pilares fundamentales que es la mejora del servicio al cliente.</p>

      <p>Bienvenidos a esta nueva experiencia digital, esperamos que nuestro portal les sea útil y que lo disfruten.</p>
    </blockquote>

    <ul class="no-bullet">
      <li>Ing. Rubén Montás</li>
      <li>Administrador Gerente General</li>
      <li>Edesur Dominicana</li>
    </ul>
  </div>
</div>