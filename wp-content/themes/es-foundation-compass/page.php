<?php

  /**
 * The template for displaying all single pages.
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  get_header();

  get_template_part( '/includes/masthead-page' );

 ?>

<?php

  switch ($post->ID) {

    case is_page( 13 ):
      get_template_part( 'content', 'compras-normativas' );
      break;

    case is_page( 26 ):
      get_template_part( 'content', 'empresa' );
      break;

    case is_page( 4 ):
      get_template_part( 'content', 'empresa-mision' );
      break;

    case is_page( 8 ):
      get_template_part( 'content', 'empresa-historia' );
      break;

    case is_page( 11 ):
      get_template_part( 'content', 'empresa-consejo' );
      break;

    case is_page( 'ley-general-de-electricidad' ):
      get_template_part( 'content', 'legal-ley-electricidad' );
      break;

    case is_page( 28 ):
      get_template_part( 'content', 'noticia' );
      break;

    case is_page( 80 ):
      get_template_part( 'content', 'nuevo-contrato' );
      break;

    case is_page( 53 ):
      get_template_part( 'content', 'preguntas-frecuentes-static' );
      break;

    case is_page( 46 ):
      get_template_part( 'content', 'recursos-humanos-nomina' );
      break;

    case is_page( 'encuesta-himno' ):
      get_template_part( 'content', 'recursos-humanos-encuesta-himno' );
      break;

    case is_page( 'puntos-de-pago' ):
      get_template_part( 'content', 'servicios-puntos-pago' );
      break;

    case is_page('mantenimientos'):
      get_template_part( 'content', 'interrupcion' );
      break;

      case is_page('formulario-enviado'):
      get_template_part( 'content', 'forms-confirmation' );
      break;

      case is_page('solicitud-nuevo-servicio'):

      get_template_part( 'content', 'comercial-solicitud-nuevo-servicio' );

      // if (!is_super_admin() ) {
      //   get_template_part( 'content', 'comercial-contratacion-express-phone' );
      // } else {
      //   get_template_part( 'content', 'comercial-solicitud-nuevo-servicio' );
      // }

      break;

      case is_page('solicitud-nuevo-servicio-enviada'):
        get_template_part( 'content', 'forms-confirm-nuevo-servcio' );
      break;

      case is_page('contratacion-express'):
        // get_template_part( 'content', 'comercial-contratacion-express' );
        get_template_part( 'content', 'comercial-contratacion-express-phone' );
      break;

    default:
      # code...
      break;
  }
?>

<?php get_footer(); ?>