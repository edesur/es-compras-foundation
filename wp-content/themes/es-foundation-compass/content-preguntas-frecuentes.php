<?php

  $args = array( 'post_type' =>  'pregunta', /*'orderby' => 'date', 'order' => 'DESC',*/ 'nopaging' => true );

  $es_query = new WP_Query( $args );
?>

<section class="es-site-section-wrapper small-12 columns" data-equalizer>

 <div class="row">
   <div class="small-12 columns">


      <dl class="tabs" data-tab>

        <?php if ( have_posts() ) : while ( $es_query->have_posts() ) : $es_query->the_post(); ?>



          <dd class=""><a href="#post-<?php the_id(); ?>"><?php the_title(); ?></a>
          </dd>

        <?php endwhile; else : ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
      </dl>



     <div class="tabs-content">

      <?php /*$fields = get_field_objects();*/ ?>

      <?php /*foreach ($fields['es_preguntas_fecuentes']['sub_fields'] as $field):*/ ?>


        <div><?php /*printr($field, false);*/ ?></div>

      <?php /*endforeach*/ ?>

      <?php if ( have_posts() ) : while ( $es_query->have_posts() ) : $es_query->the_post(); ?>

        <?php

          // $fields = get_field_objects();

          // if( $fields )
          // {
          //   foreach( $fields as $field_name => $field )
          //   {
          //     echo '<div>';
          //       echo '<h3>' . $field['label'] . '</h3>';
          //       printr($field, false);
          //     echo '</div>';
          //   }
          // }


         ?>

        <div class="content" id="post-<?php the_id(); ?>">
          <dl class="accordion" data-accordion="">

            <?php $fields = get_field_objects(); ?>

            <?php /*printr($fields['es_preguntas_fecuentes'], false);*/ ?>

            <?php if ($fields): ?>

              <?php foreach ($fields as $field_name => $field): ?>


                <div><?php /*printr($field['sub_fields']['key'], false);*/ ?></div>
                <div><?php /*printr($field, false);*/ ?></div>

              <?php endforeach ?>

            <?php else: ?>

            <?php endif ?>




            <?php if (have_rows( 'es_preguntas_fecuentes')): ?>





              <?php /*printr($fields['es_preguntas_fecuentes'], false);*/ ?>

              <?php while(have_rows( 'es_preguntas_fecuentes' )): the_row(); ?>


                <dd class="accordion-navigation">
                  <a href="#panel1b"><?php the_sub_field('es_preguntas_fecuentes_pregunta'); ?></a>
                  <div id="panel1b" class="content"><?php the_sub_field('es_preguntas_fecuentes_repuesta'); ?></div>


                </dd>
              <?php endwhile; ?>

            <?php else: ?>

            <?php endif ?>
          </dl>
        </div>

      <?php endwhile; else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

     </div>
   </div>
 </div>
</section>