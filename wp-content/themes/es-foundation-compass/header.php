<?php
  //  Beter var_dump
   function printr($data, $exit = TRUE) {
     if ($data) {
       print '<pre>';
       print_r($data);
       print '</pre>';
     }
     if ($exit) {
       exit;
     }
   }
 ?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="shortcut icon" href="images/favicon.ico"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
    <!-- <title>Edesur Dominicana</title> -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
    <?php if (is_page(370)) {
      echo '<style type="text/css">
        html, body {
            height: 100%;
            margin: 0px;
        }
        .container {
            height: 100%;
            background: #f0e68c;
        }
      </style>';
    } ?>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- EDESUR Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61029108-1', 'auto');
      ga('send', 'pageview');

    </script> <!-- EDESUR Google Analytics -->

  </head>
  <body>

  <?php get_template_part('includes/es-site-nav-top'); ?>