<div class="row">
 <div class="small-12 columns">
   <h3>Realizar Denuncia</h3>
 </div>
</div>

<div class="row">

 <div class="small-12 large-4 columns">
   <p>Su denuncia y las informaciones suministradas a nosotros para esta finalidad, son estrictamente confidenciales. Por lo que puede realizar su denuncia sin temor a represalias de ningún tipo. Es el deber de cada ciudadano colaborar con eliminar el fraude eléctrico, colaborando así con la mejora continua del servicio en diferentes sectores de toda nuestra área de concesión.</p>
   <p>Analizar las denuncias es una labor ardua, por favor, evitenos retrasos en las investigaciones con reportes que no son reales. Estas solicitudes son analizadas con estricta confidencialidad dentro de nuestra unidad corporativa, y luego remitidas a la institución correspondiente, por lo que garantizamos su anonimato para proteger su identidad.</p>

   <p>Por favor, complete esta solicitud para realizar su denuncia por correo electrónico.</p>
 </div>

 <div class="small-12 large-8 columns">

   <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 8 ); } ?>

 </div>
</div>