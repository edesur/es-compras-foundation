<div class="es-site-comercial-contratacion-express es-site-section-wrapper small-12 columns">

<!-- <div class="row medium-collapse">
  <h3>Solicitud de Nuevo Servicio</h3>
</div> -->

<div class="row">
   <div class="small-12 large-6 columns">
     <div class="es-site-contratacion-express-tagline panel">

       <img src="<?php echo get_template_directory_uri() . '/images/es-site-logo-comercial-contratacion-express.png' ?>" alt="">
       <h2 class="text-center">¡Servicio de contratación<br> donde quiera que estés!</h2>

     </div>
   </div>
   <div class="small-12 large-4 columns end">
      <h3>Con Contratación Express</h3>
      <ul>
        <li>Ahorras tiempo y dinero</li>
        <li>Recibes atención personalizada</li>
        <li>Nos ajustamos a tu tiempo y al lugar que decidas</li>
        <li>Siempre sabrás el estado de tu solicitud</li>
        <li>Firmas el contrato en el lugar de tu preferencia</li>
        <li>La Instalación es en menos de 48 hrs</li>
      </ul>

      <h4>Fácil de solicitar:</h4>
      <ul class="no-bullet">
        <li>Llamando al <a href="tel://+18096839393">Call Center</a> 24 horas <a href="tel://+18096839393">(809) 683-9393</a></li>
        <li>En cualquiera de nuestras oficinas comerciales.</li>
      </ul>
      <p><a href="<?php echo site_url(); ?>/preguntas-frecuentes">Preguntas frecuentes</a> sobre Contratación Express</p>
   </div>
</div>


</div>