<?php get_header(); ?>

  <section class="es-site-masthead-home es-site-masthead-home-temp es-site-masthead columns">

    <div class="row text-center">
      <div class="es-site-masthead-title small-12 large-6 columns large-centered">
        <h2>Desde hoy, &iexcl;Vive la experiencia!</h2>
          <!-- <div class="row"> -->

          <!-- </div> -->
        <h3 class="subheader">7:00PM</h3>

        <a href="https://www.edesur.com.do/ov/" class="es-site-masthead-cta button radius" target="_blank">Oficina Virtual</a>

      </div>
      <div class="small-12 columns">

      </div>
    </div>

<?php get_footer(); ?>