<ul class="accordion" data-accordion role="tablist" style="border-top: none">

  <?php foreach ( $es_proceso_compra_years as $es_proceso_compra_year ): ?>

    <?php
      if ( is_post_type_archive() ) {
       $es_proceso_compra_query_args = array(
          'year' => $es_proceso_compra_year,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1,
       );
      } elseif ( is_tax() ) {

        $es_proceso_compra_query_date_array = array(
          'taxonomy' => $es_proceso_compra_obj->taxonomy,
          'terms' => $es_proceso_compra_obj->term_id
        );

        $es_proceso_compra_query_tax_array = array(
          'taxonomy' => $es_proceso_compra_obj->taxonomy,
          'terms' => $es_proceso_compra_obj->term_id
        );

       $es_proceso_compra_query_args = array(
           'year' => $es_proceso_compra_year,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1,
         'tax_query' => array($es_proceso_compra_query_tax_array)
       );
      }

      $es_proceso_compra_query_year = new WP_Query( $es_proceso_compra_query_args );
    ?>

    <?php
      $es_class_accordion = ( $es_proceso_compra_year === reset($es_proceso_compra_years) ) ? 'accordion-navigation active' : 'accordion-navigation';
      $es_class_accordion_content = ( $es_proceso_compra_year === reset($es_proceso_compra_years) ) ? 'content active' : 'content';
      $es_proceso_compra_count_label = ($es_proceso_compra_query_year->post_count > 1) ? 'Procesos' : 'Proceso' ;
    ?>

    <li class="<?php echo $es_class_accordion; ?>" style="border-top: 1px solid #c0c0c0;">

      <a href="#panel_<?php echo $es_proceso_compra_year; ?>">
        <h4><?php echo $es_proceso_compra_year; ?> <small><?php echo $es_proceso_compra_query_year->post_count . ' ' . $es_proceso_compra_count_label; ?></small></h4>
      </a>
      <div id="panel_<?php echo $es_proceso_compra_year; ?>" class="<?php echo $es_class_accordion_content; ?>">

        <?php
          if ( $es_proceso_compra_query_year->have_posts() ) {

            $es_proceso_compra_months = array();

            foreach ($es_proceso_compra_query_year->posts as $es_proceso_compra_post) {

              $es_proceso_compra_months[] = date_format( date_create( $es_proceso_compra_post->post_date ), 'F' );

            }

            $es_proceso_compra_months = array_unique($es_proceso_compra_months);

            include(locate_template( '/es-compras/_content-tax-type-month.php'));
          }
        ?>

      </div>
    </li>

    <?php wp_reset_postdata(); // Reset Post Data ?>

  <?php endforeach ?>

</ul>