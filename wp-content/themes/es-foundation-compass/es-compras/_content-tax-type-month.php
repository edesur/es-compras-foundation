<ul class="accordion" data-accordion role="tablist">

  <?php foreach ($es_proceso_compra_months_new as $es_proceso_compra_month_new): ?>

    <?php

      $es_compra_month_new = date( 'm', $es_proceso_compra_month_new );
      $es_proceso_compra_month_i18n_new = date_i18n( 'F', $es_proceso_compra_month_new  );

      $es_proceso_compra_query_date_array = array(
        'year' => $es_proceso_compra_year,
        'month' => $es_compra_month_new
      );

      if ( is_post_type_archive() ) {
       $es_proceso_compra_query_args = array(
          'date_query' => $es_proceso_compra_query_date_array,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1
       );
      } elseif ( is_tax() ) {

        $es_proceso_compra_query_tax_array = array(
          'taxonomy' => $es_proceso_compra_obj->taxonomy,
          'terms' => $es_proceso_compra_obj->term_id
        );

       $es_proceso_compra_query_args = array(
         'date_query' => $es_proceso_compra_query_date_array,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1,
         'tax_query' => array($es_proceso_compra_query_tax_array)
       );
      }

      $es_proceso_compra_query_month = new WP_Query( $es_proceso_compra_query_args );

      // $m=1; $m<=12; ++$m
      // for($m=1; $m<=$es_loop_counter; ++$m){
      //   $es_proceso_compra_months_new[] = date('F', mktime(0, 0, 0, $m, 1));
      //     // echo '<pre>' . date('F', mktime(0, 0, 0, $m, 1)) .'<br></pre>';
      // }

      $es_class_accordion = ( $es_proceso_compra_month_new === reset($es_proceso_compra_months_new) ) ? 'accordion-navigation active' : 'accordion-navigation';
      $es_class_accordion_content = ( $es_proceso_compra_month_new === reset($es_proceso_compra_months_new) ) ? 'content active' : 'content';

      if ($es_proceso_compra_query_month->post_count > 1) {
        $es_proceso_compra_count_label = $es_proceso_compra_query_month->post_count . ' Procesos';
      } elseif ($es_proceso_compra_query_month->post_count == 1) {
        $es_proceso_compra_count_label = $es_proceso_compra_query_month->post_count . ' Proceso';
      }elseif ($es_proceso_compra_query_month->post_count < 1) {
        $es_proceso_compra_count_label = 'No hubo procesos realizados';
      }

    ?>
    <li class="<?php echo $es_class_accordion; ?>">

      <a href="#panel_<?php echo $es_proceso_compra_year . '_'. $es_compra_month_new; ?>">
        <h5><i class="fi-folder large" style="color: rgb(0, 45, 98); font-size: 1.5rem; margin-right: 0.5rem;"></i><?php echo $es_proceso_compra_month_i18n_new . ' ' . $es_proceso_compra_year; ?> <small><?php echo $es_proceso_compra_count_label; ?></small></h5>
      </a>
      <div id="panel_<?php echo $es_proceso_compra_year . '_'. $es_compra_month_new; ?>" class="<?php echo $es_class_accordion_content; ?>" style="padding: 0;">

        <?php include(locate_template( '/es-compras/_content-tax-type-index.php')); ?>
      </div>
    </li>

  <?php endforeach ?>

   <?php wp_reset_postdata(); // Reset Post Data ?>

</ul>