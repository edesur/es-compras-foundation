<ul class="accordion" data-accordion role="tablist">

  <?php foreach ( $es_proceso_compra_months as $es_proceso_compra_month ): ?>

    <?php
      $es_compra_month = date_format( date_create( $es_proceso_compra_month ), 'm' );
      $es_proceso_compra_month_i18n = date_i18n( 'F', strtotime( $es_proceso_compra_month ) );

      $es_proceso_compra_query_date_array = array(
        'year' => $es_proceso_compra_year,
        'month' => $es_compra_month
      );

      if ( is_post_type_archive() ) {
       $es_proceso_compra_query_args = array(
          'date_query' => $es_proceso_compra_query_date_array,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1
       );
      } elseif ( is_tax() ) {

        $es_proceso_compra_query_tax_array = array(
          'taxonomy' => $es_proceso_compra_obj->taxonomy,
          'terms' => $es_proceso_compra_obj->term_id
        );

       $es_proceso_compra_query_args = array(
         'date_query' => $es_proceso_compra_query_date_array,
         'post_type' => 'proceso_compra',
         'posts_per_archive_page'  => -1,
         'tax_query' => array($es_proceso_compra_query_tax_array)
       );
      }

      $es_proceso_compra_query_month = new WP_Query( $es_proceso_compra_query_args );
    ?>

    <?php
      $es_class_accordion = ( $es_proceso_compra_month === reset($es_proceso_compra_months) ) ? 'accordion-navigation active' : 'accordion-navigation';
      $es_class_accordion_content = ( $es_proceso_compra_month === reset($es_proceso_compra_months) ) ? 'content active' : 'content';
      $es_proceso_compra_count_label = ($es_proceso_compra_query_month->post_count > 1) ? 'Procesos' : 'Proceso' ;
    ?>

    <li class="<?php echo $es_class_accordion; ?>">
      <a href="#panel_<?php echo $es_proceso_compra_year . '_'. $es_compra_month; ?>">
        <h5><?php echo $es_proceso_compra_month_i18n . ' ' . $es_proceso_compra_year; ?> <small><?php echo $es_proceso_compra_query_month->post_count . ' ' . $es_proceso_compra_count_label; ?></small></h5>
      </a>
      <div id="panel_<?php echo $es_proceso_compra_year . '_'. $es_compra_month; ?>" class="<?php echo $es_class_accordion_content; ?>" style="padding: 0;">
        <?php include(locate_template( '/es-compras/_content-tax-type-index.php')); ?>
      </div>
    </li>

  <?php endforeach ?>

  <?php wp_reset_postdata(); // Reset Post Data ?>

</ul>