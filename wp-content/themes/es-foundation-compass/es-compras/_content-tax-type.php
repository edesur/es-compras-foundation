<?php global $wp_query;

  $es_proceso_compra_obj = get_queried_object();
  $es_proceso_compra_fields = get_field_objects();

$es_proceso_compra_type_terms_list = get_terms( array(
        'taxonomy' => get_object_taxonomies( get_post_type() ),
        'hide_empty' => false)
    );

  $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
  // $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

  $es_proceso_compra_status_terms_list = get_terms($es_proceso_compra_status_tax, array('hide_empty' => 1, 'post_status' => 'published')
);

  // if ( is_post_type_archive() ) {
  //  $es_proceso_compra_query_args = array(
  //    'post_type' => 'proceso_compra',
  //    'posts_per_archive_page'  => -1,
  //    // 'orderby'   => 'modified',
  //    // 'order'     => 'DESC'
  //  );
  // } elseif ( is_tax() ) {

  //   $es_proceso_compra_query_tax_array = array(
  //     'taxonomy' => $es_proceso_compra_obj->taxonomy,
  //     'terms' => $es_proceso_compra_obj->term_id,
  //     'hide_empty' => false
  //   );

  //  $es_proceso_compra_query_args = array(
  //    'post_type' => 'proceso_compra',
  //    'posts_per_archive_page'  => -1,
  //    'tax_query' => array($es_proceso_compra_query_tax_array)
  //  );
  // }

  $es_proceso_compra_query_args = array(
     'post_type' => 'proceso_compra',
     'posts_per_archive_page'  => -1,
     // 'orderby'   => 'modified',
     // 'order'     => 'DESC'
   );

  $es_proceso_compra_query = new WP_Query($es_proceso_compra_query_args);
?>

<section class="es-site-section-compras-procesos es-site-section-wrapper small-12 columns" data-equalizer>

<?php include(locate_template( '/es-compras/_content-notices.php')); ?>

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title"><?php
        if (is_post_type_archive()) {
          post_type_archive_title();
        } else {
          echo $es_proceso_compra_obj->name;
        }
       ?></h3>
    </div>
  </div>

  <div class="row">
    <div class="es-site-section-wrapper-background small-12 columns">

    <div class="es-site-compra-proceso-filter-buttons row">
      <div class="small-12 columns">
        <h5>Procesos por modalidad</h5>

        <ul class="button-group radius">

          <?php foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_term): ?>

            <?php $es_proceso_compra_type_term_link = get_term_link($es_proceso_compra_type_term); ?>

            <?php if ($es_proceso_compra_type_term->taxonomy == 'es_compras_proceso_type_tax'): ?>

              <li>
                <a href="<?php echo esc_url( $es_proceso_compra_type_term_link ); ?>" class="button tiny secondary">
                <?php echo $es_proceso_compra_type_term->name; ?>
                </a>
              </li>

            <?php endif ?>

          <?php endforeach ?>

        </ul>
      </div>
    </div>

      <div class="es-site-compra-proceso-filter-buttons row">
        <div class="small-12 columns" style="padding-right: 0; ">
          <h6>Procesos por estado</h6>
        </div>
        <div class="small-12 columns">

          <ul class="button-group radius">

            <?php foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_term): ?>

              <?php $es_proceso_compra_type_term_link = get_term_link($es_proceso_compra_type_term); ?>

              <?php if ($es_proceso_compra_type_term->taxonomy == 'es_compras_proceso_status_tax'): ?>

                <li>
                  <a href="<?php echo esc_url( $es_proceso_compra_type_term_link ); ?>" class="button tiny secondary">
                  <?php echo $es_proceso_compra_type_term->name; ?>
                  </a>
                </li>

              <?php endif ?>

            <?php endforeach ?>

          </ul>
        </div>
      </div>

      <div class="row">
        <div class="small-12 columns">

        <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );  ?>


        <?php


        ?>

        <?php
          $es_proceso_compra_years = array();

          $es_proceso_compra_years[] =  date('Y');

          foreach ($es_proceso_compra_query->posts as $es_proceso_compra_post) {

            $es_proceso_compra_years[] = date_format( date_create( $es_proceso_compra_post->post_date ), 'Y' );

          }

          $es_proceso_compra_years = array_unique($es_proceso_compra_years);

          include(locate_template( '/es-compras/_content-tax-type-year.php'));

        ?>

          <?php if ( $es_proceso_compra_query->have_posts() ) : ?>

            <?php /*include(locate_template( '/es-compras/_content-tax-type-year.php'));*/ ?>



             <?php

             // $big = 999999999; // need an unlikely integer

             // echo paginate_links( array(
             //   'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
             //   'format' => '?paged=%#%',
             //   'current' => max( 1, get_query_var('paged') ),
             //   'total' => $wp_query->max_num_pages,
             //   'prev_text'          => __('« Anterior'),
             //   'next_text'          => __('Siguiente »')
             // ) );

             ?>

           <?php   else: ?>

            <?php /*$es_proceso_compra_years[] =  date('Y');*/ ?>

            <?php /*include(locate_template( '/es-compras/_content-tax-type-year.php'));*/ ?>
              <!-- <h3>
                No se encontraron
                <?php
                  if (is_post_type_archive()) {
                    post_type_archive_title();
                  } else {
                    $es_proceso_compra_obj = $wp_query->get_queried_object();
                    echo "Procesos de compra: " . $es_proceso_compra_obj->name;
                  }
                ?>
              </h3> -->
            <?php endif; ?>

            <?php wp_reset_query(); ?>

        </div>
      </div>


    </div>
  </div>
</section>