<div class="row" data-equalizer>

  <div class="small-12 columns text-center" data-equalizer-watch>
    <div data-alert class="alert-box radius secondary" style="padding: 0.5em">
      <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 6px;">Licitación Pública Nacional</h4>
      <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-LPN-2018-019, Contratación del Servicio de Renta de Equipos de Impresión Multifuncional</h5>
    <p style="margin-bottom: 8px">Convocatoria a presentar Ofertas para la Contratación del Servicio de Renta de Equipos de Impresión Multifuncional NO.EDESUR-CCC-LPN-2018-019</p>
        <a style="margin-bottom: 0;" class="button primary tiny" href="http://edesur2.edesur.com.do/procesos-de-compras/convocatoria-a-presentar-ofertas-para-la-contratacion-del-servicio-de-renta-de-equipos-de-impresion-multifuncional-no-edesur-ccc-lpn-2018-019/" title="EDESUR-CCC-LPN-2018-019, Contratación del Servicio de Renta de Equipos de Impresión Multifuncional">Mas detalles</a>
      </div>
  </div>

  <?php /*

  <div class="large-6 small-12 columns text-center" data-equalizer-watch>
      <div data-alert class="alert-box radius secondary" style="padding: 0.5em">
        <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 6px;">Licitación Pública Nacional</h4>
        <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-LPN-2018-011, Adquisición de Conductores, Medidores y Módulos de Transformación</h5>
      <p style="margin-bottom: 8px">Adquisición de Conductores, Medidores y Materiales para su instalación, Módulos de Transformación para medida indirecta y Materiales Varios.</p>
          <a style="margin-bottom: 0;" class="button primary tiny" href="http://edesur2.edesur.com.do/procesos-de-compras/adquisicion-de-conductores-medidores-y-materiales-para-su-instalacion-modulos-de-transformacion-para-medida-indirecta-y-materiales-varios/" title="EDESUR-CCC-LPN-2018-011, Adquisición de Conductores, Medidores y Módulos de Transformación">Mas detalles</a>
        </div>
    </div>

  */ ?>

</div>


<?php /*
  <div class="row">

    <div class="small-12 columns">
        <div data-alert class="alert-box secondary radius text-center">
          <h3 style="color:#fff; background: rgb(0, 45, 98); padding: 8px; border-bottom: 3px solid #f68934;">COMUNICADO</h3>
          <p style="margin: 16px;">Informamos a todos nuestros suplidores de Bienes y Servicios,  que nuestros almacenes estarán cerrados por motivos de inventario  de fin de año 2017, desde el 15 hasta el 28 de noviembre de 2017 y la fecha límite para la entrega de materiales será hasta el viernes 10 de noviembre de 2017.</p>
          <p style="margin: 16px;">Las operaciones de entrega y recepción de mercancía continuarán de forma ordinaria en nuestros almacenes a partir del 1ero de diciembre del 2017.</p>

        </div>
    </div>

  </div>

  <div class="row" data-equalizer>
    <div class="large-4 small-12 columns" data-equalizer-watch>
      <div data-alert class="alert-box radius secondary">
        <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
        <h5 style="color:rgb(0, 45, 98);">LPN-BID-01-2017, Adquisición de camiones, para habilitar oficinas móviles</h5>
      <p>Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)</p>
          <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/wp-content/uploads/2017/04/es-site-cdeee-licitacion-lpn-bdi-01-2017-camion.pdf" title="Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)">Mas detalles</a>
        </div>
    </div>

    <div class="large-4 small-12 columns" data-equalizer-watch>
        <div data-alert class="alert-box radius secondary">
          <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
          <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-PU-2017-003</h5>
          <p>Licitación Pública Nacional de plazos reducidos para la Contratación de los Servicios de Gestión Técnico Comercial en za Zona de concesión de EDESUR Dominicana, S.A.</p>
          <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/procesos-de-compras/licitacion-publica-nacional-de-plazos-reducidos-para-la-contratacion-de-los-servicios-de-gestion-tecnico-comercial-en-la-zona-de-concesion-de-edesur-dominicana-s-a-edesur-ccc-pu-201/" title="EDESUR-CCC-PU-2017-003, Licitación Pública Nacional de plazos reducidos para la “Contratación de los Servicios de Gestión Técnico Comercial en za Zona de concesión de EDESUR Dominicana, S.A.”">Mas detalles</a>
        </div>
    </div>

    <div class="large-4 small-12 columns" data-equalizer-watch>
        <div data-alert class="alert-box radius secondary">
          <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
          <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-LPN-2017-023</h5>
          <p>Rehabilitación y Normalización de Clientes Circuitos: CSAT102, PALA102, HANU101 y HANU102, GRBO102, GRBO102, Santo Domingo y Municipios Nigua y Hatillo, Provincia San Cristóbal</p>
          <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/procesos-de-compras/edesur-ccc-lpn-2017-023-para-la-contratacion-rehabilitacion-y-normalizacion-de-clientes-de-los-circuitos-csat102-pala102-hanu101-y-hanu102-grbo102-nigua-y-grbo102-hatillo-provinci/" title="EDESUR-CCC-LPN-2017-023, Rehabilitación y Normalización de Clientes de los Circuitos: CSAT102, PALA102, HANU101 y HANU102, GRBO102, GRBO102, Provincia Santo Domingo y Municipios Nigua y Hatillo, Provincia San Cristóbal">Mas detalles</a>
        </div>
    </div>
  </div>
*/ ?>