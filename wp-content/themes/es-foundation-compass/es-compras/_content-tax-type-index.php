<ul class="es-site-compra-proceso-list small-block-grid-1">

  <?php if ($es_proceso_compra_query_month->have_posts() ): ?>

       <?php while ( $es_proceso_compra_query_month->have_posts() ) : $es_proceso_compra_query_month->the_post(); ?>

        <?php

         // $es_proceso_compra_fields = get_field_objects();

         $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];
         $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

         $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
         $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

         $es_proceso_compra_files = $es_proceso_compra_fields['es_compras_proceso_compra_files']['name'];

         $es_proceso_compra_date_pub = get_field('es_compras_proceso_compra_date_published');

         $es_proceso_compra_date_pub_i18n = date_i18n( 'j \d\e F, Y', strtotime( $es_proceso_compra_date_pub ) );

         foreach ($es_proceso_compra_fields['es_compras_proceso_compra_files']['value'] as $es_proceso_compra_file) {

           $es_proceso_compra_single_file_type = $es_proceso_compra_file['es_compras_proceso_compra_file_type'];

           switch ($es_proceso_compra_single_file_type) {
             case 'Pliego':
               $es_proceso_compra_type_doc = $es_proceso_compra_single_file_type;
               break;

           case 'Adjuntos':
             $es_proceso_compra_type_attachment = $es_proceso_compra_single_file_type;
             break;

           case 'Enmiendas/Adendas':
               $es_proceso_compra_type_amendment = $es_proceso_compra_single_file_type;
               break;

           case 'Preguntas y respuestas':
               $es_proceso_compra_type_faq = $es_proceso_compra_single_file_type;
               break;

               case 'Carta adjudicataria':
               $es_proceso_compra_type_allocation = $es_proceso_compra_single_file_type;
               break;

               case 'Acto de cancelación':
               $es_proceso_compra_type_cancelation = $es_proceso_compra_single_file_type;
               break;

             default:
               # code...
               break;
           }
         }

         foreach ($es_proceso_compra_type_terms as $term) {
           $es_proceso_compra_type = $term->name;
         }

         // $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

         $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
         $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

         foreach ($es_proceso_compra_status_terms as $term) {
           $es_proceso_compra_status = $term->name;
         }

          switch ($es_proceso_compra_status) {

            case 'En proceso':
              $es_proceso_compra_status_class = 'label';
              break;

           case 'Enmendado':
              $es_proceso_compra_status_class = 'label warning';
              break;

            case 'Adjudicado':
              $es_proceso_compra_status_class = 'label success';
              break;

            case 'Desierto':
              $es_proceso_compra_status_class = 'label alert';
              break;

            default:
              // code...
              break;
          }


         ?>

      <?php if ($es_proceso_compra_id): ?>

        <li class="es-site-compra-proceso-list-single" >


          <div class="es-site-compra-proceso-list-single-wrapper small-12 columns">

           <div class="row">
             <div class="es-site-compra-proceso-list-single-wrapper-copy medium-12 columns">

               <h6 class="es-site-compra-proceso-list-single-title">
                <a href="<?php the_permalink(); ?>">
                 <?php the_title(); ?>
                </a>
               </h6>

             </div>
           </div>
            <div class="row">
              <?php /*
               <!-- <div class="es-site-compra-proceso-list-single-wrapper-th small-2 medium-1 columns text-center">
                 <a href="<?php the_permalink(); ?>" class="th">
                   <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                 </a>
               </div> -->
              */ ?>
              <div class="es-site-compra-proceso-list-single-wrapper-copy small-12 columns">

                <div class="row collapse">

                   <ul class="inline-list" style="margin-bottom: 0.5rem;">
                     <li class="es-site-compra-proceso-list-single-id" style="margin: 0;"><?php echo $es_proceso_compra_id_name; ?></li>
                     <li>Publicado: <?php echo $es_proceso_compra_date_pub_i18n; ?></li>
                   </ul>
                </div>

                <div class="row collapse">
                  <div class="small-12 medium-10 columns">

                    <ul class="es-compra-proceso-type inline-list">

                        <li class="text-center">
                         <span class="label info"><?php echo $es_proceso_compra_type ?></span>
                        </li>
                        <li>
                          <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                        </li>

                    </ul>

                  </div>

                  <?php while( have_rows($es_proceso_compra_files) ): the_row();

                    $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                    $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];

                    $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                    $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                  ?>

                  <?php endwhile; ?>

                  <div class="es-site-compra-proceso-list-single-download medium-2 small-3 columns clearfix">
                      <a class="es-site-compra-proceso-list-single-download-button button tiny expand right" href="<?php the_permalink(); ?>" class="button tiny info">Mas Detalles</a>

                  </div>


                </div>

              </div>

            </div>

          </div>

        </li>

      <?php endif ?>



    <?php endwhile; ?>

  <?php else: ?>

    <div class="es-site-compra-proceso-list-single-wrapper-copy small-12 columns">
      <div class="row">
        <div class="small-12 columns">
          <h5>Durante el mes de <?php echo $es_proceso_compra_month_i18n_new . ' ' . $es_proceso_compra_year; ?> no se realizó ningún Proceso de compra bajo esta modalidad</h5>
        </div>
      </div>
    </div>

  <?php endif ?>

 </ul>