<?php if ( is_search() ) : ?>

  <p><?php _e( 'No se encontraron resultados en la búsqueda. Por favor intente de nuevo utilizando diferentes términos o palabras claves.
', 'es-foundation-compass' ); ?></p>
  <div class="medium-4 columns">
    <?php  get_search_form(); ?>
  </div>

<?php else : ?>

  <p><?php _e( 'Al parecer no fue posible encontrar lo que busca, posiblemente utilizando diferentes términos o palabras claves tenga mejor resultado
', 'es-foundation-compass' ); ?></p>
    <div class="medium-4 columns">
      <?php  get_search_form(); ?>
    </div>

<?php endif; ?>