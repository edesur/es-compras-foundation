<div class="row small-collapse medium-uncollapse">
  <div class="large-10 large-centered columns">
    <ul class="es-site-footer-nav medium-block-grid-4" data-equalizer>
      <li data-equalizer-watch>
        <div class="small-12 columns">
          <h4><a href="">Servicios</a></h4>
          <ul class="side-nav disc">
            <!-- <li><a href="ov.html">Oficina Virtual</a></li>
            <li><a href="#">Nuevo Contrato</a></li>
            <li><a href="ov.html">Pago de Factura</a></li>
            <li><a href="#">Puntos de pago</a></li>
            <li><a href="#">Factura por E-mail</a></li> -->

            <li><a href="<?php echo get_page_link(80); ?>">Nuevo Contrato</a></li>
            <li><a href="https://www.edesur.com.do/ov/" title="Pago de Factura" target="_blank">Pago de Factura</a></li>
            <li><a href="<?php echo get_page_link(18); ?>">Denuncia de Fraude</a></li>
            <li><a href="<?php echo get_page_link($es_interrupciones_ID); ?>">Mantenimientos Programados</a></li>
            <li><a data-reveal-id="es-subsidio-iframe-modal">Subsidio</a></li>

          </ul>
        </div>
      </li>
      <li data-equalizer-watch>
        <div class="small-12 columns">
          <h4><a>Transparencia</a></h4>
          <ul class="side-nav disc ">
            <!-- <li><label>Compras & Contrataciones</label></li> -->
            <!-- <li><a href="<?php echo get_page_link(13); ?>">Normativas</a></li> -->
            <li><a href="<?php echo get_post_type_archive_link( 'proceso_compra' ); ?>">Procesos de compras</a></li>
            <!-- <li><label>Proveedores</label></li> -->
            <li><a href="<?php echo get_page_link(15); ?>">Formulario de Proveedores</a></li>
            <!-- <li><label>Recursos Humanos</label></li> -->
            <li><a href="<?php echo get_page_link(46); ?>">Nómina</a></li>
            <!-- <li><a href="#">Compras</a></li>
            <li><a href="#">Licitaciones</a></li>
            <li><a href="#">En Proceso</a></li>
            <li><a href="#">Adjudicadas</a></li>
            <li><a href="#">Legal</a></li> -->
          </ul>
        </div>
      </li>
      <li data-equalizer-watch>
        <div class="small-12 columns">
          <h4>Empresa</h4>
          <ul class="side-nav disc">
          <li><a href="<?php echo get_page_link(26); ?>">Quienes Somos</a></li>
            <li><a href="<?php echo get_page_link(4); ?>">Misión, Visión & Valores</a></li>
            <li><a href="<?php echo get_page_link(8); ?>">Historia</a></li>
            <?php /*<li><a href="<?php echo get_page_link(367); ?>">Despacho del Administrador</a></li>*/ ?>
            <li><a href="<?php echo get_page_link(11); ?>">Consejo Administrativo</a></li>
            <!-- <li><a href="#">Política de privacidad</a></li> -->
          </ul>
        </div>
      </li>
      <li data-equalizer-watch>
        <div class="small-12 columns">
          <h4><a href="">Enlaces</a></h4>
          <ul class="side-nav disc">
            <li><a href="http://presidencia.gob.do/" target="_blank" title="Presidencia">Presidencia</a></li>
            <li><a href="http://www.cdeee.gob.do/" target="_blank" title="Corporación  Dominicana de Empresas Eléctricas Estatales">CDEEE</a></li>
            <li><a href="http://www.edeeste.com.do/" target="_blank" title="EDEESTE">EDEESTE</a></li>
            <li><a href="http://www.edenorte.com.do/" target="_blank" title="Edeeste">EDENORTE</a></li>
            <li><a href="http://www.hidroelectrica.gob.do/" target="_blank" title="Empresa de Generación Hidroeléctrica Dominicana">EGEHID</a></li>
            <li><a href="http://www.eted.gov.do/" target="_blank" title="Empresa de Transmisión Eléctrica Dominicana">ETED</a></li>

<!--             <li><a href="http://www.comprasdominicana.gov.do/" target="_blank" title="Edeeste">Compras y Contrataciones</a></li>
 -->            <!-- <li><a href="<?php echo get_page_link(15); ?>">Formulario de Proveedores</a></li>
            <li><label>Recursos Humanos</label></li>
            <li><a href="<?php echo get_page_link(46); ?>">Nómina</a></li> -->
            <!-- <li><a href="#">Compras</a></li>
            <li><a href="#">Licitaciones</a></li>
            <li><a href="#">En Proceso</a></li>
            <li><a href="#">Adjudicadas</a></li>
            <li><a href="#">Legal</a></li> -->
          </ul>
        </div>
      </li>
      <!-- <li>
        <div class="small-12 columns">
          <h4><a href="">Gobierno</a></h4>
          <ul class="side-nav">


            <li><a href="#">Compras</a></li>
            <li><a href="#">Licitaciones</a></li>
            <li><a href="#">En Proceso</a></li>
            <li><a href="#">Adjudicadas</a></li>
            <li><a href="#">Legal</a></li>
          </ul>
        </div>
      </li> -->
    </ul>
  </div>
</div>