<!-- <div class="row">
  <div class="small-12 medium-3 columns text-center">
    <img class="th" src="<?php echo site_url(); ?>/wp-content/uploads/2015/04/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
  </div>

  <div class="small-12 medium-6 columns">
    <h2>Prende el bombillo</h2>
    <p>Inscríbete, crea y gana hasta RD$500,000. Demuestra tu talento y creatividad desarrollando una campaña para Edesur!</p>

    <p>Edesur, toda nuestra energía para que vivas mejor.</p>
  </div>

  <div class="small-12 medium-3 columns">
    <form action="#" data-abide>
      <div class="row">
        <fieldset class="small-12 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nic">NIC de suministro <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nic" type="text" placeholder="Ingrese los dígitos de su NIC" required pattern="alpha" />
              </label>
              <small class="error">NIC no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-parentesco">Parentesco con el titular del contrato <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-parentesco" type="text" placeholder="Ingrese su parentesco con el titular" required pattern="alpha" />
              </label>
              <small class="error">Parentesco no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

      </div>

    </form>
  </div>
</div> -->

<!-- <div class="es-site-mercadeo-prende-bombillo row">

  <div class="small-12 medium-3 columns text-center">
    <img class="th" src="<?php echo site_url(); ?>/wp-content/uploads/2015/04/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
  </div>

  <div class="small-12 medium-6 columns">
    <h2>Prende el bombillo</h2>
    <p>Inscríbete, crea y gana hasta RD$500,000. Demuestra tu talento y creatividad desarrollando una campaña para Edesur!</p>

    <p>Edesur, toda nuestra energía para que vivas mejor.</p>
  </div>

  <div class="small-12 medium-3 columns">
    <form action="#" data-abide>
      <div class="row">
        <fieldset class="small-12 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nic">NIC de suministro <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nic" type="text" placeholder="Ingrese los dígitos de su NIC" required pattern="alpha" />
              </label>
              <small class="error">NIC no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-parentesco">Parentesco con el titular del contrato <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-parentesco" type="text" placeholder="Ingrese su parentesco con el titular" required pattern="alpha" />
              </label>
              <small class="error">Parentesco no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

      </div>

    </form>
  </div>

  <div class="small-12 columns">
    <h4>Formulario de inscripción integrante no. 1</h4>
    <form action="#" data-abide>
      <div class="row">
        <fieldset class="small-12 medium-4 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label>Campaña a trabajar  <small>Requerido</small>
                <select>
                  <option value="cobros">Aumento de cobros</option>
                  <option value="servicio">Mejora de Servicio al Cliente</option>
                  <option value="perdidas">Disminución de las perdidas</option>
                </select>
              </label>
              <small class="error">Es necesario seleccionar una campaña</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

        <fieldset class="small-12 medium-4 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label>Campaña a trabajar  <small>Requerido</small>
                <select>
                  <option value="cobros">Aumento de cobros</option>
                  <option value="servicio">Mejora de Servicio al Cliente</option>
                  <option value="perdidas">Disminución de las perdidas</option>
                </select>
              </label>
              <small class="error">Es necesario seleccionar una campaña</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

        <fieldset class="small-12 medium-4 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label>Campaña a trabajar  <small>Requerido</small>
                <select>
                  <option value="cobros">Aumento de cobros</option>
                  <option value="servicio">Mejora de Servicio al Cliente</option>
                  <option value="perdidas">Disminución de las perdidas</option>
                </select>
              </label>
              <small class="error">Es necesario seleccionar una campaña</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

      </div>

    </form>
  </div>
</div> -->

<!-- <div class="es-site-mercadeo-prende-bombillo row">

  <div class="small-12 medium-3 columns">
    <img class="th" src="<?php echo site_url(); ?>/wp-content/uploads/2015/04/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
    <p>Inscríbete, crea y gana hasta RD$500,000. Demuestra tu talento y creatividad desarrollando una campaña para Edesur!</p>

    <p>Edesur, toda nuestra energía para que vivas mejor.</p>
  </div>
  <div class="small-12 medium-9 columns">
    <h4>Formulario de inscripción integrante no. 1</h4>
    <form action="#" data-abide>
      <div class="row">
        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nic">NIC de suministro <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nic" type="text" placeholder="Ingrese los dígitos de su NIC" required pattern="alpha" />
              </label>
              <small class="error">NIC no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-parentesco">Parentesco con el titular del contrato <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-parentesco" type="text" placeholder="Ingrese su parentesco con el titular" required pattern="alpha" />
              </label>
              <small class="error">Parentesco no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

        </fieldset>

        <fieldset class="small-12 medium-6 columns end">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label>Campaña a trabajar  <small>Requerido</small>
                <select>
                  <option value="cobros">Aumento de cobros</option>
                  <option value="servicio">Mejora de Servicio al Cliente</option>
                  <option value="perdidas">Disminución de las perdidas</option>
                </select>
              </label>
              <small class="error">Es necesario seleccionar una campaña</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <button class="button small">Agregar integrantes</button>
            </div>
          </div>

        </fieldset>

      </div>

    </form>

  </div>
</div> -->

<form class="es-site-mercadeo-prende-bombillo" action="#" data-abide>

  <div class="row">

    <div class="small-12 columns text-center">
      <h3>Formulario de inscripción</h3>
    </div>

    <fieldset class="small-12 medium-3 columns">
      <img class="th" src="<?php echo site_url(); ?>/wp-content/uploads/2015/04/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
      <p>Inscríbete, crea y gana hasta RD$500,000. Demuestra tu talento y creatividad desarrollando una campaña para Edesur!</p>

      <p>Edesur, toda nuestra energía para que vivas mejor.</p>
    </fieldset>

    <div class="small-12 medium-9 columns">

      <div class="row">
        <fieldset class="columns">
          <div class="row">
            <div class="small-12 medium-6 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nic">NIC de suministro <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nic" type="text" placeholder="Ingrese los dígitos de su NIC" required pattern="alpha" />
              </label>
              <small class="error">NIC no puede ser dejado en blanco</small>
            </div>
            <div class="small-12 medium-6 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-parentesco">Parentesco con el titular del contrato <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-parentesco" type="text" placeholder="Ingrese su parentesco con el titular" required pattern="alpha" />
              </label>
              <small class="error">Parentesco no puede ser dejado en blanco</small>
            </div>
          </div>
        </fieldset>
      </div>

      <h4>Datos del lider del grupo</h4>

      <div class="row">
        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div> <!-- Nombre completo del lider del grupo -->

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div> <!-- Cédula -->

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Celular <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su celular" required pattern="number" />
              </label>
              <small class="error">Celular no puede ser dejado en blanco</small>
            </div>
          </div>
        </fieldset>

        <fieldset class="small-12 medium-6 columns end">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div> <!-- Universidad -->

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div> <!-- Carrera que cursa -->

          <div class="row">
            <div class="small-12 columns">
              <label>Campaña a trabajar  <small>Requerido</small>
                <select>
                  <option value="cobros">Aumento de cobros</option>
                  <option value="servicio">Mejora de Servicio al Cliente</option>
                  <option value="perdidas">Disminución de las perdidas</option>
                </select>
              </label>
              <small class="error">Es necesario seleccionar una campaña</small>
            </div>
          </div>

        </fieldset>
      </div>

    </div>

  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <h3>Integrantes</h3>
    </div>

    <div class="small-12 medium-6 columns">
      <h4>Datos del integrante no. 1</h4>
      <div class="row">
        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

        </fieldset>

        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

        </fieldset>
      </div>
    </div>
    <div class="small-12 medium-6 columns">
      <h4>Datos del integrante no. 2</h4>
      <div class="row">
        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-nombre-lider">Nombre completo del lider del grupo <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-nombre-lider" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
              </label>
              <small class="error">Nombre no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-cedula">
                Cédula <small>Requerido</small>

                <input id="es-site-form-mercadeo-prende-bombillo-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
              </label>
              <small class="error">Cédula no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-telefono">
                Teléfono <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
              </label>
              <small class="error">Teléfono no puede ser dejado en blanco</small>
            </div>
          </div>

        </fieldset>

        <fieldset class="small-12 medium-6 columns">

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-universidad">Universidad <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-universidad" type="text" placeholder="Ingrese el nombre de su universidad" required pattern="alpha" />
              </label>
              <small class="error">Universidad no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-carrera">Carrera que cursa <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-carrera" type="text" placeholder="Ingrese el nombre de su carrera" required pattern="alpha" />
              </label>
              <small class="error">Carrera no puede ser dejado en blanco</small>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="es-site-form-mercadeo-prende-bombillo-correo">
                Correo Electrónico <small>Requerido</small>
                <input id="es-site-form-mercadeo-prende-bombillo-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
              </label>
              <small class="error">Correo Electrónico no es valido</small>
            </div>
          </div>

        </fieldset>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="small-12 columns">
      <button class="button small">Enviar solicitud</button>
    </div>
  </div>

</form>