 <?php

    $args = array(
      'post_type' => 'post',
      'order' => 'DESC',
      'orderby' => 'date',
      'nopaging' => true
    );
    $es_query = new WP_Query( $args );

  ?>

  <section class="es-site-noticias small-12 medium-12 large-12 columns">

   <div class="row">
     <div class="es-site-noticias-articulos es-site-noticias-wrapper">



      <?php if ( have_posts() ) : while ( $es_query->have_posts() ) : $es_query->the_post(); ?>

        <article class="">
          <div class="columns">
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' );  ?></a>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p><?php the_excerpt(); ?></p>
          </div>
        </article>

      <?php endwhile; else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

     </div>
   </div>

   <div class="row">
     <div class="es-site-noticias-videos es-site-noticias-wrapper">
     <h2><a href="#">Videos de Edesur</a></h2>
       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-redes-independencia.jpg" alt=""></a>
           <h3><a href="#">Edesur rehabilita redes en barrios Avenida Independencia</a></h3>
         </div>
       </article>
       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-circuito-24.jpg" alt=""></a>
           <h3><a href="#">Edesur inaugura circuitos 24 horas</a></h3>
         </div>
       </article>
       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-engombe-uasd.jpg" alt=""></a>
           <h3><a href="#">Circuito 24 horas,Engombe UASD</a></h3>
         </div>
       </article>

       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-los-coroneles.jpg" alt=""></a>
           <h3><a href="#">Circuito 24 Horas, Los Coroneles de Herrera</a></h3>
         </div>
       </article>
       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-desarrollo-gas.jpg" alt=""></a>
           <h3><a href="#">Desarrollo del Gas en República Dominicana</a></h3>
         </div>
       </article>
       <article class="">
         <div class="columns">
           <a href="#"><img src="images/es-site-noticias-video-marcelo-jorge.jpg" alt=""></a>
           <h3><a href="#">Premios Energia 2014 reconoce a Marcelo Jorge</a></h3>
         </div>
       </article>
     </div>
   </div>

 </section>