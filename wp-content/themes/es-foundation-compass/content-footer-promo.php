
<?php if (is_front_page()): ?>
<?php   include(locate_template('includes/es-promo/es-content-promo-front.php')); ?>


<?php endif ?>

<div id="es-horario-de-servcios" class="reveal-modal medium" data-reveal>


  <?php
      /**
       * The WordPress Query class.
       * @link http://codex.wordpress.org/Function_Reference/WP_Query
       *
       */
      $args_horario = array(
        'post_type'   => 'horario',
        'post_status' => 'publish',
        'order'               => 'DESC',
        'orderby'             => 'date',
        'posts_per_page'         => 1,
      );

    $es_horario_query = new WP_Query( $args_horario );
  ?>

  <?php if ( $es_horario_query->have_posts() ) : ?>

    <?php while ( $es_horario_query->have_posts() ) : $es_horario_query->the_post(); ?>

      <?php
        $args_horario_files = array(
           'post_type' => 'attachment',
           'numberposts' => 1,
           'post_parent' => $post->ID
        );

        $es_horario_files = get_posts( $args_horario_files );
        $es_horario_files_url = wp_get_attachment_url( $es_horario_files[0]->ID );
        $es_horario_files_img = wp_get_attachment_image( $es_horario_files[0]->ID,  'full' );
      ?>

      <?php if ($es_horario_files): ?>

        <?php echo $es_horario_files_img; ?>
        <a class="close-reveal-modal">&#215;</a>

      <?php else: ?>

        <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/2015-03-17-es-horario-de-servcios-comerciales.jpg" alt="Testing condition">
        <a class="close-reveal-modal">&#215;</a>


      <?php endif ?>

      <?php /*the_content();*/ ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <p><?php _e( 'No se encontraron Horarios de servicio' ); ?></p>
  <?php endif; ?>

</div>

<div id="es-subsidio-iframe-modal" class="reveal-modal tiny text-center" data-reveal>
  <iframe src="http://edesurdominicana.com/subsidio-iframe/" frameborder="0" width="300" height="624" style="margin: 0 auto;"></iframe>
  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="es-call-center-promo" class="reveal-modal small" data-reveal>
  <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/2015-03-03-es-call-center-promo-poster.jpg" alt="">
  <a class="close-reveal-modal">&#215;</a>
</div>