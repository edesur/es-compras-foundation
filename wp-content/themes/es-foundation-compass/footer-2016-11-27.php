    <?php
      if (!is_page(370)) {
        get_template_part('/includes/es-nav-services');
      }
    ?>
    </section>

    <?php if (is_front_page()): ?>
      <section class="es-site-footer-news columns">

      <?php /*
      <div class="row">
          <div class="small-12 columns text-center">
            <h2>Consultas Públicas</h2>
            <h3>Estudio de Impacto Ambiental (EIA) de Rehabilitación de Redes</h3>
            <a href="http://edesurdominicana.com/2015/03/proyecto-de-rehabilitacion-de-redes-de-distribucion-de-electricidad-fase-ii/" target="_blank" class="button small alert">Ver aquí</a>
          </div>

        </div>
      */ ?>

      <div class="row">
        <div data-alert class="alert-box radius text-center ">
          <h5 style="color:#fff;">AVISO: LICITACIÓN PÚBLICA INTERNACIONAL CDEEE-LPI-001-2016 PARA LA COMPRAVENTA DE POTENCIA Y ENERGÍA ELÉCTRICA ASOCIADA MEDIANTE CONTRATOS DE LARGO PLAZO DE LAS EMPRESAS DISTRIBUIDORAS DE ELECTRICIDAD
      </h5>
          <a style="margin-bottom: 0;" href="http://www.edesur.com.do/wp-content/uploads/2016/08/es-site-cdeee-licitacion-lpi-001-2016-publicaion-oficial-2016-08-24.pdf" class="button primary info tiny">Mas detalles</a>
        </div>
      </div>

      </section>
    <?php endif ?>


    <?php if (!is_page(370)): ?>

      <footer class="es-site-footer small-12 columns">

        <?php /*if (is_front_page()):*/ ?>
          <!-- <div class="es-site-footer-news row">
            <div class="medium-4 columns">
              <a href="http://edesurdominicana.com/2015/03/proyecto-de-rehabilitacion-de-redes-de-distribucion-de-electricidad-fase-ii/" target="_blank">
                <img src="<?php echo site_url(); ?>/wp-content/uploads/2015/03/banner-banco-mundial-impactoambiental-rehabilitacion-de-redes-2015-03-24.jpg" alt="Proyecto de Rehabilitación de Redes de Distribución de Electricidad FASE II">
              </a>
            </div>
          </div> -->
        <?php /*endif*/ ?>

        <?php get_template_part('content', 'footer-contact'); ?>

        <?php get_template_part('content', 'footer-nav'); ?>

        <?php get_template_part('content', 'footer-copyright'); ?>
      </footer>

      <?php get_template_part('content', 'footer-promo'); ?>
    <?php endif ?>

    <?php wp_footer(); ?>
  </body>
</html>
