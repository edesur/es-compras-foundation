<?php

  $args = array( 'post_type' =>  'nomina', 'orderby' => 'date', 'order' => 'DESC', 'nopaging' => true );

  $the_query = new WP_Query( $args );
?>

<section class="es-site-section-wrapper small-12 columns" data-equalizer>

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title">Nómina</h3>
    </div>
  </div>

  <div class="es-site-section-wrapper-background small-12 columns">
    <div class="row">

     <div class="es-site-adobe-reader-download small-7 columns push-5 show-for-medium-up">
       <div class="row">
         <div class="es-site-file-adobe-reader-copy small-8 medium-8 large-9 columns">
           <small>Si no puede ver los documentos, es posible que no tenga instalado <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader">Adobe Reader</a>. Presione la imagen para instalarlo.</small>
         </div>
         <div class="es-site-file-icon-adobe-reader small-4 medium-4 large-3 columns">
           <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-get-adobe-reader.gif' ?>" alt="Descargue Adobe Reader"></a>
         </div>
       </div>
     </div>

     <div class="small-12 columns">

      <p>Si está interesado en información adicional, por favor hacerlo a través del <a href="http://www.edesur.com.do/transparencia/oai/solicitud-acceso-informacion-publica/" title="Formulario de solicitud de OAI (Acceso a la Información Pública)">formulario de solicitud de OAI (Acceso a la Información Pública)</a>.</p>
     </div>

      <ul class="small-block-grid-2 medium-block-grid-3">


      <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post() ?>

         <?php

           $es_rh_nomina = get_field( 'es_recursos_humanos_nomina' );
           $es_rh_nomina_url =  wp_get_attachment_url( $es_rh_nomina['id'] );
          ?>

         <?php if ($es_rh_nomina): ?>

           <li class="text-center">
             <a href="<?php echo $es_rh_nomina['url']; ?>" class="th"><img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-recursos-humanos-nomina-icon.jpg' ?>"/> </a>
             <h6><a href="<?php echo $es_rh_nomina['url']; ?>">

               <?php the_title(); ?>
             </a></h6>
           </li>

         <?php endif ?>

       <?php endwhile; else: ?>

       <?php endif; ?>

     </ul>
    </div>
  </div>

 <div class="row">
   <div class="small-12 columns">



   </div>
 </div>
</section>