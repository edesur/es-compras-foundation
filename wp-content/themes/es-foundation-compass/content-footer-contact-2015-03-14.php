<div id="es-site-footer-contact" class="row small-collapse medium-uncollapse">
  <div class="small-12 columns">
    <h4>Contacto</h4>
  </div>


  <div class="small-12 medium-8 large-5 columns">

    <form class="es-site-footer-contact-form" action="#" data-abide>

      <div class="row small-collapse large-uncollapse">
          <div class="es-site-contact-field large-6 columns">
            <label for="es-site-contact-dropdown-select">¿Que le gustaría decirnos? <small>Requerido</small>
            <select id="es-site-contact-dropdown-select" class="es-site-contact-dropdown" required>
              <option value>Selecione un opción</option>
              <option value="denuncias">Denuncias</option>
              <option value="factura-email">Factura por email</option>
              <option value="telemedida">Telemedida</option>
              <option value="empleo">Solicitud de empleo</option>
              <option value="sugerencias">Buzón de Sugerencias</option>
              <option value="sitio-web">Redes sociales</option>
              <option value="otros">Otro tema</option>

              <!-- <option value="facturacion">Facturación</option> -->
              <!-- <option value="solicitar-lectura">Solicitar lectura</option> -->
              <!-- <option value="cambio-titular">Cambio de titular</option> -->
              <!-- <option value="cambio-potencia">Cambio de potencia</option> -->
              <!-- <option value="oficina-virtual">Oficina Virtual</option> -->
              <!-- <option value="nuevo-contrato">Nuevo contrato</option> -->
              <!-- <option value="nuestro-servicio">Nuestro Servicio</option> -->
              <!-- <option value="fraude">Fraude</option> -->
              <!-- <option value="reportar-contratista">Reportar contratista</option> -->
              <!-- <option value="reportar-empleado">Reportar empleado</option> -->
              <!-- <option value="sugerencias">Sugerencias</option> -->
              <!-- <option value="sitio-web">Sitio web</option> -->
              <!-- <option value="otros">Otros</option> -->
            </select>
            </label>
            <small class="error">Es necesario seleccionar una opción</small>
          </div>

          <div class="es-site-contact-field large-6 columns">
            <label for="es-site-contact-input-email">Su correo electrónico <small>Requerido</small></label>
              <input id="es-site-contact-input-email" type="email" placeholder="" required pattern="email"/>
            </label>
            <small class="error">Correo Electrónico no es valido</small>
          </div>

      </div>

      <div class="row small-collapse large-uncollapse">
        <div class="es-site-contact-field large-6 columns">
          <label for="es-site-contact-input-nombre">Nombre <small>Requerido</small></label>
            <input id="es-site-contact-input-nombre" type="text" placeholder="" required pattern="alpha"/>
            <small class="error">Nombre no puede ser dejado en blanco</small>
        </div>
        <div class="es-site-contact-field large-6 columns">
            <label for="es-site-contact-input-apellido">Apellidos <small>Requerido</small></label>
              <input id="es-site-contact-input-apellido" type="text" placeholder="" required pattern="alpha"/>
              <small class="error">Apellidos no puede ser dejado en blanco</small>
          </div>
      </div>

      <div class="es-site-contact-field-account-info row small-collapse large-uncollapse">
        <div class="es-site-contact-field large-6 columns">
          <label for="es-site-contact-input-cedula">Cédula <small>Requerido</small></label>
            <input id="es-site-contact-input-cedula" type="number" placeholder=""  pattern="number"/>
        </div>

        <div class="es-site-contact-field large-6 columns">
          <label for="es-site-contact-input-nic">NIC <small>Requerido</small></label>
            <input id="es-site-contact-input-nic" type="number" placeholder=""  pattern="number"/>
        </div>
      </div>

      <div class="row small-collapse large-uncollapse">

        <div class="es-site-contact-field es-site-contact-textarea-box large-12 columns">
          <label for="es-site-contact-textarea">Su Mensaje <small>Requerido</small></label>
          <textarea id="es-site-contact-textarea" placeholder="" required pattern="alpha_numeric"></textarea>
          <small class="error">Mensaje no puede ser dejado en blanco</small>
          <button type="submit" class="es-site-contact-form-submit button right">Enviar</button>
        </div>
      </div>

    </form>

    <?php if( function_exists( 'ninja_forms_display_form' ) ){ninja_forms_display_form( 6 ); } ?>
    <?php $all_fields = ninja_forms_get_all_fields(); ?>
    <h3><?php var_dump($all_fields); ?></h3>
  </div>

  <div class="es-site-footer-address medium-4 large-3 columns large-push-4" itemscope itemtype="http://schema.org/GovernmentService">
    <div itemprop="address">
      <!-- <h5>Dirección</h5> -->
      <ul class="no-bullet">
        <li><h5>Oficina Administrativa</h5></li>
        <li>Av. Tiradentes # 47</li>
        <li>Esq. Carlos Sánchez y Sánchez</li>
        <li>Torre Serrano</li>
        <li>Ensanche Naco</li>
        <li>Santo Domingo D.N.</li>
        <li>República Dominicana</li>
        <li itemprop="telephone"><a href="tel://+18096839292" >(809) 683-9292</a></li>
        <!-- <li>
          <time itemprop="openingHours" datetime="Mo-Fri">Lunes a Viernes</time>
        </li> -->
        <li>
          <time itemprop="openingHours" datetime="08:00-17:00">8:00 a.m. - 5:00 p.m.</time>
        </li>
        <li itemprop="telephone"><h5>Call Center</h5>
          <a href="tel://+18096839393" >(809) 683-9393</a>
        </li>
        <li>
          <time itemprop="openingHours" datetime="00:00-23:29">24 Horas</time>
        </li>
        <li><h5>Redes Sociales</h5></li>
        <li class="es-site-nav-addthis">
          <!-- Go to www.addthis.com/dashboard to customize your tools -->
          <div class="addthis_horizontal_follow_toolbox"></div>
        </li>
      </ul>

    </div>
  </div>

  <div class="es-site-footer-map-container medium-12 large-4 columns large-pull-3">
    <div id="es-site-footer-map-canvas" class="es-site-footer-map small-12 columns"></div>
  </div>

</div>