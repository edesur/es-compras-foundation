<?php get_header(); ?>

  <section class="es-site-masthead-home es-site-masthead columns">

    <div class="row text-center">
      <div class="es-site-masthead-title small-12 large-6 columns large-centered">
        <h1 class="hide-for-large-up"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo('name') ?>">Edesur Dominicana</a></h1>
        <h2>&iexcl;Vive la experiencia!</h2>
          <!-- <div class="row"> -->

          <!-- </div> -->
        <h3 class="subheader">Todos los servicios al alcance de un click</h3>

        <a href="https://www.edesur.com.do/ov/" class="es-site-masthead-cta button radius" target="_blank">Oficina Virtual</a>

      </div>
      <div class="small-12 columns">

      </div>
    </div>

    <?php get_template_part('/includes/es-bar-app') ?>

<?php get_footer(); ?>