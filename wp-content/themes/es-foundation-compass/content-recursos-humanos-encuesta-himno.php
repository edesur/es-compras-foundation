<?php

  $args = array( 'post_type' =>  'nomina', 'orderby' => 'date', 'order' => 'DESC', 'nopaging' => true );

  $the_query = new WP_Query( $args );
?>

<section class="es-site-section-wrapper small-12 columns" data-equalizer>

  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title"><?php the_title(); ?></h3>
    </div>
  </div>

  <div class="es-site-section-wrapper-background small-12 columns">
    <div class="row">

     <div class="small-12 large-8 columns">
       <ul>
         <?php if (function_exists('vote_poll') && !in_pollarchive()): ?>
           <li class="text-left">

               <?php get_poll(1);?>

           </li>
         <?php endif; ?>
       </ul>


     </div>
     <div class="small-12 columns">


       <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

        <?php endwhile; else : ?>
       <?php endif; ?>

     </div>

    </div>
  </div>

 <div class="row">
   <div class="small-12 columns">



   </div>
 </div>
</section>