<div class="es-site-nav-top-wrapper contain-to-grid fixed">
  <nav class="es-site-nav-top top-bar" role="navigation" data-topbar>
    <ul class="title-area">
      <li class="name">
        <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo('name') ?>">Edesur Dominicana</a></h1>
      </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>

    <section class="top-bar-section">
      <!-- Right Nav Section -->
      <ul class="es-site-nav-top-right right">

        <li class="es-site-nav-item not-click has-dropdown">
          <a href="#">Servicios</a>
          <ul class="dropdown">
            <li><label>Servicio al Cliente</label></li>
            <li><a href="<?php echo get_page_link(80); ?>">Nuevo Contrato</a></li>
            <li><label>Servicios en Línea</label></li>
            <li><a href="<?php echo get_page_link(18); ?>">Denuncia de Fraude</a></li>
            <!-- <li><a href="#">Servicio al Cliente</a></li>
            <li><a href="ov.html">Servicios en Linea</a></li>
            <li><a href="#">Puntos de pago</a></li> -->
          </ul>
        </li>
        <li class="es-site-nav-item not-click has-dropdown">
          <a href="#">Transparencia</a>
          <ul class="dropdown">
            <li><label>Compras &amp; Contrataciones</label></li>
            <li><a href="<?php echo get_page_link(13); ?>">Normativas</a></li>
            <li><a href="<?php echo get_post_type_archive_link( 'proceso_compra' ); ?>">Procesos de compras</a></li>
            <li><label>Proveedores</label></li>
            <li><a href="<?php echo get_page_link(15); ?>">Formulario de Proveedores</a></li>
            <li><label>Recursos Humanos</label></li>
            <li><a href="<?php echo get_page_link(46); ?>">Nómina</a></li>
          </ul>
        </li>
        <li class="es-site-nav-item"><a href="<?php echo get_page_link(53); ?>">Preguntas</a></li>
        <li class="es-site-nav-item has-dropdown">
          <a href="<?php echo get_page_link(26); ?>">Empresa</a>
          <ul class="dropdown">
            <li><a href="<?php echo get_page_link(4); ?>">Misión, Visión & Valores</a></li>
            <li><a href="<?php echo get_page_link(8); ?>">Historia</a></li>
            <li><a href="<?php echo get_page_link(11); ?>">Consejo de Administración</a></li>
            <li style="display:none"><a class="es-modal-amor-primera-mordida-link" data-reveal-id="es-modal-amor-primera-mordida" href="">Agua</a></li>
          </ul>
        </li>
        <li class="es-site-nav-item"><a href="#es-site-footer-contact">Contacto</a></li>




        <li class="es-site-nav-search-wrapper has-form">
          <div class="es-site-nav-search row collapse">
            <div class="es-site-nav-search-input large-9 small-9 columns">
              <input class="right" type="text" placeholder="Buscar en Edesur">
            </div>
            <div class="large-3 small-3 columns">
              <a href="#" class="button expand">
                <i class="step fi-magnifying-glass size-72"></i>
              </a>
            </div>
          </div>
        </li>
        <li class="es-site-nav-addthis">
          <!-- Go to www.addthis.com/dashboard to customize your tools -->
          <div class="addthis_horizontal_follow_toolbox"></div>
        </li>
      </ul>
    </section>
  </nav>
</div>