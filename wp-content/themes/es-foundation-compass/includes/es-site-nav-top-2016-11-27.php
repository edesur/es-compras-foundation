<div class="es-site-nav-top-wrapper contain-to-grid fixed">

  <div class="row show-for-large-up">
    <div class="small-12 columns">
      <div class="small-10 columns"></div>
      <div class="es-site-nav-top-rd-shield small-2 columns"><h6>República Dominicana</h6></div>
    </div>
  </div>

  <nav class="es-site-nav-top top-bar" role="navigation" data-topbar>
    <ul class="title-area">
      <li class="name">
        <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo('name') ?>">Edesur Dominicana</a></h1>
      </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>
    <?php if (!is_page(370)): ?>
      <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="es-site-nav-top-right right">

          <li class="es-site-nav-item not-click has-dropdown">
            <?php
                $es_interrupciones_obj = get_page_by_path('mantenimientos');
                $es_interrupciones_ID = $es_interrupciones_obj->ID;
                $es_actualizacion_de_datos_obj = get_page_by_path('actualizacion-de-datos');
                $es_actualizacion_de_datos_ID = $es_actualizacion_de_datos_obj->ID;
              ?>
            <a href="#">Servicios</a>
            <ul class="dropdown">
              <li><label>Servicios en Línea</label></li>
              <li><a href="https://www.edesur.com.do/ov/" title="Pago de Factura" target="_blank">Pago de Factura</a></li>
              <?php /*<li><a href="<?php echo site_url(); ?>/solicitud-nuevo-servicio">Nuevo Servicio</a></li>*/ ?>
              <li><a href="<?php echo get_page_link($es_actualizacion_de_datos_ID); ?>">Actualización de datos</a></li>
              <li><a href="<?php echo get_page_link(18); ?>">Denuncia de Fraude</a></li>

              <li><label>Servicio al Cliente</label></li>
              <li><a href="<?php echo get_page_link($es_interrupciones_ID); ?>">Mantenimientos Programados</a></li>
              <li><a data-reveal-id="es-subsidio-iframe-modal">Subsidio</a></li>
              <!-- <li><a href="#">Servicio al Cliente</a></li>
              <li><a href="ov.html">Servicios en Linea</a></li>
              <li><a href="#">Puntos de pago</a></li> -->
            </ul>
          </li>
          <li class="es-site-nav-item not-click has-dropdown">
            <a>Transparencia</a>
            <ul class="dropdown">
              <li><label>Compras &amp; Contrataciones</label></li>
             <!--  <li><a href="<?php echo get_page_link(13); ?>">Normativas</a></li> -->
              <li><a href="<?php echo get_post_type_archive_link( 'proceso_compra' ); ?>">Procesos de compras</a></li>
               <li><label>Legal</label></li>
               <li><a href="<?php /*echo get_page_link(886);*/ ?><?php echo esc_url( get_permalink( get_page_by_title( 'Ley General de Electricidad' ) ) ); ?>">Ley General de Electricidad</a></li>
              <li><label>Proveedores</label></li>
              <li><a href="<?php echo get_page_link(15); ?>">Formulario de Proveedores</a></li>
              <li><label>Recursos Humanos</label></li>
              <li><a href="<?php echo get_page_link(46); ?>">Nómina</a></li>
            </ul>
          </li>
          <li class="es-site-nav-item"><a href="<?php echo get_page_link(53); ?>">Preguntas</a></li>
          <li class="es-site-nav-item has-dropdown">
            <a>Empresa</a>
            <ul class="dropdown">
              <li><a href="<?php echo get_page_link(26); ?>">Quienes Somos</a></li>
              <li><a href="<?php echo get_page_link(4); ?>">Misión, Visión & Valores</a></li>
              <li><a href="<?php echo get_page_link(8); ?>">Historia</a></li>
              <?php /*<li><a href="<?php echo get_page_link(367); ?>">Despacho del Administrador</a></li>*/ ?>
              <li><a href="<?php echo get_page_link(11); ?>">Consejo de Administración</a></li>
              <li style="display:none"><a class="es-modal-promotions-link" data-reveal-id="es-modal-promotions-banner" href=""></a></li>
            </ul>
          </li>
          <li class="es-site-nav-item"><a href="#es-site-footer-contact">Contacto</a></li>




          <li class="es-site-nav-search-wrapper has-form">
            <div class="es-site-nav-search small-12 columns">
            <?php get_search_form(); ?>
            </div>
          </li>
          <!-- <li class="es-site-nav-addthis"> -->
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <!-- <div class="addthis_horizontal_follow_toolbox"></div> -->
          <!-- </li> -->
        </ul>
      </section>
    <?php endif ?>

  </nav>
</div>