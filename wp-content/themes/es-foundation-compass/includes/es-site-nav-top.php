<div class="es-site-nav-top-wrapper contain-to-grid fixed es-site-nav-top-wrapper-bg">

  <nav class="es-site-nav-top top-bar" role="navigation" data-topbar>
    <ul class="title-area">
      <li class="name">
        <h1><a href="http://edesur.com.do" rel="home" title="<?php bloginfo('name') ?>">Edesur Dominicana</a></h1>
      </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>

  </nav>
</div>