<?php

    function es_comercial_forms_wrap_before_field( $field_id, $data ) {

       /*********************************** Tipo de Solicitud ***********************************/
       //Fields: Tipo de Solicitud
       if ($field_id == 309) {
         echo '<div class="row">
                   <fieldset class="large-8 columns">
                     <div class="row">
                       <div class="large-6 columns">';
       }



       //Fields: Contrato previo con EDESUR, Contrato (NIC) Previo
       if ($field_id == 310) {
         echo '<div class="row">
                       <div class="large-6 columns">';
       }

       //Fields: Contrato (NIC) Previo
       if ($field_id == 311) {
         echo '<div class="row">
                       <div class="large-4 columns">';
       }

      /*********************************** Datos del cliente ***********************************/
      //Fields: Nombre, Apellido
      if ($field_id == 313) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      if ($field_id == 314) {
        echo '
                  <div class="large-6 columns">';
      }

      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 315) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 316 || $field_id == 317) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Nacionalidad, Verificación de identidad
      if ($field_id == 318) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 319) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Teléfono, Celular
      if ($field_id == 320) {
        echo '<div class="row">
                    <div class="large-6 columns">';
      }

      if ($field_id == 321) {
        echo '
                <div class="large-6 columns end">';
      }

      //Fields: Correo electrónico, Twitter
      if ($field_id == 322) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 323) {
        echo '<div class="large-6 columns">';
      }

      /*********************************** Datos de ubicación ***********************************/
      //Fields:  Calle/Avenida, Número de Residencia/Edificio
      if ($field_id == 325) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      if ($field_id == 326) {
        echo '
                  <div class="large-6 columns end">';
      }

      //Fields: Edificio/Comercio/Residencial, Apartamento, Sector/Localidad/Barrio
      if ($field_id == 327) {
        echo '<div class="row">
                <div class="large-5 columns">';
      }

      if ($field_id == 328) {
        echo '<div class="large-2 columns">';
      }

      if ($field_id == 329) {
        echo '<div class="large-5 columns">';
      }

      //Fields: Municipio/Distrito Municpal, Provincia
      if ($field_id == 330) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 331) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Verificación de dirección, Documentación de titularidad del inmueble
      if ($field_id == 333) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 334) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Medidor de referencia, Contrato de referencia (NIC), Matrícula del centro de transformación
      if ($field_id == 335) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 336) {
        echo '<div class="large-6 columns">';
      }

      if ($field_id == 337) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      /*********************************** Contratación Express ***********************************/

      //Fields: Seleccione el tipo de Contratación
      if ($field_id == 339) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      //Fields: Oficina Comercial/Punto Expreso, Fecha de la visita, Hora de la visita
      if ($field_id == 342) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 360 || $field_id == 361) {
        echo '<div class="large-3 columns">';
      }

      //Fields: Verificación de datos suministrados
      if ($field_id == 363) {
        echo '<div class="row">
                <div class="large-12 columns">';
      }

    }

    add_action( 'ninja_forms_display_before_field', 'es_comercial_forms_wrap_before_field', 10, 2 );

    function es_comercial_forms_wrap_after_field( $field_id, $data ) {

      /*********************************** Tipo de Solicitud ***********************************/
       //Fields: Tipo de Solicitud
      if ($field_id == 309) {
          echo '</div>
                    </div>';
      }

      //Fields: Contrato previo con EDESUR, Contrato (NIC) Previo
      if ($field_id == 310) {
          echo '</div>
                    </div>';
      }

      //Fields: Contrato (NIC) Previo
      if ($field_id == 311) {
          echo '</div>
                  </div>
                </fieldset>
              </div>';
      }

      /*********************************** Datos del cliente ***********************************/

      //Fields: Nombre, Apellido
      if ($field_id == 313) {
          echo '</div>';
      }

      if ($field_id == 314) {
          echo '</div>
                    </div>';
      }

      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 315 || $field_id == 316) {
          echo '</div>';
      }

      if ($field_id == 317) {
          echo '</div>
                    </div>';
      }

       //Fields: Nacionalidad, Verificación de identidad
      if ($field_id == 318) {
          echo '</div>';
      }

      if ($field_id == 319) {
          echo '</div>
                    </div>';
      }

      //Fields: Teléfono, Celular
      if ($field_id == 320) {
          echo '</div>';
      }

      if ($field_id == 321) {
          echo '</div>
                    </div>';
      }

      //Fields: Correo electrónico, Twitter
      if ($field_id == 322) {
          echo '</div>';
      }


      if ($field_id == 323) {
          echo '</div>
                  </div>
                </fieldset>
              </div>';
      }

      /*********************************** Datos de ubicación ***********************************/
      //Fields: Calle/Avenida, Número de Residencia/Edificio
      if ($field_id == 325) {
          echo '</div>';
      }

      if ($field_id == 326) {
          echo '</div>
                    </div>';
      }

       //Fields: Edificio/Comercio/Residencial, Apartamento, Sector/Localidad/Barrio
      if ($field_id == 327 || $field_id == 328) {
          echo '</div>';
      }

      if ($field_id == 329) {
          echo '</div>
                    </div>';
      }

     //Fields: Municipio/Distrito Municpal, Provincia
      if ($field_id == 330) {
          echo '</div>';
      }

      if ($field_id == 331) {
          echo '</div>
                    </div>';
      }

      //Fields: Verificación de dirección, Documentación de titularidad del inmueble
      if ($field_id == 333) {
          echo '</div>';
      }

      if ($field_id == 334) {
          echo '</div>
                    </div>';
      }

      //Fields: Medidor de referencia, Contrato de referencia (NIC), Matrícula del centro de transformación
      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 335) {
          echo '</div>';
      }

      if ($field_id == 336) {
          echo '</div>
                    </div>';
      }

      if ($field_id == 337) {
          echo '</div>
                    </div>
                    </fieldset>
                    </div>';
      }

      /*********************************** Contratación Express ***********************************/
      if ($field_id == 339) {
          echo '</div>
                    </div>';
      }

      //Fields: Oficina Comercial/Punto Expreso, Fecha de la visita, Hora de la visita
      if ($field_id == 342 || $field_id == 360) {
          echo '</div>';
      }

      if ($field_id == 361) {
          echo '</div>
                    </div>';
      }

      //Fields: Verificación de datos suministrados
      if ($field_id == 363) {
          echo '</div>
                    </div>
                    </fieldset>
                    <fieldset id="">

                    </fieldset>
                    </div>';
      }
    }



    add_action( 'ninja_forms_display_after_field', 'es_comercial_forms_wrap_after_field', 10, 2 );

?>