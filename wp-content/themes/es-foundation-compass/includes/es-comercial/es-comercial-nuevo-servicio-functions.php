<?php

    function es_comercial_forms_wrap_before_field( $field_id, $data ) {

      /*********************************** Tipo de Solicitud ***********************************/
       //Fields: Tipo de Solicitud
       if ($field_id == 153) {
         echo '<div class="row">
                   <fieldset class="large-8 columns">
                     <div class="row">
                       <div class="large-6 columns">';
       }

       //Fields: Contrato previo con EDESUR, Contrato (NIC) Previo
       if ($field_id == 154) {
         echo '<div class="row">
                       <div class="large-6 columns">';
       }

       //Fields: Contrato (NIC) Previo
       if ($field_id == 155) {
         echo '<div class="row">
                       <div class="large-4 columns">';
       }

      /*********************************** Datos del cliente ***********************************/
      //Fields: Nombre, Apellido
      if ($field_id == 157) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      if ($field_id == 158) {
        echo '
                  <div class="large-6 columns end">';
      }

      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 159) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 160 || $field_id == 161) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Nacionalidad, Verificación de identidad
      if ($field_id == 162) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 163) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Teléfono, Celular
      if ($field_id == 164) {
        echo '<div class="row">
                    <div class="large-6 columns">';
      }

      if ($field_id == 165) {
        echo '
                <div class="large-6 columns end">';
      }

      //Fields: Correo electrónico, Twitter
      if ($field_id == 166) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 167) {
        echo '<div class="large-6 columns">';
      }

      /*********************************** Datos de ubicación ***********************************/
      //Fields:  Calle/Avenida, Número de Residencia/Edificio
      if ($field_id == 169) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      if ($field_id == 170) {
        echo '
                  <div class="large-6 columns end">';
      }

      //Fields: Edificio/Comercio/Residencial, Apartamento, Sector/Localidad/Barrio
      if ($field_id == 171) {
        echo '<div class="row">
                <div class="large-5 columns">';
      }

      if ($field_id == 172) {
        echo '<div class="large-2 columns">';
      }

      if ($field_id == 173) {
        echo '<div class="large-5 columns">';
      }

      //Fields: Municipio/Distrito Municpal, Provincia
      if ($field_id == 174) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 175) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Verificación de dirección, Documentación de titularidad del inmueble
      if ($field_id == 177) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 178) {
        echo '<div class="large-6 columns">';
      }

      //Fields: Medidor de referencia, Contrato de referencia (NIC), Matrícula del centro de transformación
      if ($field_id == 179) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 180) {
        echo '<div class="large-6 columns">';
      }

      if ($field_id == 181) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      /*********************************** Contratación Express ***********************************/

      //Fields: Seleccione el tipo de Contratación
      if ($field_id == 183) {
        echo '<div class="row">
                  <fieldset class="large-8 columns">
                    <div class="row">
                      <div class="large-6 columns">';
      }

      //Fields: Oficina Comercial/Punto Expreso, Fecha de la visita, Hora de la visita
      if ($field_id == 209) {
        echo '<div class="row">
                <div class="large-6 columns">';
      }

      if ($field_id == 203 || $field_id == 204) {
        echo '<div class="large-3 columns">';
      }

      //Fields: Verificación de datos suministrados
      if ($field_id == 206) {
        echo '<div class="row">
                <div class="large-12 columns">';
      }

    }

    add_action( 'ninja_forms_display_before_field', 'es_comercial_forms_wrap_before_field', 10, 2 );

    function es_comercial_forms_wrap_after_field( $field_id, $data ) {

      /*********************************** Tipo de Solicitud ***********************************/
       //Fields: Tipo de Solicitud
      if ($field_id == 153) {
          echo '</div>
                    </div>';
      }

      //Fields: Contrato previo con EDESUR, Contrato (NIC) Previo
      if ($field_id == 154) {
          echo '</div>
                    </div>';
      }

      //Fields: Contrato (NIC) Previo
      if ($field_id == 155) {
          echo '</div>
                  </div>
                </fieldset>
              </div>';
      }

      /*********************************** Datos del cliente ***********************************/

      //Fields: Nombre, Apellido
      if ($field_id == 157) {
          echo '</div>';
      }

      if ($field_id == 158) {
          echo '</div>
                    </div>';
      }

      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 159 || $field_id == 160) {
          echo '</div>';
      }

      if ($field_id == 161) {
          echo '</div>
                    </div>';
      }

       //Fields: Nacionalidad, Verificación de identidad
      if ($field_id == 162) {
          echo '</div>';
      }

      if ($field_id == 163) {
          echo '</div>
                    </div>';
      }

      //Fields: Teléfono, Celular
      if ($field_id == 164) {
          echo '</div>';
      }

      if ($field_id == 165) {
          echo '</div>
                    </div>';
      }

      //Fields: Correo electrónico, Twitter
      if ($field_id == 166) {
          echo '</div>';
      }


      if ($field_id == 167) {
          echo '</div>
                  </div>
                </fieldset>
              </div>';
      }

      /*********************************** Datos de ubicación ***********************************/
      //Fields: Calle/Avenida, Número de Residencia/Edificio
      if ($field_id == 169) {
          echo '</div>';
      }

      if ($field_id == 170) {
          echo '</div>
                    </div>';
      }

       //Fields: Edificio/Comercio/Residencial, Apartamento, Sector/Localidad/Barrio
      if ($field_id == 171 || $field_id == 172) {
          echo '</div>';
      }

      if ($field_id == 173) {
          echo '</div>
                    </div>';
      }

     //Fields: Municipio/Distrito Municpal, Provincia
      if ($field_id == 174) {
          echo '</div>';
      }

      if ($field_id == 175) {
          echo '</div>
                    </div>';
      }

      //Fields: Verificación de dirección, Documentación de titularidad del inmueble
      if ($field_id == 177) {
          echo '</div>';
      }

      if ($field_id == 178) {
          echo '</div>
                    </div>';
      }

      //Fields: Medidor de referencia, Contrato de referencia (NIC), Matrícula del centro de transformación
      //Fields: Documento de identidad, Cédula, Pasaporte
      if ($field_id == 179) {
          echo '</div>';
      }

      if ($field_id == 180) {
          echo '</div>
                    </div>';
      }

      if ($field_id == 181) {
          echo '</div>
                    </div>
                    </fieldset>
                    </div>';
      }

      /*********************************** Contratación Express ***********************************/
      if ($field_id == 183) {
          echo '</div>
                    </div>';
      }

      //Fields: Oficina Comercial/Punto Expreso, Fecha de la visita, Hora de la visita
      if ($field_id == 209 || $field_id == 203) {
          echo '</div>';
      }

      if ($field_id == 204) {
          echo '</div>
                    </div>';
      }

      //Fields: Verificación de datos suministrados
      if ($field_id == 206) {
          echo '</div>
                    </div>
                    </fieldset>
                    </div>';
      }
    }



    add_action( 'ninja_forms_display_after_field', 'es_comercial_forms_wrap_after_field', 10, 2 );

?>