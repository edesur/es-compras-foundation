<div id="es_site_comercial_nuevo_servicio_req_residencial" class="reveal-modal xlarge" data-reveal aria-labelledby="Requisitos: Para personas físicas" aria-hidden="true" role="dialog">
  <div class="es-site-comercial-nuevo-servicio-req-residencial panel callout radius">
    <h3>Requisitos</h3>
    <h4>Para personas físicas:</h4>
    <ol>
      <li>Copia de la <b>cédula de identidad</b> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>
      <li>La documentación que acredite la titularidad del inmueble entre los cuales tenemos: El título de propiedad del inmueble o certificación de la Dirección General de Catastro que acredite la Posesión o tenencia del inmueble.</li>
      <li>Contrato de alquiler a nombre del solicitante debidamente notariado.</li>
      <li>En caso de existir deuda se le podría solicitar (opcional) que el contrato de alquiler este registrado ante el Banco Agrícola, así como la copia del título de propiedad de ese inmueble.</li>
      <li>Contrato  de préstamo de vivienda debidamente notariado.</li>
      <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta debidamente notariada.</li>
      <li>En caso de inmuebles construidos en terrenos del Estado Dominicana: Declaración jurada ante siete (7) testigos debidamente notariada y registrada ante el registro civil, documento que acredite el uso legítimo del inmueble.</li>
      <li>En caso de que el titular de inmueble se deba hacer representar deberá hacerlo mediante poder especial debidamente notariado.  El representante también deberá aportar copia de su cédula.</li>
    </ol>
  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="es_site_comercial_nuevo_servicio_req_comercial" class="reveal-modal xlarge" data-reveal aria-labelledby="Requisitos: Personas Jurídicas" aria-hidden="true" role="dialog">
  <div class="es-site-comercial-nuevo-servicio-req-comercial panel callout radius">
    <h3>Requisitos</h3>
    <h4>Personas Jurídicas:</h4>
    <ol>
      <li>Copia de la <b>cédula de identidad</b> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>
      <li>La documentación que acredite la titularidad del inmueble entre los cuales tenemos: El título de propiedad del inmueble o certificación de la Dirección General de Catastro que acredite la Posesión o tenencia del inmueble.</li>
      <li>Contrato de alquiler a nombre del solicitante debidamente notariado.</li>
      <li>En caso de existir deuda se le podría solicitar (opcional) que el contrato de alquiler este registrado ante el Banco Agrícola, así como la copia del título de propiedad de ese inmueble.</li>
      <li>Contrato  de préstamo de vivienda debidamente notariado.</li>
      <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta debidamente notariada.</li>
      <li>En caso de inmuebles construidos en terrenos del Estado Dominicana: Declaración jurada ante siete (7) testigos debidamente notariada y registrada ante el registro civil, documento que acredite el uso legítimo del inmueble.</li>
      <li>En caso de que el titular de inmueble se deba hacer representar deberá hacerlo mediante poder especial debidamente notariado.  El representante también deberá aportar copia de su cédula.</li>
    </ol>

    <p><b>Además de los requisitos anteriormente especificados se requerirá:</b></p>
    <p>Copia certificada de los documentos constitutivos de la empresa.  Entre estos:</p>

    <ol>
      <li>Acta del Consejo que avale al representante legal.</li>
      <li>Certificación o Tarjeta del Registro Nacional de Contribuyente (RNC).</li>
      <li>Copia de la <b>cédula de identidad</b> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal de la persona que actué como representante.</li>
      <li>En caso de apoderado, el poder debidamente notariado y copia de cedula.</li>
      <li>Carta membretada y sellada de la razón social solicitando el suministro.</li>
      <li>Sello físico de la empresa o institución</li>
    </ol>
  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="es_site_comercial_nuevo_servicio_req_area_comun" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="es-site-comercial-nuevo-servicio-req-area-comun panel callout radius">
    <h3>Requisitos</h3>
    <h4>Áreas comunes</h4>
    <p><b>Los que estén constituidos:</b></p>
    <ol>
      <li>Acta de constitución del edificio.</li>
      <li>Certificación del Registro Nacional de contribuyente (RNC).</li>
      <li>Ultima acta de Asamblea.</li>
      <li>Copia de la <b>cédula de identidad</b> del solicitante. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>
      <li>Poder de representación debidamente notariada en caso de hacerse representar, y copia de la cedula del mismo.</li>
      <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta debidamente notariada.</li>
    </ol>
    <p><b>Los que no estén constituidos:</b></p>
    <ol>
      <li>Acta de representación debidamente notariada y firmada por cada uno de los moradores del edificio.</li>
      <li>Título de propiedad, contrato de venta o alquiler de los propietarios y el solicitante del servicio para el área común.</li>
      <li>Copia de la cédula de identidad del solicitante y cada uno de los firmantes en el acta de asamblea. En caso de ser extranjero y no tener cédula, una copia del pasaporte o residencia legal.</li>
      <li>En caso de solicitud de inmuebles en trámites de compra: Contrato de compra y venta tripartito entre el cliente, banco e inmobiliaria o promesa de venta debidamente notariada.</li>
    </ol>
  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>