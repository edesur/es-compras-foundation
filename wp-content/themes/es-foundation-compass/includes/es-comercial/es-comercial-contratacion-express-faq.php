<dl class="accordion" data-accordion="">

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-01">1- ¿Qué es la contratación express?</a>
    <div id="es-faqs-content-contratacion-express-01" class="content active">
      <p>Es un nuevo producto que ofrece EDESUR a sus clientes para la contratación de nuevos servicios, ofreciendo al cliente la facilidad de por un costo mínimo no tener que acudir a ninguna oficina comercial para realizar los trámites de contratación y recibir en el lugar de su preferencia el contrato para su firma.</p>
    </div>
  </dd>

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-02">2- ¿Qué tipos de clientes pueden obtener este servicio?</a>
    <div id="es-faqs-content-contratacion-express-02" class="content">
      <p>Solo los clientes residenciales y comerciales.</p>
    </div>
  </dd>

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-03">3- ¿Puedo solicitarla desde cualquier punto del país?</a>
    <div id="es-faqs-content-contratacion-express-03" class="content">
      <p>Sí, pero hasta el momento sólo aplica para servicios de la oficina de Rómulo Betancourt.</p>
      <p>La gestión de la firma sólo se hará en el Distrito Nacional y Provincia Santo Domingo.</p>
    </div>
  </dd>

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-04">4- ¿Sólo se solicita por internet?</a>
    <div id="es-faqs-content-contratacion-express-04" class="content">
      <p>No, puede ser por tres vías:</p>
      <p>Oficina Comercial, Call Center 24Hrs e <a href="<?php echo site_url(); ?>/contratacion-express" title="Contratación Express">internet</p>
    </div>
  </dd>

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-05">5- ¿Qué costo tiene esta contratación?</a>
    <div id="es-faqs-content-contratacion-express-05" class="content">
      <p>RD$600.00 que se reflejan en su primera factura.</p>
    </div>
  </dd>

  <dd class="accordion-navigation">
    <a href="#es-faqs-content-contratacion-express-06">6- ¿Cuáles son los beneficios de la contratación express?</a>
    <div id="es-faqs-content-contratacion-express-06" class="content">
      <ul>
        <li>El solicitante en ningún momento tendrá que trasladarse a ninguna oficina comercial, ahorrando tiempo y dinero.</li>
        <li>Podrá realizar la solicitud por una de estas tres vías; telefónica, en oficina comercial o por la página Web.</li>
        <li>El contrato será firmado donde el cliente lo decida, ajustándonos a la disponibilidad del cliente.</li>
        <li>El cliente sabrá en todo momento el estado de su solicitud.</li>
        <li>Trato personalizado, por parte de los agentes de servicio y técnicos.</li>
        <li>Normalización del servicio en menos 48Hrs.</li>
        <li>Entrega de artículos promocionales.</li>
        <li>Oportunidad de participar en concursos de la empresa.</li>
      </ul>
    </div>
  </dd>
</dl>