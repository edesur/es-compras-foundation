<div class="es-site-nav-services small-12 columns">
  <!-- <div class="row">
    <div class="es-site-nav-service-single small-4 columns">
      <div class="row">
        <div class="small-4 columns">
          <a href="">
            <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-puntos-pago.png' ?>">
          </a>
        </div>
        <div class="small-8 columns">
          <h4><a href="">Puntos de pago</a></h4>
          <p>Busca la Oficina Comercial o estafeta más cerca de ti.</p>
        </div>
      </div>
    </div>
    <div class="es-site-nav-service-single small-4 columns">
      <div class="row">
        <div class="small-4 columns">
          <a href="" >
            <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-horario.png' ?>">
          </a>
        </div>
        <div class="small-8 columns">
          <h4><a href="">Horario de servicio</a></h4>
          <p>Conoce el horario de nuestras oficinas comerciales y estafetas</p>
        </div>
      </div>
    </div>
    <div class="es-site-nav-service-single small-4 columns">
      <div class="row">
        <div class="small-4 columns"  itemprop="telephone">
          <a href="tel://+18096839393">
            <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center.png' ?>">
          </a>
        </div>
        <div class="small-8 columns" itemprop="telephone" >
          <h4><a href="tel://+18096839393">Call Center</a></h4>
          <p><a href="tel://+18096839393">(809) 683-9393</a><br>
          24 horas, todos los días del año</p>
        </div>
      </div>
    </div>

    <div class="es-site-nav-service-single small-4 columns">
      <div class="row">
        <div class="small-4 columns">
          <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-clima.png' ?>">
        </div>
        <div class="small-8 columns">
          <h4><a href="">Estaciones Climáticas</a></h4>
          <p>Enterate del pronóstico del tiempo en el área de concesión.</p>
        </div>
      </div>
    </div>
  </div> -->

  <div class="row">
    <ul class="small-block-grid-3">
      <li class="es-site-nav-service-single">

        <?php
          /*<div class="row">
            <div class="small-12 large-4 columns">
              <!-- <a href="" data-reveal-id="es-puntos-de-pago">
                <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-puntos-pago.png' ?>">
              </a> -->
              <a href="" data-reveal-id="es-mantenimiento-modal">
                <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-puntos-pago.png' ?>">
              </a>
            </div>
            <div class="small-12 large-8 columns">
              <!-- <h4><a href="" data-reveal-id="es-puntos-de-pago" >Puntos de pago</a></h4> -->
              <h4><a href="" data-reveal-id="es-mantenimiento-modal" >Puntos de pago</a></h4>
              <p class="show-for-large-up">Busca la Oficina Comercial o Estafeta más cerca de ti.</p>
            </div>
          </div>*/
        ?>


        <?php $args=array(
          'name'           => 'puntos-de-pago',
          'post_type'      => 'page',
          'posts_per_page' => 1
          );
          $puntos_de_pago = get_posts($args);
        ?>

        <a href="<?php echo get_page_link($puntos_de_pago[0]->ID); ?>">
          <div class="row">
            <div class="small-12 large-4 columns">

              <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-puntos-pago.png' ?>">
            </div>
            <div class="small-12 large-8 columns">
              <h4>Puntos de pago</h4>
              <p class="show-for-large-up">Busca la Oficina Comercial o Estafeta más cerca de ti.</p>
            </div>
          </div>
        </a>
      </li>

      <li class="es-site-nav-service-single">
        <?php
          /*<div class="row">
            <div class="small-12 large-4 columns">
              <a href="" data-reveal-id="es-horario-de-servcios">
                <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-horario.png' ?>">
              </a>
            </div>
            <div class="small-12 large-8 columns">
              <h4><a href="" data-reveal-id="es-horario-de-servcios">Horario de servicio</a></h4>
              <p class="show-for-large-up">Conoce el horario de nuestras Oficinas Comerciales y Estafetas</p>
            </div>
          </div>*/
        ?>

        <a href="" data-reveal-id="es-horario-de-servcios">
          <div class="row">
            <div class="small-12 large-4 columns">

              <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-horario.png' ?>">
            </div>
            <div class="small-12 large-8 columns">
              <h4>Horario de servicio</h4>
              <p class="show-for-large-up">Busca la Oficina Comercial o Estafeta más cerca de ti.</p>
            </div>
          </div>
        </a>
      </li>

      <li class="es-site-nav-service-single">
        <div class="row">
          <div class="small-12 large-4 columns"  itemprop="telephone">
            <a class="show-for-small-only" href="tel://+18096839393">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center.png' ?>">
            </a>
            <a class="show-for-medium-up" href="" data-reveal-id="es-call-center-promo">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center.png' ?>">
            </a>
          </div>
          <div class="small-12 large-8 columns" itemprop="telephone" >
            <h4>
              <a class="show-for-small-only" href="tel://+18096839393">Call Center</a>
              <a class="show-for-medium-up" href="tel://+18096839393" data-reveal-id="es-call-center-promo">Call Center</a>
            </h4>
            <p class="show-for-large-up"><a href="tel://+18096839393" data-reveal-id="es-call-center-promo">(809) 683-9393</a><br>
            <!-- <a href="tel://+18096839393" data-reveal-id="es-call-center-chat">Chat</a> -->
            24 horas, todos los días del año</p>
          </div>
        </div>


      </li>
    </ul>
  </div>
</div>