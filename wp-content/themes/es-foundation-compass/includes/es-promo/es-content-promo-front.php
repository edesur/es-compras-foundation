  <?php
      /**
       * The WordPress Query class.
       * @link http://codex.wordpress.org/Function_Reference/WP_Query
       *
       */
      $args_promocion = array(
        'post_type'   => 'promocion',
        'post_status' => 'publish',
        'order'               => 'DESC',
        'orderby'             => 'date',
        'posts_per_page'         => 1,
      );

    $es_promocion_query = new WP_Query( $args_promocion );
  ?>

  <?php if ( $es_promocion_query->have_posts() ) : ?>

    <div id="es_content_promocion_front" class="reveal-modal medium" data-reveal>

    <?php while ( $es_promocion_query->have_posts() ) : $es_promocion_query->the_post(); ?>

      <?php
        $args_promocion_files = array(
           'post_type' => 'attachment',
           'numberposts' => 1,
           'post_parent' => $post->ID
        );

        $es_promocion_files = get_posts( $args_promocion_files );
        $es_promocion_files_url = wp_get_attachment_url( $es_promocion_files[0]->ID );
        $es_promocion_files_img = wp_get_attachment_image( $es_promocion_files[0]->ID,  'full' );
      ?>

      <?php if ($es_promocion_files): ?>

        <?php echo $es_promocion_files_img; ?>
        <a class="close-reveal-modal">&#215;</a>


      <?php endif ?>

    <?php endwhile; ?>

    </div>

    <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <<!-- div id="es_content_promocion_front" class="es-site-comercial-contratacion-express reveal-modal" data-reveal>

        <div class="row">
           <div class="small-12 large-6 columns">
             <div class="es-site-contratacion-express-tagline panel">

               <img src="<?php echo get_template_directory_uri() . '/images/es-site-logo-comercial-contratacion-express.png' ?>" alt="">
               <h2 class="text-center">¡Servicio de contratación<br> donde quiera que estés!</h2>

             </div>
           </div>
           <div class="small-12 large-6 columns">
              <h3>Con Contratación Express</h3>
              <ul>
                <li>Ahorras tiempo y dinero</li>
                <li>Recibes atención personalizada</li>
                <li>Nos ajustamos a tu tiempo y al lugar que decidas</li>
                <li>Siempre sabrás el estado de tu solicitud</li>
                <li>Firmas el contrato en el lugar de tu preferencia</li>
                <li>La Instalación es en menos de 48 hrs</li>
              </ul>

              <h4>Fácil de solicitar:</h4>
              <ul class="no-bullet">
                <li class="text-center"><a href="<?php echo site_url(); ?>/solicitud-nuevo-servicio" class="button small radius">Solicita en línea</a></li>
                <li>Llamando al <a href="tel://+18096839393">Call Center</a> 24 horas <a href="tel://+18096839393">(809) 683-9393</a></li>
                <li>En cualquiera de nuestras oficinas comerciales.</li>
              </ul>
           </div>
        </div>

        <a class="close-reveal-modal">&#215;</a>
      </div> -->
  <?php endif; ?>

