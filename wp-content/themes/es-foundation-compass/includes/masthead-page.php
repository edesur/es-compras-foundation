<section class="es-site-masthead small-12 columns">

  <?php if (is_page('formulario-enviado')): ?>

  <?php else: ?>
    <div class="row">
      <div class="es-site-masthead-title es-site-masthead-title-article es-site-masthead-noticias-articulo-titulo small-12 columns">
        <h2>
          <?php

            if (is_post_type_archive()) {

              post_type_archive_title();

            } elseif ('interrupcion' == get_post_type()) {
              echo 'Mantenimientos Programados';
            } elseif ( is_tax()  ) {
              $tax_obj = $wp_query->get_queried_object();

              if ($post) {
                $post_obj = get_post_type_object( $post->post_type );
                echo $post_obj->labels->name . " - " . $tax_obj->name;
              } else {
                echo $tax_obj->name;
              }

            } elseif ( is_search() ) {
              printf( __( 'Resultados de búsqueda: %s', 'es-foundation-compass' ), '<span>' . get_search_query() . '</span>' );# code...
            } else {
              the_title();
            }

          ?>
        </h2>
      </div>
    </div>
  <?php endif ?>

  <?php get_template_part('/includes/es-bar-app') ?>
</section>