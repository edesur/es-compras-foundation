<?php

  /**
 * The template for displaying all single pages.
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  get_header();

  get_template_part( '/includes/masthead-page' );

 ?>

<section class="es-site-section-search-content es-site-section-wrapper small-12 medium-12 large-12 columns">
  <div class="row">

    <div class="row collapse">
      <div class="small-12 columns">
        <h3><?php printf( __( 'Resultados de búsqueda: %s', 'es-foundation-compass' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
      </div>
    </div>

    <div class="es-site-empresa-historia es-site-section-wrapper-background small-12 medium-12 columns">

      <?php if ( have_posts() ) : ?>

      <?php /* Start the Loop */ ?>
      <?php while ( have_posts() ) : the_post(); ?>

        <?php
        /**
         * Run the loop for the search to output the results.
         * If you want to overload this in a child theme then include a file
         * called content-search.php and that will be used instead.
         */
        get_template_part( 'content', 'search' );
        ?>

      <?php endwhile; ?>

      <?php /*esd_underscores_paging_nav();*/ ?>

      <?php else : ?>

      <?php get_template_part( 'content', 'none' ); ?>

      <?php endif; ?>

    </div>
  </div>
</section>


<?php get_footer(); ?>