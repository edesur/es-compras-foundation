<section class="es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row">
    <div class="small-12 medium-12 large-10 columns">
      <h3>Normativas</h3>
      <p>EDESUR se reserva el derecho de modificar las normativas en cualquier momento mediante la publicación de las mismas actualizadas en el sitio web. Por lo tanto, siempre rige la versión más actualizada de todas las normativas.</p>
      <p>Normativas que rigen los procedimientos de las licitaciones en nuestro Departamento de Compras.</p>
    </div>
  </div>

  <div class="es-site-file-container es-site-section-wrapper small-12 columns">
    <div class="row">
      <div class="small-12 columns">
        <h4>Descargue las Normativas</h4>
      </div>
    </div>
    <div class="row">
      <div class="small-6 columns">
        <div class="es-site-file-icon small-2 medium-2 large-2 columns">
          <a href="<?php echo get_template_directory_uri() . '/docs/normativas-licitaciones.pdf' ?>" title="Normativas de Licitación"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-pdf.png' ?>" alt="Normativas de Licitación icono PDF"></a>
        </div>
        <div class="es-site-file-title small-8 medium-6 large-6 columns">
          <a href="<?php echo get_template_directory_uri() . '/docs/normativas-licitaciones.pdf' ?>" title="Normativas de Licitación">Normativas de Licitación</a>
        </div>
        <div class="es-site-file-icon-download small-2 medium-2 large-2 columns end">
          <a href="<?php echo get_template_directory_uri() . '/docs/normativas-licitaciones.pdf' ?>" download="<?php echo get_template_directory_uri() . '/docs/normativas-licitaciones.pdf' ?>" title="Descargue las Normativas de Licitación"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-download.png' ?>" alt="Normativas de Licitación icono descarga"></a>
        </div>
      </div>

      <div class="small-6 columns end">
        <div class="row">
          <div class="es-site-file-adobe-reader-copy small-8 medium-8 large-8 columns">
            <small>Si no puede ver los documentos, es posible que no tenga instalado <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader">Adobe Reader</a>. Presione la imagen para instalarlo.</small>
          </div>
          <div class="es-site-file-icon-adobe-reader small-4 medium-4 large-4 columns">
            <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-get-adobe-reader.gif' ?>" alt="Descargue Adobe Reader"></a>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>