<section class="es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row">
    <div class="es-site-form-confirmation es-site-section-wrapper-background small-12 medium-12 columns text-center">

      <div class="row">
        <div class="small-12 columns">
          <h5 class="subheader">Muchas gracias por su Solicitud de Nuevo Servicio, brevemente nos estaremos comunicando con usted vía telefónica o correo electrónico.</h5>

          <p class="subheader">Para dar seguimiento a su solicitud puede hacerlo via telefónica en nuestro Call Center</p>
        </div>
      </div>

      <div class="row">

        <div class="small-12 medium-4 columns medium-offset-4">
        <a class="button large expand info show-for-small-only" href="tel://+18096839393">
          <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-app-icon-call-center-form.png"> Call Center
        </a>
        <a  class="es-site-form-confirmation-button-cc button large expand info show-for-medium-up" href="tel://+18096839393" data-reveal-id="es-call-center-promo">
        <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-app-icon-call-center-form.png"> (809) 683-9393
        </a>
        </div>
      </div>

      <div class="row">
        <div class="small-12 columns">
          <h5 class="subheader">Para estar conectado e informado, le invitamos a seguirnos en nuestras redes sociales</h5>
        </div>
      </div>

      <div class="row">
        <div class="small-12 medium-4 columns large-offset-4">
          <ul class="small-block-grid-2 medium-block-grid-6">
            <li><a href="https://www.facebook.com/EdesurDominicana" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-facebook.png">
            </a></li>
            <li><a href="https://twitter.com/EdesurRD" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-twitter.png">
            </a></li>
            <li><a href="https://www.instagram.com/edesurrd/" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-instagram.png">
            </a></li>
            <li><a href="https://www.youtube.com/user/EdesurDominicana" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-youtube.png">
            </a></li>
            <li><a href="https://www.pinterest.com/edesurrd" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-pinterest.png">
            </a></li>
            <li><a href="https://foursquare.com/v/edesur/4c27ca3997d00f474eec3eea" class="button large" target="_blank">
              <img src="http://www.edesur.com.do/wp-content/themes/es-foundation-compass/images/es-site-social-icon-foursquare.png">
            </a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>

</section>