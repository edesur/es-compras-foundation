jQuery(document).ready(function($) {

  var $S = Foundation.utils.S;

  console.log($S('[enctype="multipart/form-data"]'));

  var ConsultaNIC = {
  init: function(config) {
    // console.log('Initialized!');



    this.config = config;
    this.bindEvents();
  },
  bindEvents: function () {
    // console.log('Events Fired!');


    var $self = ConsultaNIC,

    $es_nuevo_sevicio_form = this.config.es_nuevo_sevicio_form,
    $es_nuevo_sevicio_form_field_req = $es_nuevo_sevicio_form.find('.ninja-forms-field');

    $es_nuevo_sevicio_form.attr('data-abide', '');

     $es_nuevo_sevicio_form_field_req_error_msg = function ($required_field, $error_text) {
        $es_nuevo_sevicio_form_field_req_error = $('<small></small>', {
          class: 'error',
          html: $error_text
        });
        $required_field.after($es_nuevo_sevicio_form_field_req_error);

        $required_field.on('keypress', function(e) {
          if (e.which == 35 || e.which == 36 || e.which == 37 || e.which == 38 || e.which == 40 || e.which == 41 || e.which == 45 || e.which == 60 || e.which == 62  || e.which == 63 || e.which == 91 || e.which == 92 || e.which == 93 || e.which == 123 || e.which == 124 || e.which == 125) {
            $S(this).parent('.field-wrap').addClass('error');
            return false;
          } else {
            $S(this).parent('.field-wrap').removeClass('error');
          }
        });
     };

      $es_nuevo_sevicio_form_field_req_error_msg( $es_nuevo_sevicio_form_field_req, 'No se permiten caracteres como "< #\\ $  % & > "');


    this.config.es_nuevo_sevicio_previo_seleccion.on('change', function(event) {
      // event.preventDefault();

      if ($S(this).val() === 'si'  && $S('.es-nuevo-servicio-contrato-previo-nic').val() === '') {
        $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().hide();
        $S('.ninja-forms-mp-nav-wrap').hide();
      } else {
        $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().show();
        $S('.ninja-forms-mp-nav-wrap').show();
         $S('.es-consulta-nic-error').remove();
         $S('.es-nuevo-servicio-contrato-previo-nic').val('');
      }
    });

    this.config.es_nuevo_sevicio_nic_previo.on('keyup', function(event) {
      event.preventDefault();
      var $es_nic_previo_val = $S(this).val();

      if ($S('.es-consulta-nic-error').length) {
        $S('.es-consulta-nic-error').remove();
        console.log('Trying to erase the warning!!!!')
        }

      if(!isNaN($es_nic_previo_val)) {

        if ($S('.es-consulta-nic-error').length) {
          $S('.es-consulta-nic-error').remove();
          console.log('Trying to erase the warning!!!!')
          }

        $self.consulta();
      } else {
        $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().hide();
        $S('.ninja-forms-mp-nav-wrap').hide();
      }
    });

  },
  consulta : function () {
    var $self  = ConsultaNIC;


    $self.getDeuda();
  },
  getDeuda: function () {
    console.log('Consulta de deuda');
    var $self = ConsultaNIC,
    $results = {};
    $.ajax({
      url: '../wp-content/themes/es-foundation-compass/includes/es-comercial/es-comercial-nuevo-servicio-consulta.php',
      cache: false,
      type: 'POST',
      dataType: 'json',
      data: this.config.es_nuevo_sevicio_form.serialize(),
      complete: function () {
        console.log('Consulta completa');
      },
      success: function ($results) {
        // console.log($('.es-nuevo-servicio-contrato-previo-nic').parent('.field-wrap'));
        console.log('Consulta exitosa!!!!');
        $es_nic_previo = $S('.es-nuevo-servicio-contrato-previo-nic');
        console.log($results);

        function toTitleCase(str){
          return str.replace(/\w+/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        $es_consulta_codigo = $results.CodigoRespuesta;
        $es_consulta_deuda_vencida = $results.DeudaVencida;

        var  $es_consulta_nic_error = $('<div></div>',  {
          class: 'es-consulta-nic-error large-8 columns '
        }),
        $es_consulta_nic_error_alert = $('<div></div>', {
          class: 'alert-box'
        }),
        $es_consulta_nic_error_label = $('<strong></strong>'),
        $es_consulta_nic_error_message = $('<p></p>'),
        $es_consulta_nic_error_callcenter = '<a href="tel://+18096839393" class="button tiny round info" style="margin: 0; padding: 0.25rem 0.375rem;">(809) 683-9393</a>',
        $es_consulta_nic_error_msg_small = $('<small></small>'),
        $es_consulta_nic_previo_wrap = $S('.es-nuevo-servicio-contrato-previo-nic-wrap').parent();

        var $es_consulta_nic_error_append = function ($label_copy, $message_copy) {

          $es_consulta_nic_error_alert.css({
            padding: '0.375rem'
          });

          $es_consulta_nic_error_message.css({
            fontSize: '95%',
            margin: 0
          });

          $es_consulta_nic_error_label.append($label_copy);
          $es_consulta_nic_error_message.append($message_copy);
          $es_consulta_nic_error.append( $es_consulta_nic_error_alert);
          $es_consulta_nic_error_alert.append( $es_consulta_nic_error_label);
          $es_consulta_nic_error_alert.append( $es_consulta_nic_error_message);

          if ($S('.es-consulta-nic-error').length) {
            $S('.es-consulta-nic-error').remove();
            $es_consulta_nic_previo_wrap.after( $es_consulta_nic_error);
          } else {

            $es_consulta_nic_previo_wrap.after( $es_consulta_nic_error);
          }
        }

          if ($es_consulta_codigo === 'RC001'  && $es_consulta_deuda_vencida == 0) {
            $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().show();
            $S('.ninja-forms-mp-nav-wrap').show();
            $es_consulta_nic_error_alert.addClass('success');
            $es_consulta_nic_error_append('NIC válido', 'El Numero de Contrato (NIC) suministrado es correcto y no presenta deuda, puede continuar con su solicitud.');
          } else if ($es_consulta_codigo === 'RC001'  && $es_consulta_deuda_vencida > 0) {
            $es_consulta_nic_error_alert.addClass('alert');
            $es_consulta_nic_error_append('Deuda Pendiente', 'Este número de NIC presenta una deuda pendiente, favor llame al  ' + $es_consulta_nic_error_callcenter + '  para realizar el pago y proceder con esta solicitud.');
            $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().hide();
            $S('.ninja-forms-mp-nav-wrap').hide();
          } else {
            $S('.ninja-forms-mp-breadcrumbs').find('input[value="Datos del cliente"]').parent().hide();
            $S('.ninja-forms-mp-nav-wrap').hide();
          }

        switch ($es_consulta_codigo) {
          case 'RC001':
            break;

          case 'RC002':

            $es_consulta_nic_error_append('NIC no válido', 'Este número de NIC no es válido, favor confirmarlo nuevamente  en la esquina superior derecha de su factura o llamando al ' + $es_consulta_nic_error_callcenter);


            break;
            case 'RC003':
            break;
            case 'RC004':
            break;
          default:
            // statements_def
            break;
        }

        // var  $es_consulta_validation = $S('</small>', {
        //   'className': 'es-comercial-nuevo-servicio-validation error',
        //   'content': 'El NIC no es valido'
        // });
        // <div id="ninja_forms_field_274_error" style="" class="ninja-forms-field-error">
        //   <p>Es necesario completar este campo</p>  </div>

        if ($results.DeudaVencida !== null) {

          console.log('Consulta Existosa');
          console.log($results);
          console.log(parseFloat($results.DeudaVencida));
          // console.log(toTitleCase($results.NombreTitular));

        };

        if ($results.NombreTitular !== null) {

          $('.es-nuevo-servicio-contrato-previo-nic').parent('.field-wrap').removeClass('ninja-forms-error');
          $('.es-nuevo-servicio-contrato-previo-nic').next('.ninja-forms-field-error').html('Por favor digite un NIC valido').css('display', 'none');

          // $('#ninja_forms_field_59').val(toTitleCase(results.NombreTitular));
          // $('#ninja_forms_field_59').parent('.field-wrap').removeClass('ninja-forms-error');
          // $('#ninja_forms_field_59').next('.ninja-forms-field-error').css('display', 'none');


        } else {
          // $('#ninja_forms_field_59').val(toTitleCase(''));
          $('.es-nuevo-servicio-contrato-previo-nic').parent('.field-wrap').addClass('ninja-forms-error');
          $('.es-nuevo-servicio-contrato-previo-nic').next('.ninja-forms-field-error').html('Por favor digite un NIC valido').css('display', 'block');
        };



      },
      fail: function () {
        console.log('Consulta Fracasada');
      }
    });
  }


  }

  ConsultaNIC.init({
    es_nuevo_sevicio_form:      $S('.ninja-forms-form'),
    es_nuevo_sevicio_previo_seleccion: $S('.es-nuevo-servicio-contrato-previo-seleccion'),
    es_nuevo_sevicio_nic_previo: $S('.es-nuevo-servicio-contrato-previo-nic')
  });

  // console.log('Prende el Bombillo')

});