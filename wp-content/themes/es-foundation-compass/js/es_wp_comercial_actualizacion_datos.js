jQuery(document).ready(function($) {

  // Hide/Show Address form fields depending on selection
  var $es_site_comercial_datos_show_hide_address = (function () {


    var $S = Foundation.utils.S;

    $S('input.ninja-forms-field-conditional-listen[type=radio]').change(function() {
           if ($(this).val() == 'si') {


            $S('.es-site-actualizacion-address-field').parent('.field-wrap').css('display', 'none');

            $S('.es-site-actualizacion-address').slideDown(500, function() {
              $S('.es-site-actualizacion-address-field').parent('.field-wrap').css('display', 'none');


              $S('.es-site-actualizacion-address-field').parent('.field-wrap')
              .slideDown({
                'duration': 500,
                'queue'   : true
              });
            });

           }
           else if ($(this).val() == 'no') {
              $S('.es-site-actualizacion-address-field').parent('.field-wrap').slideUp('slow', function () {
                $S('.es-site-actualizacion-address').slideUp('slow');
              });

           }
       });

  })();

});