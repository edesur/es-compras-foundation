jQuery(document).ready(function($) {

  var $S = Foundation.utils.S,
      $es_site_nav_top_wrapper = $S('.es-site-nav-top-wrapper'),
      $es_site_nav_top = $es_site_nav_top_wrapper.find('.es-site-nav-top'),
      $es_site_nav_item = $S('.es-site-nav-item'),
      $es_site_nav_top_rd_shield = $S('.es-site-nav-top-rd-shield'),
      $es_site_nav_search = $S('.es-site-nav-search'),
      $es_site_nav_search_wrapper = $S('.es-site-nav-search-wrapper'),
      $es_site_nav_search_input = $S('.es-site-nav-search').find('input'),
      $es_site_nav_search_button = $S('.es-site-nav-search').find('.button')
      $es_site_nav_addthis = $S('.es-site-nav-addthis');


      // es-site-nav-top-rd-shield

  if ($S('#wpadminbar').length) {
    $es_site_nav_top_wrapper.css('margin-top', $S('#wpadminbar').outerHeight() + 'px');
    $S('#wpadminbar').css('z-index', 999999);
    // console.log('Yes there is wp-admin-bar');
  };

  // $S('.f-topbar-fixed').css('padding-top', '0px');

  $es_site_nav_top.find('.right li:has(.addthis_horizontal_follow_toolbox)').css({
      'width': '223px',
  });

  if (matchMedia(Foundation.media_queries['large']).matches){

    // $es_site_nav_search;
    //
    $es_site_nav_top_rd_shield.css('width', ($es_site_nav_search.width()) + 'px');
    // console.log($es_site_nav_top_rd_shield.parent().width());
    // console.log(($es_site_nav_top_rd_shield.parent().width()) - $es_site_nav_top_rd_shield.outerWidth()  + 'px');
    $es_site_nav_top_rd_shield.siblings('.small-10').css('width', ($es_site_nav_top_rd_shield.parent().outerWidth()) - $es_site_nav_top_rd_shield.width()  + 'px');

   $es_site_nav_top.find('.right li:has(.addthis_horizontal_follow_toolbox)').css({
       'padding': $es_site_nav_top.find('li:not(.has-form)').find('a:not(button)').css('padding')
   });
  } else {
    $es_site_nav_top.find('.right li:has(.addthis_horizontal_follow_toolbox)').css({
        'padding': $es_site_nav_top.find('li').find('a').css('padding')
    });
  };

  // $es_site_nav_search_wrapper.css('padding-top', ($S('.es-site-nav-top').outerHeight() - $es_site_nav_search.outerHeight())/2 + 'px');

  // $es_site_nav_search_button.css('height', $es_site_nav_search_input.outerHeight() + 'px');

  // console.log(matchMedia(Foundation.media_queries['xlarge']).matches);
  // console.log(matchMedia(Foundation.media_queries).matches);

  if (matchMedia(Foundation.media_queries.medium).matches) {
    // $es_site_nav_search_wrapper.hide();
    // $es_site_nav_search_wrapper.find('input[type=text]').hide();
    // $es_site_nav_addthis.hide();
    // $S('.es-site-compra-proceso-list-single-wrapper').css('height', $S('.es-site-compra-proceso-list-single-wrapper').parent('.es-site-compra-proceso-list-single').height() +'px');
  };

  // $S('.es-site-compra-proceso-list-single-wrapper');


  function loadAddThis() {

      var $es_site_social_share = $S('.atss-left');

        // console.log($es_site_social_share);

      // console.log($es_site_social_share);
  }


  // window.setTimeout(loadAddThis, 3000);

  var $es_site_nav_top_add_bg = (function() {

    var $es_site_home_masthead_heading = $S('.es-site-masthead-title'),
        $es_site_home_masthead_heading_pos = $es_site_home_masthead_heading.position(),
        $es_site_noticias_articulo = $S('.es-site-noticias-articulo'),
        $es_site_noticias_articulo_pos = $S('.es-site-noticias-articulo').position(),
        $es_site_nav_services = $S('.es-site-nav-services'),
        $es_site_nav_services_pos = $es_site_nav_services.position();

    // if (matchMedia(Foundation.media_queries['large']).matches){
    //   $es_site_nav_search_wrapper.hide();
    //   $es_site_nav_addthis.hide();
    // };

    var es_position = $S(window).scrollTop();

    $S(window).on('scroll', function() {

      var es_scroll = $S(window).scrollTop();

      // Position the $es_site_social_share bar inside the article
      var $es_site_social_share = $S('.atss-left');

      if ($es_site_social_share.length) {
        if ($S(this).scrollTop() >= $es_site_noticias_articulo_pos.top - $es_site_nav_top.outerHeight()) {
          $es_site_social_share.css('top', '80px');
        } else{
          $es_site_social_share.css('top', ($es_site_noticias_articulo_pos.top - $S(this).scrollTop()) + 16 + 'px');
        };

        if ($S(this).scrollTop() >= $es_site_nav_services_pos.top - $es_site_social_share.outerHeight() - $es_site_nav_top.outerHeight() ) {
          $es_site_social_share.css('top', $es_site_nav_services_pos.top - $S(this).scrollTop() - $es_site_social_share.outerHeight() + 'px');
        }
      };

      // Change the opacity of the nav bar by togling the es-site-nav-top-wrapper-bg class
      if ($S(this).scrollTop() >= ($es_site_home_masthead_heading_pos.top - $S('.es-site-nav-top-wrapper').height())) {
        $S('.es-site-nav-top-wrapper').addClass('es-site-nav-top-wrapper-bg');
      } else {
        $S('.es-site-nav-top-wrapper').removeClass('es-site-nav-top-wrapper-bg');
      };

      // if (matchMedia(Foundation.media_queries['medium']).matches) {
      //   if (es_scroll > es_position) {
      //     console.log('down');

      //     var esSlideDownSearchAddThis = function  () {
      //       $es_site_nav_search_wrapper.slideDown({
      //         'duration': 400
      //       });

      //       $es_site_nav_addthis.slideDown({
      //         'duration': 400
      //       });
      //     };

      //     $es_site_nav_item.slideUp({
      //       'duration': 400,
      //       'queque': true,
      //       'complete': esSlideDownSearchAddThis,
      //       'done': $es_site_nav_item.hide()
      //     });

      //   } else {
      //     console.log('up');

      //     var esSlideDownNavItem = function () {

      //       $es_site_nav_item.slideDown({
      //         'duration': 400
      //       });
      //     }

      //     $es_site_nav_addthis.slideUp({
      //       'duration': 400,
      //       'done': $es_site_nav_addthis.hide()
      //     });

      //     $es_site_nav_search_wrapper.slideUp({
      //       'duration': 400,
      //       'queque': true,
      //       'complete': esSlideDownNavItem,
      //       'done': $es_site_nav_search_wrapper.hide()
      //     })

      //   };

      //   es_position = es_scroll;
      // };

    });
  })();

  /*var $es_resize_footer_elements = (function () {

    if (matchMedia(Foundation.media_queries.large).matches) {
      var $es_contact_info = $S('.es-site-contact-field-box'),
            $es_contact_info_label = $S('.es-site-contact-field-box .es-site-contact-field:first-child').find('label'),
            $es_contact_textarea = $S('.es-site-contact-textarea-box').find('textarea');

      // $es_contact_textarea.css( 'height', ($es_contact_info.outerHeight() - $es_contact_info_label.outerHeight(true)));

      $S('.es-site-footer-map-container').css('padding-top', $S('.es-site-contact-textarea-box').find('label').outerHeight(true));

      $S('.es-site-footer-address').css('padding-top', $S('.es-site-contact-textarea-box').find('label').outerHeight(true));

      $S('.es-site-footer-map').css('height', $S('.es-site-footer-contact-form').outerHeight() - $S('.es-site-footer-contact-textarea-box').css('margin-bottom').replace(/[^-\d\.]/g, '') - $S('.es-site-contact-form-submit').css('margin-bottom').replace(/[^-\d\.]/g, '') - $S('.es-site-contact-textarea-box').find('label').outerHeight(true) - $S('.es-site-contact-field-account-info').outerHeight(true) + 'px');
    } else { $S('.es-site-footer-map').css('height', '320px'); };


    // console.log($S('.es-site-footer-contact-textarea-box').css('margin-bottom').replace(/[^-\d\.]/g, ''));
  })();*/

  var es_position_top_bar_social = (function () {

    var $es_site_top_bar_social = $S('.es-site-top-bar-social'),
        $es_site_noticias = $S('.es-site-noticias'),
        $es_site_noticias_pos = $es_site_noticias.position(),
        $es_site_nav_services = $S('.es-site-nav-services'),
        $es_site_nav_services_pos = $es_site_nav_services.position();

        $es_site_top_bar_social.css({
            'top': '64px',
            'right': 0
          });

        $S(window).scroll(function() {

           if ($S(this).width() < 1360 &&
               $es_site_noticias.length &&
               ($S(this).scrollTop() + $es_site_top_bar_social.outerHeight()) >=
               $es_site_noticias_pos.top &&
               ($S(this).scrollTop()) <=
               ($es_site_top_bar_social.outerHeight() + $es_site_noticias.outerHeight())
               ) {
            $es_site_top_bar_social.css({
              'right': '-30px'
            });

            $es_site_top_bar_social.on('mouseenter', function(event) {
              event.preventDefault();
              $es_site_top_bar_social.css({
                'right': 0
              });
            });

            $es_site_top_bar_social.on('mouseleave', function(event) {
              event.preventDefault();
              $es_site_top_bar_social.css({
                'right': '-30px'
              });
            });

          } else{
            $es_site_top_bar_social.css({
              'top': '64px',
              'right': 0
            });
          };

        });

  })();

  // Set background color for preguntas-frecuentes accordion
  var es_faq_set_accordion_nav_bckg = (function () {
    var es_site_faqs_tabs_content = $S('.es-site-faqs-tabs-content'),
        es_site_faqs_tabs_content_nav = es_site_faqs_tabs_content.find('.accordion-navigation');
        // es_site_faqs_tabs_content_nav.has('.content.active').find('a').on('click', 'a', function(event) {
        //   event.preventDefault();
        //   $(this).css('color', 'white');
        // });
        // console.log(es_site_faqs_tabs_content_nav.has('.content.active').find('a'));
  })();

  console.log($S('.recaptcha-wrap').parent('.small-12.medium-6.columns'));

  $(window).load(function() {

     console.log(Foundation.utils.is_small_only());

     if (Foundation.utils.is_small_only()) {
      $S('.recaptcha-wrap .g-recaptcha').find('div[style]').css('width', '18rem');
     };
  });

  // Open modal for New User Registration when document loads
  var es_reg_requisitos        = $S('#esRegRequisitos'),
      es_site_masthead_modal_content = $S('.es-site-masthead-modal-content'),
      es_reg_requisitos_button = es_site_masthead_modal_content.find('.es-site-masthead-modal-button');
  es_reg_requisitos.foundation('reveal', 'open');

  es_reg_requisitos_button.on('click', function(event) {
    event.preventDefault();
    es_site_masthead_modal_content.foundation('reveal', 'close');

  });


  // Fade alerts in Registration Forms
  window.setTimeout(fadeAlert, 5000);

  function fadeAlert () {
    $S('.es-site-masthead-form-title-alert').fadeOut('1000');
  }

  // Noticias Slider settings

  if ($S('.es-site-noticias-articulos').length) {
    $('.es-site-noticias-wrapper').slick({
      // arrows: false,
      dots: true,
      infinite: false,
      slide: 'article',
      slidesToShow: 3,
      slidesToScroll: 3
    });
  };

  var $es_compra_proceso_list_add_margin_dwnld_btn = (function () {
      // $S('.es-site-compra-proceso-list-single-download-button').css('margin-top', $S('.es-site-compra-proceso-list-single-download').prev().outerHeight() - $S('.es-site-compra-proceso-list-single-download-button').outerHeight() + 'px');
    })();

  var $es_site_file_container = $S('.es-site-file-container'),
      $es_site_file_title = $es_site_file_container.find('.es-site-file-title'),
      $es_site_file_icon = $es_site_file_container.find('.es-site-file-icon'),
      $es_site_file_adobe_reader_copy = $S('.es-site-file-adobe-reader-copy'),
      $es_site_file_icon_adobe_reader = $S('.es-site-file-icon-adobe-reader').find('a');
  $es_site_file_title.css('line-height', $es_site_file_icon.outerHeight() + 'px');
  // $es_site_file_icon_adobe_reader.css('line-height', $es_site_file_icon.outerHeight() + 'px');
  // $es_site_file_adobe_reader_copy.css('margin', ($es_site_file_icon.outerHeight() - $es_site_file_adobe_reader_copy.outerHeight()) / 2 + 'px 0');

  // Interrupciones Media queries

  var es_site_set_high_docs_pdf = (function () {

    if (Foundation.utils.is_large_up()) {

      $S('.gde-frame').css('height', '1040px');

    } else if (Foundation.utils.is_medium_up()) {

      $S('.gde-frame').css('height', '520px');

    } else if (Foundation.utils.is_small_up()) {

      $S('.gde-frame').css('height', '374px');

    };

  })();

  // Hide/Show Cédula and NIC form fields depending on selection
  var $es_site_show_hide_contact_account_info = (function () {
    var $es_site_contact_field_account_info = $S('.es-site-contact-field-account-info'),
        $es_site_contact_field_acct_info_label = $es_site_contact_field_account_info.find('label'),
        $es_site_contact_field_acct_info_input = $es_site_contact_field_account_info.find('input');;

    $es_site_contact_field_account_info.hide();

    $S('#es-site-contact-dropdown-select').on('change', function(event) {

      var $this = $S(this),
          $es_site_contact_option = $this.find('option:selected'),
          $es_site_contact_option_selected = $es_site_contact_option.val();

      switch($es_site_contact_option_selected) {
        case 'facturacion':
          $es_site_contact_field_account_info.show('slow');
          $es_site_contact_field_account_info.find('input').attr('required', true);

        break;

        case 'solicitar-lectura':
        console.log($this)
          $es_site_contact_field_account_info.show('slow');
          $es_site_contact_field_account_info.find('input').attr('required', true);

        break;

        case 'factura-email':
          console.log($this)
          $es_site_contact_field_account_info.show('slow');
          $es_site_contact_field_account_info.find('input').attr('required', true);

        break;

        case 'cambio-titular':
          console.log($this)
          $es_site_contact_field_account_info.show('slow');
          $es_site_contact_field_account_info.find('input').attr('required', true);

        break;

        case 'cambio-potencia':
          console.log($this)
          $es_site_contact_field_account_info.show('slow');
          $es_site_contact_field_account_info.find('input').attr('required', true);

        break;

        default:
          $es_site_contact_field_account_info.hide('slow');
          $es_site_contact_field_account_info.find('input').removeAttr('required');
        break;
      }
    });
  })();


  // Mercadeo - Prende el bombillo
  var $es_site_prende_bombillo_func = (function () {

    // NinjaForms Scripts

    // $S('.ninja-forms-form').attr('data-abide', '');

    var $es_site_prende_bombillo_ninja_form_content = $S('.es-site-mercadeo-prende-bombillo').find('.ninja-forms-cont'),
        $es_site_prende_bombillo_ninja_form = $es_site_prende_bombillo_ninja_form_content.find('.ninja-forms-form'),
        $es_site_prende_bombillo_ninja_form_progress = $es_site_prende_bombillo_ninja_form_content.find('.meter.nostripes.progressbar'),
        $es_site_prende_bombillo_ninja_form_meter = $es_site_prende_bombillo_ninja_form_progress.find('span'),
        $es_site_prende_bombillo_ninja_form_breadcrumbs = $es_site_prende_bombillo_ninja_form_content.find('.ninja-forms-mp-breadcrumbs'),
        $es_site_prende_bombillo_ninja_form_nav = $es_site_prende_bombillo_ninja_form_content.find('.ninja-forms-mp-nav'),
        $es_site_prende_bombillo_ninja_forms_error = $es_site_prende_bombillo_ninja_form.find('.ninja-forms-error'),
        $es_site_prende_bombillo_ninja_forms_field_error = $es_site_prende_bombillo_ninja_forms_error.find('.ninja-forms-field-error');

    // $es_site_prende_bombillo_ninja_form.attr({
    //   'data-abide': '',
    //   'novalidate': 'novalidate'
    // });
    // $S('.ninja-forms-req').attr('required', '');
    $es_site_prende_bombillo_ninja_form_progress.addClass('progress round').removeClass('meter nostripes progressbar');
    $es_site_prende_bombillo_ninja_form_meter.addClass('meter');
    $es_site_prende_bombillo_ninja_form_breadcrumbs.addClass('button-group even-4 stack-for-small');
    $es_site_prende_bombillo_ninja_form_nav.addClass('button tiny info');

    console.log($es_site_prende_bombillo_ninja_form);
    // console.log($es_site_prende_bombillo_ninja_forms_error);
    if ($es_site_prende_bombillo_ninja_forms_error.length) {
      console.log($es_site_prende_bombillo_ninja_forms_field_error);
      console.log( 'Error Test');
    };

    // NinjaForms Scripts



    $es_prende_bombillo_effects_css = {
      'transition-property': 'display',
      '-moz-transition-duration': '0.5s',
      '-o-transition-duration': '0.5s',
      '-webkit-transition-duration': '0.5s',
      'transition-duration': '0.5s',
      '-moz-transition-timing-function': 'ease-in',
      '-o-transition-timing-function': 'ease-in',
      '-webkit-transition-timing-function': 'ease-in',
      'transition-timing-function': 'ease-in'
    };


    /*$es_prende_bombillo_consulta_deuda = function () {
      var $webServiceURL = 'http://10.240.8.15:8086/wsscp.asmx/ConsultarDeuda';
      $webServiceURL += '?idSucursal=TXOFTNNASASIUBTWHLYT';
      $webServiceURL += '&idCaja=1';
      $webServiceURL += '&NIC=6152206';

      // console.log($webServiceURL);

      $.ajax({
        url: $webServiceURL,
        type: "GET",
        dataType: "xml",
        // data: xmlDocument,
        contentType: "text/xml; charset=\"utf-8\"",
        crossDomain: true,
        success: $es_prende_bombillo_consulta_deuda_exitosa,
        error: $es_prende_bombillo_consulta_deuda_error
      });

    return false;
    };
    // $es_prende_bombillo_consulta_deuda = function () {
    //   var $webServiceURL = 'http://10.240.8.15:8086/wsscp.asmx/ConsultarDeuda';
    //   $webServiceURL += '?idSucursal=TXOFTNNASASIUBTWHLYT';
    //   $webServiceURL += '&idCaja=1';
    //   $webServiceURL += '&NIC=6152206';

    //   // console.log($webServiceURL);

    //   $.ajax({
    //     url: $webServiceURL,
    //     type: "GET",
    //     dataType: "xml",
    //     data: xmlDocument,
    //     contentType: "text/xml; charset=\"utf-8\"",
    //     crossDomain: true,
    //     success: $es_prende_bombillo_consulta_deuda_exitosa,
    //     error: $es_prende_bombillo_consulta_deuda_error
    //   });

    // return false;
    // };

    // $es_prende_bombillo_consulta_deuda_exitosa = function (data) {
    //   console.log(data);
    // };

    // $es_prende_bombillo_consulta_deuda_error = function () {
    //   // body...
    // }


    $es_prende_bombillo_consulta_deuda();*/
    // $es_prende_bombillo_consulta_deuda();

  })();
  // Mercadeo - Prende el bombillo

  // Footer Google Map
  var es_footer_map;
  var esLatLang = new google.maps.LatLng( 18.479330, -69.927836 );
  function initialize() {
    var esMapOptions = {
        zoom: 17,
        center: esLatLang,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false
    };

    es_footer_map = new google.maps.Map(document.getElementById('es-site-footer-map-canvas'),
    esMapOptions);
    var es_footer_marker = new google.maps.Marker({
        position: esLatLang,
        map: es_footer_map,
        title:"Edesur Dominicana"
    });
  };
  google.maps.event.addDomListener(window, 'load', initialize);
  google.maps.event.addDomListener(window, 'resize', function() {
    es_footer_map.setCenter(esLatLang);
  });

  function activateEmail ( selector ) { // define
    $( selector ).html( function() { // set text of selected element after...
      var adr = $( this ).data( 'address' ),
        dom = $( this ).data( 'domain' ),
        ext = $( this ).data( 'ext' );
      return adr + '@' + dom + ext; // ... constructing email address
    }).attr( 'href', function() { // make it clickable
      return 'mailto:' + $( this ).html();
    })/*.attr( 'target', '_blank' )*/; // open in a new tab
  }

  activateEmail($( '.email' )); // run it

});