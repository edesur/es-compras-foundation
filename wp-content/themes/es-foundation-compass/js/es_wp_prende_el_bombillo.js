jQuery(document).ready(function($) {

  var $S = Foundation.utils.S;

  var ConsultaNIC = {
  init: function(config) {
    console.log('Initialized!');

    this.config = config;
    this.bindEvents();
  },
  bindEvents: function () {
    console.log('Events Fired!');

    var $self = ConsultaNIC;
    this.config.es_prende_bombillo_int_count.on('change', function() {

      $es_int_count = $S(this).val();
      console.log($S(this).val());

      if ($es_int_count == 3) {
        console.log('Son ' + $es_int_count + ' Integrantes!');
      };
    });

    var $self = ConsultaNIC;
    this.config.es_prende_bombillo_nic_lider.on('blur', function() {
      var $es_nic_lider = $S(this).val();

      $self.consulta();

      // console.log('NIC Lider: ' + $es_nic_lider);
    });

    this.config.es_prende_bombillo_nic_int_1.on('blur', function() {
      var $es_nic_int_1 = $S(this).val();

      console.log('NIC Integrante 1: ' + $es_nic_int_1);
    });

    this.config.es_prende_bombillo_nic_int_2.on('blur', function() {
      var $es_nic_int_2 = $S(this).val();

      console.log('NIC Integrante 2: ' + $es_nic_int_2);
    });

  },
  consulta : function () {
    var $self         = ConsultaNIC,
        $es_nic_lider = $S('#ninja_forms_field_58').val(),
        $es_nic_int_1 = $S('#ninja_forms_field_60').val(),
        $es_nic_int_2 = $S('#ninja_forms_field_62').val();

    $self.getDeuda();
  },
  getDeuda: function () {
    console.log('Consulta deuda');
    var $self = ConsultaNIC,
    $results = {};
    $.ajax({
      url: '../wp-content/themes/es-foundation-compass/includes/es-mercadeo/es-prende-el-bombillo-consulta.php',
      cache: false,
      type: 'POST',
      dataType: 'json',
      data: this.config.es_prende_bombillo_form.serialize(),
      complete: function () {
        console.log('Consulta completa');
        // console.log(results.CodigoRespuesta);
      },
      success: function (results) {
        // console.log($('#ninja_forms_field_58').parent('.field-wrap'));

        function toTitleCase(str){
          return str.replace(/\w+/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        if (results.DeudaVencida !== null) {

          console.log('Consulta Existosa');
          console.log(results);
          console.log(parseFloat(results.DeudaVencida));
          console.log(toTitleCase(results.NombreTitular));

        };

        if (results.NombreTitular !== null) {

          $('#ninja_forms_field_58').parent('.field-wrap').removeClass('ninja-forms-error');
          $('#ninja_forms_field_58').next('.ninja-forms-field-error').html('Por favor digite un NIC valido').css('display', 'none');

          $('#ninja_forms_field_59').val(toTitleCase(results.NombreTitular));
          $('#ninja_forms_field_59').parent('.field-wrap').removeClass('ninja-forms-error');
          $('#ninja_forms_field_59').next('.ninja-forms-field-error').css('display', 'none');


        } else {
          $('#ninja_forms_field_59').val(toTitleCase(''));
          $('#ninja_forms_field_58').parent('.field-wrap').addClass('ninja-forms-error');
          $('#ninja_forms_field_58').next('.ninja-forms-field-error').html('Por favor digite un NIC valido').css('display', 'block');
        };



      },
      fail: function () {
        console.log('Consulta Fracasada');
      }
    });
  }


  }

  ConsultaNIC.init({
    es_prende_bombillo_form:      $('#ninja_forms_form_20'),
    es_prende_bombillo_int_count: $S('#ninja_forms_field_56'),
    es_prende_bombillo_nic_lider: $S('#ninja_forms_field_58'),
    es_prende_bombillo_nic_int_1: $S('#ninja_forms_field_60'),
    es_prende_bombillo_nic_int_2: $S('#ninja_forms_field_62')
  });

  // console.log('Prende el Bombillo')

});