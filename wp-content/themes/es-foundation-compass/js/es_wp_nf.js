jQuery(document).ready(function($) {

  var $S = Foundation.utils.S;

  // Comercial | Nuevo Servicio
  var $es_site_comercial_func = (function () {

    // NinjaForms Scripts

    $S('.input-counter').css('display', 'none');

    var $es_site_comercial_ninja_form_content = $S('.es-site-comercial-nuevo-servicio').find('.ninja-forms-cont'),
        $es_site_comercial_ninja_form = $es_site_comercial_ninja_form_content.find('.ninja-forms-form'),
        $es_site_comercial_ninja_form_progress = $es_site_comercial_ninja_form_content.find('.meter.nostripes.progressbar'),
        $es_site_comercial_ninja_form_meter = $es_site_comercial_ninja_form_progress.find('span'),
        $es_site_comercial_ninja_form_breadcrumbs = $es_site_comercial_ninja_form_content.find('.ninja-forms-mp-breadcrumbs'),
        $es_site_comercial_ninja_form_nav = $es_site_comercial_ninja_form_content.find('.ninja-forms-mp-nav'),
        $es_site_comercial_ninja_forms_error = $es_site_comercial_ninja_form.find('.ninja-forms-error'),
        $es_site_comercial_ninja_forms_field_error = $es_site_comercial_ninja_forms_error.find('.ninja-forms-field-error');

    // $es_site_comercial_ninja_form.attr({
    //   'data-abide': '',
    //   'novalidate': 'novalidate'
    // });
    // $S('.ninja-forms-req').attr('required', '');
    $es_site_comercial_ninja_form_progress.addClass('progress round').removeClass('meter nostripes progressbar');
    $es_site_comercial_ninja_form_meter.addClass('meter');
    $es_site_comercial_ninja_form_breadcrumbs.addClass('button-group radius stack-for-small even-4');
    $es_site_comercial_ninja_form_nav.addClass('button small info');

    $es_site_comercial_ninja_form.find('.es-site-comercial-nuevo-servicio-tipo').each(function(index, el) {

      $S(el).on('change', function(event) {
      event.preventDefault();

      var $servicioTipo = $S(this).val();

      switch ($servicioTipo) {
        case "residencial":
          $S('#es_site_comercial_nuevo_servicio_req_residencial').foundation('reveal', 'open');
          break;
        case "comercial":
          $S('#es_site_comercial_nuevo_servicio_req_comercial').foundation('reveal', 'open');
          break;
        case "area-comun":
          $S('#es_site_comercial_nuevo_servicio_req_area_comun').foundation('reveal', 'open');
          break;

        default:
      }


    });
      // console.log(el);
    });

    // console.log($es_site_comercial_ninja_form.find('.es-site-comercial-nuevo-servicio-tipo').val());
    // console.log($es_site_comercial_ninja_forms_error);
    if ($es_site_comercial_ninja_forms_error.length) {
      console.log($es_site_comercial_ninja_forms_field_error);
      console.log( 'Error Test');
    };

    // NinjaForms Scripts



    $es_comercial_effects_css = {
      'transition-property': 'display',
      '-moz-transition-duration': '0.5s',
      '-o-transition-duration': '0.5s',
      '-webkit-transition-duration': '0.5s',
      'transition-duration': '0.5s',
      '-moz-transition-timing-function': 'ease-in',
      '-o-transition-timing-function': 'ease-in',
      '-webkit-transition-timing-function': 'ease-in',
      'transition-timing-function': 'ease-in'
    };

  })();
  // Comercial | Nuevo Servicio

  $.ajax({
    type: 'get',
    url: '../wp-content/themes/es-foundation-compass/includes/es-comercial/es-comercial-nuevo-servicio-consulta.php',
    dataType: 'html',
    // data: data,
    success: function (data) {
      // console.log(da_ta);
    },
  });

});