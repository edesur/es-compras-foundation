jQuery(document).ready(function($) {
  // Foundation JavaScript
  // Documentation can be found at: http://foundation.zurb.com/docs
  $(document).foundation({
    topbar : {
      // sticky_class : 'sticky',
      // custom_back_text: true, // Set this to false and it will pull the top level link name as the back text
      back_text: 'Regresar' //,  Define what you want your custom back text to be if custom_back_text: true
      // is_hover: true,
      // mobile_show_parent_link: false, // will copy parent links into dropdowns for mobile navigation
      // scrolltop : true // jump to top when sticky nav menu toggle is clicked
    },
    "magellan-expedition": {
      threshold: 100, // how many pixels until the magellan bar sticks, 0 = auto
      fixed_top: 100, // top distance in pixels assigend to the fixed element on scroll

    }
  });
});