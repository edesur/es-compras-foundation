<section class="es-site-noticias small-12 medium-12 large-12 columns">

      <div class="row">
        <div class="es-site-noticias-articulos es-site-noticias-wrapper">
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-acuerdo-neiba.jpg" alt="Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand"></a>
              <h3><a href="#">EDESUR y la Coalición Neiba firman un acuerdo para mejorar el servicio</a></h3>
              <p>Edesur Dominicana anunció un aumento de la cantidad de horas diarias de electricidad que ofrece a sus clientes y usuarios de Neiba.</p>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-utiles-deportivos-sur.jpg" alt="Edesur ofrece orientaciones a clientes sobre áreas técnicas"></a>
              <h3><a href="#">EDESUR obsequia útiles deportivos a clubes del Sur</a></h3>
              <p>El administrador gerente general de Edesur Dominicana,  ingeniero Rubén Montás, inició en San Cristóbal y Haina la entrega de cientos artículos.</p>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-rehab-redes-dn.jpg" alt=""></a>
              <h3><a href="#">Edesur invertirá 121 millones en Rehabilitación de Redes en el Distrito Nacional</a></h3>
              <p>Las brigadas de técnicos de Edesur Dominicana iniciaron los trabajos de rehabilitación del circuito MATA101, que abarca los sectores San José, San Juan y Enriquillo</p>
            </div>
          </article>

          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-reduce-perdidas.jpg" alt="Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand"></a>
              <h3><a href="#">Edesur reduce pérdidas en Los Alcarrizos y Pedro Brand</a></h3>
              <p>Edesur Dominicana redujo en 53 y 48% las pérdidas por facturación y cobro como resultado preliminar del proyecto de rehabilitación de redes.</p>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-clientes-areas-tecnicas.jpg" alt="Edesur ofrece orientaciones a clientes sobre áreas técnicas"></a>
              <h3><a href="#">Edesur ofrece orientaciones a clientes sobre áreas técnicas</a></h3>
              <p>Edesur Dominicana ofreció a un grupo de clientes las explicaciones técnicas del funcionamiento de sus áreas administrativas.</p>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-trabajos-sd-oeste.jpg" alt=""></a>
              <h3><a href="#">Edesur realizará trabajos de mantenimiento en Santo Domingo Oeste</a></h3>
              <p>Serán afectados con la suspensión los Girasoles I, II y III, Palma Real, Los Peralejos, Los Militares, Proyecto Fairbanks, Los Ángeles y Panca Dominicana.</p>
            </div>
          </article>
        </div>
      </div>

      <div class="row">
        <div class="es-site-noticias-videos es-site-noticias-wrapper">
        <h2><a href="#">Videos de Edesur</a></h2>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-redes-independencia.jpg" alt=""></a>
              <h3><a href="#">Edesur rehabilita redes en barrios Avenida Independencia</a></h3>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-circuito-24.jpg" alt=""></a>
              <h3><a href="#">Edesur inaugura circuitos 24 horas</a></h3>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-engombe-uasd.jpg" alt=""></a>
              <h3><a href="#">Circuito 24 horas,Engombe UASD</a></h3>
            </div>
          </article>

          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-los-coroneles.jpg" alt=""></a>
              <h3><a href="#">Circuito 24 Horas, Los Coroneles de Herrera</a></h3>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-desarrollo-gas.jpg" alt=""></a>
              <h3><a href="#">Desarrollo del Gas en República Dominicana</a></h3>
            </div>
          </article>
          <article class="">
            <div class="columns">
              <a href="#"><img src="images/es-site-noticias-video-marcelo-jorge.jpg" alt=""></a>
              <h3><a href="#">Premios Energia 2014 reconoce a Marcelo Jorge</a></h3>
            </div>
          </article>
        </div>
      </div>

    </section>