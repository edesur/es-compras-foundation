<section class="es-site-section-wrapper small-12 columns" data-equalizer>

 <div class="row small-collapse medium-uncollapse">
   <div class="small-12 medium-6 columns">
     <div class="row medium-collapse">
       <div class="small-12 columns">
         <h3 class="es-site-section-wrapper-bg-title">Misión</h3>
       </div>
     </div>
     <div class="es-site-section-wrapper-background small-12 columns" data-equalizer-watch>
       <p>Somos la empresa de servicio que distribuye y comercializa energía eléctrica en nuestra área de concesión, comprometida con la calidad, mejorando continuamente los procesos con capital humano integro, calificado y tecnología de vanguardia, garantizando la rentabilidad financiera, la satisfacción de nuestros clientes y contribuyendo al desarrollo sostenible del país.</p>
     </div>
   </div>

   <div class="small-12 medium-6 columns">
     <div class="row medium-collapse">
       <div class="small-12 columns">
         <h3 class="es-site-section-wrapper-bg-title">Visión</h3>
       </div>
     </div>
     <div class="es-site-section-wrapper-background small-12 columns" data-equalizer-watch>
       <p>Ser líder en calidad de servicio de distribución y comercialización de energía eléctrica, convertida en una empresa rentable y eficiente, sirviendo de modelo en República Dominicana y el Caribe.</p>
     </div>
   </div>
 </div>

 <div class="row small-collapse medium-uncollapse">
   <div class="small-12 columns">
     <div class="row medium-collapse">
       <div class="small-12 columns">
         <h3 class="es-site-section-wrapper-bg-title">Valores</h3>
       </div>
     </div>

      <div class="es-site-section-wrapper-background small-12 columns">
        <div class="row small-collapse medium-uncollapse">

          <div class="small-12 medium-6 columns">
            <h4>Calidad</h4>
            <p>Trabajamos para que nuestros servicios cumplan con las características y los  requisitos establecidos en la normativa vigente.</p>
           <h4>Servicio al Cliente</h4>
           <p>Buscamos permanentemente satisfacer las necesidades y deseos de nuestros clientes.  Nos esforzamos por ofrecer un servicio de alta calidad.</p>
           <h4>Compromiso</h4>
           <p>Mantenemos la convicción del cumplimiento cabal de los objetivos planteados en función de nuestros clientes y el medio ambiente.</p>
           <h4>Trabajo en equipo</h4>
           <p>Participamos en grupo activamente en la consecución de metas comunes, manteniendo una alta disposición en el desarrollo de los objetivos.</p>
          </div>
          <div class="small-12 medium-6 columns">
           <h4>Orientación al Cambio</h4>
           <p>Nos esforzamos cada día en buscar mejoras que optimicen los procesos y tareas en todas las áreas de la organización, adaptándonos a los nuevos requerimientos de nuestros clientes.</p>
           <h4>Integridad</h4>
           <p>Nuestra Honradez y  rectitud en nuestra conducta, contribuirá al aumento de la confianza de la sociedad respecto a la empresa.</p>
           <h4>Entusiasmo</h4>
           <p>Hacemos las cosas con alegría y pasión, nos gusta participar, apoyar y compartir con el equipo, para lograr los resultados.</p>
           <h4>Seguridad</h4>
           <p>Asumimos el compromiso de velar por la prevención, protección y eliminación de los riesgos derivados de la actividad que puedan afectar a las personas, al patrimonio, así como de las leyes y reglamentaciones asociadas a la materia.</p>
          </div>
        </div>
      </div>
   </div>
 </div>
</section>