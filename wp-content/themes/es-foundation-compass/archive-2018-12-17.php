<?php

  /**
 * The template for displaying all single pages.
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  get_header();

  get_template_part( '/includes/masthead-page' );

 ?>

<?php



  switch ( $post_type ) {

    case 'proceso_compra':
      get_template_part( 'content', 'compras-proceso-compra' );
      break;

    default:
      # code...
      break;
  }


?>



<?php get_footer(); ?>