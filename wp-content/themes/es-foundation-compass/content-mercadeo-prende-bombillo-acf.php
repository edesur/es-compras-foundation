<div class="es-site-mercadeo-prende-bombillo row">

  <div class="small-12 medium-3 columns">
    <img class="th" src="<?php echo site_url(); ?>/wp-content/uploads/2015/04/es-site-mercadeo-prende-bombillo-logo.jpg" alt="">
    <p>Inscríbete, crea y gana hasta RD$500,000. Demuestra tu talento y creatividad desarrollando una campaña para Edesur!</p>

  </div>
  <div class="small-12 medium-9 columns">
    <h3 class="text-center">Formulario de inscripción</h3>

    <?php

      acf_form(array(
        'post_id'   => 'new_post',
        'field_groups'  => array( 818 ),
        'form_attributes' => array( 'class'=> 'es-site-mercadeo-prende-bombillo-form ',
          'enctype' => 'multipart/form-data'),
        'submit_value' => 'Enviar inscripción'
      ));

    ?>

  </div>
</div>