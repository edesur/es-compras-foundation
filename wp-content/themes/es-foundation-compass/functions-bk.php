<?php
  /**
   * This is the theme's functions.php file.
   * This file loads the theme's constants.
   * Please be cautious when editing this file, errors here cause big problems.
   *
   *
   * @package WordPress
   * @subpackage ES_Foundation_Compass
   * @author Edesur_Dominicana
   * @since ES_Foundation_Compass 1.0
   *
   *
   */

    @ini_set( 'upload_max_size' , '128M' );
    @ini_set( 'post_max_size', '128M');
    @ini_set( 'max_execution_time', '300' );

  if (! function_exists( 'es_foundation_compass_setup' )) {

    function es_foundation_compass_setup() {
      // Enable thumbail support
      add_theme_support( 'post-thumbnails' );

      add_theme_support( 'html5', array( 'search-form' ) );

      /**
       * Attach a class to linked images' parent anchors
       * Works for existing content
       */
      function give_linked_images_class($content) {

        $classes = 'th'; // separate classes by spaces - 'img image-link'

        // check if there are already a class property assigned to the anchor
        if ( preg_match('/<a.*? class=".*?"><img/', $content) ) {
          // If there is, simply add the class
          $content = preg_replace('/(<a.*? class=".*?)(".*?><img)/', '$1 ' . $classes . '$2', $content);
        } else {
          // If there is not an existing class, create a class property
          $content = preg_replace('/(<a.*?)><img/', '$1 class="' . $classes . '" ><img', $content);
        }
        return $content;
      }

      add_filter('the_content','give_linked_images_class');

       // Load ESD Foundation Compass WP Theme CSS styles
       function es_foundation_compass_wp_css() {
          if (!is_admin()) {
            wp_enqueue_style( 'main', get_template_directory_uri() . '/stylesheets/app.css' );
          }
       }

       add_action( 'wp_enqueue_scripts', 'es_foundation_compass_wp_css' );

       function es_admin_styles() {

          if (!is_admin()) {

            wp_deregister_style( 'wp-admin' );
          }
       }

       add_action( 'wp_print_styles', 'es_admin_styles', 100 );

       // Load Edesur Foundation Compass WP Theme JS scripts
       function es_foundation_compass_wp_js() {

        if (!is_admin()) {

          wp_register_script( 'foundation_modernizr', get_template_directory_uri() . '/bower_components/modernizr/modernizr.js',  false );
          wp_enqueue_script('foundation_modernizr' );

          wp_register_script( 'es_google_maps_js', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', false );
          wp_enqueue_script('es_google_maps_js' );

          // Deregister WordPress jQuery
          wp_deregister_script( 'jquery' );

          // Register Foundation jQuery
          wp_register_script( 'jquery', get_template_directory_uri() . '/bower_components/jquery/dist/jquery.min.js', false, '', true );
          wp_enqueue_script( 'jquery' );

          wp_register_script( 'foundation_js', get_template_directory_uri() . '/bower_components/foundation/js/foundation.min.js', array('jquery'), '', true );
          wp_enqueue_script('foundation_js' );

          wp_register_script( 'foundation_app_js', get_template_directory_uri() . '/js/app.js', array('jquery'), '', true );
          wp_enqueue_script('foundation_app_js' );

          wp_register_script( 'slick_js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '', true );
          wp_enqueue_script('slick_js' );

          wp_register_script( 'es_addthis_widget_js', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53e919531b032162' );
          wp_enqueue_script('es_addthis_widget_js', '', true  );

          wp_register_script( 'es_scripts_js', get_template_directory_uri() . '/js/es.js', array('jquery'), '', true );
          wp_enqueue_script('es_scripts_js' );

          wp_register_script( 'es_fndt_scripts_js', get_template_directory_uri() . '/js/es_fndt.js', array('jquery'), '', true );
          wp_enqueue_script('es_fndt_scripts_js' );
        }
      }

      add_action( 'wp_enqueue_scripts', 'es_foundation_compass_wp_js', 10 );

      function es_wp_plugins_load_home_ads() {

        wp_register_script( 'es_wp_plugins_home_ads_scripts', get_template_directory_uri() . '/js/es_wp_home_ads.js',  array('jquery'), '', true );
        if (is_front_page()) {
          wp_enqueue_script( 'es_wp_plugins_home_ads_scripts' );
        }

      }
        // add_action( 'wp_enqueue_scripts', 'es_wp_plugins_load_home_ads' );

      function es_wp_masthead_home_xlarge_styles() {

        wp_register_script( 'es_wp_masthead_home_xlarge_css', get_template_directory_uri() . '/stylesheets/masthead.css',  array('jquery'), '', true );
        if (is_front_page()) {
          wp_enqueue_script( 'es_wp_masthead_home_xlarge_css' );
        }

      }
        // add_action( 'wp_enqueue_scripts', 'es_wp_masthead_home_xlarge_styles' );

      function my_relationship_query( $args, $field, $post ) {
          // increase the posts per page
          $args['posts_per_page'] = 25;

          return $args;
      }

      // filter for a specific field based on it's key
      // add_filter('acf/fields/relationship/query/key=field_5486f7fa0b1a1', 'my_relationship_query', 10, 3);


      // Change the Worpress logo to Edesur Dominicana logo
      function es_site_login_styles() {
        wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/stylesheets/login-styles.css' );
        ?>
          <style type="text/css">
              /* body.login div#login h1 a {
                background-image: url("<?php echo get_template_directory_uri() ?>/stylesheets/images/es-site-logo-blue.png");
                padding-bottom: 30px;
                width: 194px;
                background-size: 100%;
              } */
          </style>
      <?php }

      add_action( 'login_enqueue_scripts', 'es_site_login_styles' );


      function es_login_logo_url() {
          return home_url();
      }
      add_filter( 'login_headerurl', 'es_login_logo_url' );

      function es_login_logo_url_title() {
          return get_option('blogname');
      }
      add_filter( 'login_headertitle', 'es_login_logo_url_title' );

      /**REPLACE WP LOGO**/
      function es_custom_admin_logo() {
      echo '
        <style type="text/css">
          #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
            content: url(' . get_template_directory_uri() . '/stylesheets/images/es-site-logo-orange-admin.png) !important;
            top: -2px;
          }

          #wpadminbar #wp-admin-bar-wp-logo > a.ab-item {
              pointer-events: none;
              cursor: default;
          }
        </style>
      ';
      }

      add_action('wp_before_admin_bar_render','es_custom_admin_logo', 0);
      /**END REPLACE WP LOGO**/


      function remove_admin_bar_links() {
      global $wp_admin_bar;
      // $wp_admin_bar->remove_menu('new-content');      // Remove the 'add new' button
      $wp_admin_bar->remove_menu('comments');         // Remove the comments bubble
      $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
      $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
      $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
      $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
      $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
      }
      add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );


      function es_footer_admin () {
          echo '<a href="http://edesur.com.do" target="_blank">Edesur Dominicana</a>';
          }

      add_filter('admin_footer_text', 'es_footer_admin');


      function es_change_howdy($translated, $text, $domain) {

          if (!is_admin() || 'default' != $domain)
              return $translated;

          if (false !== strpos($translated, 'Howdy'))
              return str_replace('Howdy', 'Hola', $translated);

          return $translated;
      }

      add_filter('    ', 'es_change_howdy', 10, 3);


      // Change the "enter title here" text depending on the post type
      function es_enter_title_here( $message ){
        global $post_type;

        switch ($post_type) {

          case 'proceso_compra':
            $message = 'Descripción corta del proceso de compra';
            break;

          case 'suplidor':
            $message = 'Nombre de la Empresa';
            break;

          case 'ficha_tecnica':
            $message = 'Titulo de la Ficha Técnica';
            break;

          case 'evento':
            $message = 'Titulo del Evento';
            break;

          # default:
            # code...
            # break;
        }

        return $message;
      }
      add_filter( 'enter_title_here', 'es_enter_title_here' );

      function es_add_class_attachment_link($html){
          $postid = get_the_ID();
          if ($post->post_type == 'compra_proceso') {
            $html = str_replace('<a','<a class="th"',$html);
            return $html;
          }

      }

      function es_add_proceso_compra_columns($columns) {
          unset($columns['author']);
          unset($columns['date']);
          return array_merge($columns,
                    array('es_compras_proceso_compra_id' => __('Código'),
                          'es_compras_proceso_compra_type_tax' => __('Modalidad'),
                          'es_compras_proceso_compra_status_tax' => __('Estado'),
                          'es_compras_proceso_compra_date_published' => __('Publicado')
                          ));

          return $columns;
      }
      add_filter('manage_proceso_compra_posts_columns' , 'es_add_proceso_compra_columns');



      function es_proceso_compra_custom_column( $column, $post_id ) {
          switch ( $column ) {

            case 'es_compras_proceso_compra_id' :
              echo get_post_meta( $post_id , 'es_compras_proceso_compra_id' , true );
              break;

            case 'es_compras_proceso_compra_type_tax' :

              $es_proceso_compra_type_terms = get_the_terms( $post_id , 'es_compras_proceso_type_tax');

              foreach ($es_proceso_compra_type_terms as $term) {
                echo $term->name;
              }
              break;

            case 'es_compras_proceso_compra_status_tax' :

              $es_proceso_compra_status_terms = get_the_terms( $post_id , 'es_compras_proceso_status_tax');

              foreach ($es_proceso_compra_status_terms as $term) {
                echo $term->name;
              }
              break;

            case 'es_compras_proceso_compra_date_published' :

              $es_proceso_compra_date_published = DateTime::createFromFormat('Ymd', get_field('es_compras_proceso_compra_date_published'));

                echo $es_proceso_compra_date_published->format('d/m/Y');
              break;

              // $es_proceso_compra_date_published = DateTime::createFromFormat('Ymd', get_field('es_compras_proceso_compra_date_published'));


          }
      }

      add_action( 'manage_proceso_compra_posts_custom_column' , 'es_proceso_compra_custom_column', 10, 2 );


      add_filter('posts_join', 'es_compras_proceso_search_join' );
      function es_compras_proceso_search_join ($join){
          global $pagenow, $wpdb;
          // I want the filter only when performing a search on edit page of Custom Post Type named "es_compras_proceso"
          if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='proceso_compra' && $_GET['s'] != '') {
              $join .='LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
          }
          return $join;
      }

      add_filter( 'posts_where', 'es_compras_proceso_search_where' );
      function es_compras_proceso_search_where( $where ){
          global $pagenow, $wpdb;
          // I want the filter only when performing a search on edit page of Custom Post Type named "es_compras_proceso"
          if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='proceso_compra' && $_GET['s'] != '') {
              $where = preg_replace(
             "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
             "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
          }
          return $where;
      }


      // Ninja forms functions

      function es_site_forms_add_custom_form_class( $form_class, $form_id ) {

        // Footer contact form custom class
        if ( $form_id == 6 ) {
          $form_class .= ' es-site-footer-contact-form';
        }

        // Masthead fraud form custom class
        if ( $form_id == 8 ) {
          $form_class .= ' es-site-masthead-form-fraude';
        }

        // Prende el bombillo form custom class
        if ( $form_id == 19 ) {
          $form_class .= ' es-site-mercadeo-prende-bombillo-form';
        }

        return $form_class;
      }
      add_filter( 'ninja_forms_form_class', 'es_site_forms_add_custom_form_class', 10, 2 );

      function es_site_forms_add_custom_field_class( $field_wrap_class, $field_id ) {

        // Footer contact form textarea custom class
        if ( $field_id == 14 ) {
          $field_wrap_class .= ' es-site-footer-contact-textarea-box';
        }

        // Masthead fraud form textarea custom class
        return $field_wrap_class;
      }
      add_filter( 'ninja_forms_field_wrap_class', 'es_site_forms_add_custom_field_class', 10, 2 );

      function es_site_forms_wrap_before_field( $field_id, $data ){
        // Code to run before each field is rendered. Of if checking against the $field_id, before a pecific field.

        // Footer contact form <div>s
        if ($field_id == 10 || $field_id == 11) {
          echo '<div class="row small-collapse large-uncollapse">
            <div class="es-site-contact-field large-6 columns">';
        }

        if ($field_id == 14) {
          echo '<div class="row small-collapse large-uncollapse">
            <div class="es-site-contact-field es-site-contact-textarea-box large-12 columns">';
        }

        // Masthead fraud form <div>s
        if ($field_id == 16) {
          echo '<div class="row">
                 <fieldset class="small-12 medium-6 columns">
                   <div class="row">
                     <div class="small-12 columns">';
        }

        if ($field_id == 26) {
          echo '<div class="row">
                 <fieldset class="small-12 columns">
                   <div class="row">
                     <div class="small-12 columns">';
        }

        if ($field_id == 17 || $field_id == 18 || $field_id == 22 || $field_id == 20 || $field_id == 21) {
          echo '<div class="row">
                    <div class="small-12 columns">';
        }

        if ($field_id == 19) {
          echo '<fieldset class="small-12 medium-6 columns end">
                 <div class="row">
                   <div class="small-12 columns">';
        }

        // Masthead suppliers form <div>s
        if ($field_id == 29) {
          echo '<div class="row">
             <fieldset class="small-12 medium-4 columns">
               <legend>Datos personales</legend>

               <div class="row">
                 <div class="small-12 medium-11 columns">';
        }

        if ($field_id == 34) {
          echo '<fieldset class="small-12 medium-4 columns end">
               <legend>Datos de la Empresa</legend>

               <div class="row">
                 <div class="small-12 medium-11 columns">';
        }

        if ($field_id == 39) {
          echo '<fieldset class="small-12 medium-4 columns">
               <legend class="show-for-medium-up">Enviar Solicitud</legend>

               <div class="row">
                 <div class="small-12 medium-11 columns">
                   <p>Por favor, complete esta solicitud para recibir el formulario de registro & homologación de proveedores por correo electrónico.</p>';
        }

        if ($field_id == 30 || $field_id == 31 || $field_id == 32 || $field_id == 41 || $field_id == 40 || $field_id == 38) {
          echo '<div class="row">
                 <div class="small-12 medium-11 columns">';
        }


        // Prende el bombillo form <div>s
        if ($field_id == 43) {
          echo '<div class="row">
                 <fieldset class="small-12 medium-6 columns">
                   <div class="row">
                     <div class="small-12 columns">';
        }

        if ($field_id == 49) {
          echo '<fieldset class="small-12 medium-6 columns">
                   <div class="row">
                     <div class="small-12 columns">';
        }

        if ($field_id == 45 || $field_id == 47 || $field_id == 48 || $field_id == 50 || $field_id == 51 || $field_id == 52) {
          echo '<div class="row">
                    <div class="small-12 columns">';
        }
      }
      add_action( 'ninja_forms_display_before_field', 'es_site_forms_wrap_before_field', 10, 2 );

      function es_site_forms_wrap_after_field( $field_id, $data ) {

        // Footer contact form <div>s
        if ($field_id == 10 || $field_id == 11) {
          echo '</div>
            <div class="es-site-contact-field large-6 columns">';
        }

        if ($field_id == 12 || $field_id == 13 || $field_id == 15) {
          echo '</div>
          </div>';
        }

        // Masthead fraud form <div>s
        if ($field_id == 16 || $field_id == 17 || $field_id == 18 || $field_id == 19 || $field_id == 20 || $field_id == 21 || $field_id == 26) {
          echo '</div>
             </div>';
        }

        if ($field_id == 22) {
          echo '</div>
             </div>

           </fieldset>';
        }

        if ($field_id == 21) {
          echo '</div>
             </div>

            </fieldset>
           </div>';
        }

        if ($field_id == 25) {
          echo '</fieldset>
           </div>';
        }

        // Masthead suppliers form <div>s
        if ($field_id == 29 || $field_id == 30 || $field_id == 31 || $field_id == 34 || $field_id == 41 || $field_id == 40) {
          echo '</div>
          </div>';
        }

        if ($field_id == 32 || $field_id == 38) {
          echo '</div>
               </div>

             </fieldset>';
        }

        if ($field_id == 39) {
          echo ' </div>
               </div>

             </fieldset>

           </div>';
        }

        // Prende el bombillo form <div>s
        if ($field_id == 43 || $field_id == 45 || $field_id == 47 || $field_id == 49 || $field_id == 50 || $field_id == 51) {
          echo '</div>
             </div>';
        }

        if ($field_id == 48) {
          echo '</div>
             </div>

           </fieldset>';
        }

        if ($field_id == 52) {
          echo ' </div>
               </div>

             </fieldset>

           </div>';
        }


      }
      add_action( 'ninja_forms_display_after_field', 'es_site_forms_wrap_after_field', 10, 2 );

      function es_nf_subs_capabilities( $cap ) {
        $current_user = wp_get_current_user();

        if (in_array( 'compras', $current_user->roles) || in_array( 'form_fraude', $current_user->roles) || in_array( 'form_contacto', $current_user->roles) || in_array( 'administrator', $current_user->roles)) {
          $cap =  'edit_posts';
          return $cap;
        }

      }
      add_filter( 'ninja_forms_admin_submissions_capabilities', 'es_nf_subs_capabilities' );
      add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'es_nf_subs_capabilities' );

      $es_url_protocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';

      $es_admin_current_url = $es_url_protocol . '://'. $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
      // $es_nf_admin_url = get_site_url() . '/wp-admin/admin.php?page=ninja-forms';
      $es_nf_admin_url = admin_url('admin.php?page=ninja-forms');
      $es_nf_sub_url = admin_url('edit.php?post_type=nf_sub');
      $es_nf_sub_contacto_url = admin_url('edit.php?s&post_status=all&post_type=nf_sub&action=-1&m=0&form_id=6&begin_date&end_date&paged=1&mode=list&action2=-1');
      $es_nf_sub_fraud_url = admin_url('edit.php?s&post_status=all&post_type=nf_sub&action=-1&m=0&form_id=8&begin_date&end_date&paged=1&mode=list&action2=-1');
      $es_nf_sub_supplier_url = admin_url('edit.php?s&post_status=all&post_type=nf_sub&action=-1&m=0&form_id=10&begin_date&end_date&paged=1&mode=list&action2=-1');
      $current_user = wp_get_current_user();
      // var_dump($current_user->roles);
      // var_dump($_SERVER);
      // var_dump($es_admin_current_url);
      // var_dump($es_nf_sub_contacto_url);
      // var_dump($es_nf_admin_url);
      // if ($es_admin_current_url == $es_nf_admin_url && !in_array( 'administrator', $current_user->roles )) {
      //   wp_redirect(get_site_url() . '/wp-admin/edit.php?post_type=nf_sub');
      //   exit;
      // }


      if ((($es_admin_current_url == $es_nf_admin_url || $es_admin_current_url == $es_nf_sub_url || $es_admin_current_url == $es_nf_sub_fraud_url || $es_admin_current_url == $es_nf_sub_supplier_url) && !in_array( 'administrator', $current_user->roles )) && in_array( 'form_contacto', $current_user->roles )) {
        wp_redirect($es_nf_sub_contacto_url);
        exit;

      } elseif ((($es_admin_current_url == $es_nf_admin_url || $es_admin_current_url == $es_nf_sub_url || $es_admin_current_url == $es_nf_sub_contacto_url || $es_admin_current_url == $es_nf_sub_supplier_url  && !$es_nf_sub_fraud_url) && !in_array( 'administrator', $current_user->roles )) && in_array( 'form_fraude', $current_user->roles )) {
        wp_redirect($es_nf_sub_fraud_url);
        exit;

      }  elseif ((($es_admin_current_url == $es_nf_admin_url || $es_admin_current_url == $es_nf_sub_url || $es_admin_current_url == $es_nf_sub_contacto_url || $es_admin_current_url == $es_nf_sub_fraud_url) && !in_array( 'administrator', $current_user->roles )) && in_array( 'compras', $current_user->roles )) {
        wp_redirect($es_nf_sub_supplier_url);
        exit;
      }

      add_role( 'compras', __('Gerencia de compras'), array(

        'read' => true, // true allows this capability
        'edit_posts' => true, // Allows user to edit their own posts
        'edit_pages' => false, // Allows user to edit pages
        'edit_others_posts' => false, // Allows user to edit others posts not just their own
        'edit_published_posts' => true,
        'upload_files' => true,
        'delete_published_posts' => true,
        'delete_posts' => true,
        'create_posts' => true, // Allows user to create new posts
        'manage_categories' => false, // Allows user to manage post categories
        'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false // user cant perform core updates

      ));


      add_role( 'form_contacto', __('Formulario de contacto'), array(

        'read' => true, // true allows this capability
        'edit_posts' => true, // Allows user to edit their own posts
        'edit_pages' => false, // Allows user to edit pages
        'edit_others_posts' => false, // Allows user to edit others posts not just their own
        'edit_published_posts' => true,
        'upload_files' => true,
        'delete_published_posts' => true,
        'delete_posts' => true,
        'create_posts' => true, // Allows user to create new posts
        'manage_categories' => false, // Allows user to manage post categories
        'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false // user cant perform core updates

      ));

      add_role( 'form_fraude', __('Formulario de fraude'), array(

        'read' => true, // true allows this capability
        'edit_posts' => true, // Allows user to edit their own posts
        'edit_pages' => false, // Allows user to edit pages
        'edit_others_posts' => true, // Allows user to edit others posts not just their own
        'edit_published_posts' => true,
        'upload_files' => true,
        'delete_published_posts' => true,
        'delete_posts' => true,
        'create_posts' => true, // Allows user to create new posts
        'manage_categories' => false, // Allows user to manage post categories
        'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false // user cant perform core updates

      ));

      // $result = remove_role('compras');
      // $result = remove_role('form_fraude');
      // $result = remove_role('form_contacto');
      // $result = remove_role('test_role');



      function es_role_remove_menus(){

        $current_user = wp_get_current_user();

        if (in_array( 'compras', $current_user->roles )) {
          remove_menu_page( 'index.php' );                  //Dashboard
          remove_menu_page( 'edit.php' );                   //Posts
          remove_menu_page( 'upload.php' );                 //Media
          remove_menu_page( 'edit.php?post_type=page' );    //Pages
          remove_menu_page( 'edit-comments.php' );          //Comments
          remove_menu_page( 'themes.php' );                 //Appearance
          remove_menu_page( 'plugins.php' );                //Plugins
          remove_menu_page( 'users.php' );                  //Users
          remove_menu_page( 'tools.php' );                  //Tools
          remove_menu_page( 'options-general.php' );
          remove_menu_page( 'edit.php?post_type=interrupcion' );
          remove_menu_page( 'edit.php?post_type=nomina' );
          remove_menu_page( 'edit.php?post_type=pregunta' );
          remove_menu_page( 'edit.php?post_type=horario' );
        }

        if (in_array( 'form_contacto', $current_user->roles ) || in_array( 'form_fraude', $current_user->roles)) {
          remove_menu_page( 'index.php' );                  //Dashboard
          remove_menu_page( 'edit.php' );                   //Posts
          remove_menu_page( 'upload.php' );                 //Media
          remove_menu_page( 'edit.php?post_type=page' );    //Pages
          remove_menu_page( 'edit-comments.php' );          //Comments
          remove_menu_page( 'themes.php' );                 //Appearance
          remove_menu_page( 'plugins.php' );                //Plugins
          remove_menu_page( 'users.php' );                  //Users
          remove_menu_page( 'tools.php' );                  //Tools
          remove_menu_page( 'options-general.php' );
          remove_menu_page( 'edit.php?post_type=interrupcion' );
          remove_menu_page( 'edit.php?post_type=nomina' );
          remove_menu_page( 'edit.php?post_type=pregunta' );
          remove_menu_page( 'edit.php?post_type=proceso_compra' );
          remove_menu_page( 'edit.php?post_type=horario' );
        }

      }
      add_action( 'admin_menu', 'es_role_remove_menus' );

      function es_non_admin_login_redirect( $redirect_to, $request, $user  ) {

          if (is_array( $user->roles ) && in_array( 'compras', $user->roles )) {
            return admin_url('edit.php?post_type=proceso_compra');
          }
          elseif (is_array( $user->roles ) && in_array( 'form_contacto', $user->roles ) && !in_array( 'administrator', $user->roles )) {
            return admin_url('edit.php?s&post_status=all&post_type=nf_sub&action=-1&m=0&form_id=6&begin_date&end_date&paged=1&mode=list&action2=-1');
          }
          elseif (is_array( $user->roles ) && in_array( 'form_fraude', $user->roles )) {
            return admin_url('edit.php?s&post_status=all&post_type=nf_sub&action=-1&m=0&form_id=8&begin_date&end_date&paged=1&mode=list&action2=-1');
          } else {
            return admin_url();
          }

      }
      add_filter( 'login_redirect', 'es_non_admin_login_redirect', 10, 3 );


        // global $menu;
        // global $submenu;

        // var_dump($menu);

      function es_change_post_menu_label() {
          global $menu;
          global $submenu;
          $user = wp_get_current_user();

          // var_dump($user->roles);

          if (is_array( $user->roles ) && in_array( 'compras', $user->roles )) {
            $menu['35.1337']['0'] = "Formulario de proveedores";
          }
          elseif (is_array( $user->roles ) && in_array( 'form_fraude', $user->roles )) {
            $menu['35.1337']['0'] = "Denuncias de fraude";
          }
          elseif (is_array( $user->roles ) && in_array( 'form_contacto', $user->roles )) {
            $menu['35.1337']['0'] = "Formulario de contacto";
          }




          // var_dump(in_array( 'Formularios', $menu ));
          // $menu[5][0] = 'Contacts';
          // $submenu['edit.php'][5][0] = 'Contacts';
          // $submenu['edit.php'][10][0] = 'Add Contacts';
          // $submenu['edit.php'][15][0] = 'Status'; // Change name for categories
          // $submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
          // echo '';
      }

      add_action( 'admin_menu', 'es_change_post_menu_label' );

      // Create new "Prende el bombillo" team
      add_filter('acf/pre_save_post' , 'es_pre_save_prende_bombillo_team' );

      function es_pre_save_prende_bombillo_team( $post_id ) {

        // bail early if not a new post
        if( $post_id !== 'new_post' ) {

          return $post_id;

        }

        // vars
        $title = $_POST['fields']['field_5548d61a7acf5'];
        // $content =

        // Create a new post
        $post = array(
          'post_status' => 'draft',
          'post_type'   => 'bombillo_grupo',
          'post_title'  => $title,
        );


        // insert the post
        $post_id = wp_insert_post( $post );


        // return the new ID
        return $post_id;
      } // Create new "Prende el bombillo" team

      // Save "Prende el bombillo" team and send eamil

      // add_action('acf/save_post', 'es_save_prende_bombillo_team');

      function es_save_prende_bombillo_team( $post_id ) {

        // bail early if not a contact_form post
        if( get_post_type($post_id) !== 'bombillo_grupo' ) {

          return;
        }

        // vars
        $post = get_post( $post_id );

        // get custom fields (field group exists for content_form)
        $name = get_field('es_prende_bombillo_grupo_lider_nombre', $post_id);
        $email = get_field('es_prende_bombillo_grupo_lider_email', $post_id);


        // email data
        $to = 'dev@alloy.mx';
        $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
        $subject = $post->post_title;
        $body = $post->post_content;


        // send email
        wp_mail($to, $subject, $body, $headers );

      } // Save "Prende el bombillo" team and send eamil

    }
  }

  add_action( 'after_setup_theme', 'es_foundation_compass_setup' );


?>