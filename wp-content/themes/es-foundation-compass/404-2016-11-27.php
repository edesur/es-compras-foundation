<?php

  /**
 * The template for displaying all single pages.
 *
 *
 * @package WordPress
 * @subpackage ESD Foundation Compass
 * @author Edesur Dominicana
 * @since ESD Foundation Compass 1.0
 */

  get_header();

  get_template_part( '/includes/masthead-page' );

 ?>

<section class="es-site-section-search-content es-site-section-wrapper small-12 medium-12 large-12 columns">
  <div class="row">

    <!-- <div class="row collapse">
      <div class="small-12 columns">
        <h3><?php /*printf( __( 'Resultados de búsqueda: %s', 'es-foundation-compass' ), '<span>' . get_search_query() . '</span>' );*/ ?></h3>
      </div>
    </div> -->

    <div class="es-site-empresa-historia es-site-section-wrapper-background small-12 medium-12 columns">



      <?php if ( is_search() ) : ?>

        <p><?php _e( 'No se encontraron resultados en la búsqueda. Por favor intente de nuevo utilizando diferentes términos o palabras claves.
      ', 'es-foundation-compass' ); ?></p>
        <div class="medium-4 columns">
          <?php  get_search_form(); ?>
        </div>

      <?php else : ?>



        <p><?php _e( 'Al parecer no fue posible encontrar lo que busca, posiblemente utilizando diferentes términos o palabras claves tenga mejor resultado
      ', 'es-foundation-compass' ); ?></p>
        <div class="medium-4 columns">
          <?php  get_search_form(); ?>
        </div>

      <?php endif; ?>

    </div>
  </div>
</section>


<?php get_footer(); ?>