<div class="row">
 <div class="small-12 columns">
   <h3>Realizar Denuncia</h3>
 </div>
</div>

<div class="row">

 <div class="small-4 columns">
   <p>Su denuncia y las informaciones suministradas a nosotros para esta finalidad, son estrictamente confidenciales. Por lo que puede realizar su denuncia sin temor a represalias de ningún tipo. Es el deber de cada ciudadano colaborar con eliminar el fraude eléctrico, colaborando así con la mejora continua del servicio en diferentes sectores de toda nuestra área de concesión.</p>
   <p>Analizar las denuncias es una labor ardua, por favor, evitenos retrasos en las investigaciones con reportes que no son reales. Estas solicitudes son analizadas con estricta confidencialidad dentro de nuestra unidad corporativa, y luego remitidas a la institución correspondiente, por lo que garantizamos su anonimato para proteger su identidad.</p>

   <p>Por favor, complete esta solicitud para realizar su denuncia por correo electrónico.</p>
 </div>

 <div class="small-8 columns">

   <form action="#" class="es-site-masthead-form-fraude" data-abide>

     <div class="row">
       <fieldset class="small-6 columns">

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-form-fraude-input-nombre">
               Nombre completo <small>Requerido</small>
               <input id="es-site-form-fraude-input-nombre" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
             </label>
             <small class="error">Nombre no puede ser dejado en blanco</small>
           </div>
         </div>

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-fraude-input-nic">NIC <small>Requerido</small>
               <input id="es-site-fraude-input-nic" type="number" placeholder="Ingrese su NIC" required pattern="number"/>
             </label>
             <small class="error">NIC no puede ser dejado en blanco</small>
           </div>
         </div>

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-form-fraude-input-correo">
               Correo Electrónico <small>Requerido</small>
               <input id="es-site-form-fraude-input-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
             </label>
             <small class="error">Correo Electrónico no es valido</small>
           </div>
         </div>

       </fieldset>

       <fieldset class="small-6 columns end">

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-form-fraude-input-direcion">
               Dirección <small>Requerido</small>
               <input id="es-site-form-fraude-input-direcion" type="tel" placeholder="Ingrese su dirección" required pattern="alpha_numeric" />
             </label>
             <small class="error">Dirección no puede ser dejada en blanco</small>
           </div>
         </div>

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-form-fraude-input-direcion">
               Ciudad o Municipio &amp; Provincia <small>Requerido</small>
               <input id="es-site-form-fraude-input-direcion" type="tel" placeholder="Ingrese su Ciudad o Municipio" required pattern="alpha_numeric" />
             </label>
             <small class="error">Ciudad/Municipio &amp; Provincia no puede ser dejado en blanco</small>
           </div>
         </div>

         <div class="row">
           <div class="small-12 columns">
             <label for="es-site-form-fraude-input-telefono">
               Teléfono <small>Requerido</small>
               <input id="es-site-form-fraude-input-telefono" type="tel" placeholder="Ingrese su número teléfono" required pattern="number" />
             </label>
             <small class="error">Teléfono no puede ser dejado en blanco</small>
           </div>
         </div>

       </fieldset>
     </div>

     <div class="row">
       <fieldset class="small-12 columns">

         <div class="row">

           <div class="large-12 columns">
             <label for="es-site-form-fraude-input-textarea">Expliquenos el caso <small>Requerido</small></label>
             <textarea id="es-site-form-fraude-input-textarea" placeholder="Por favor describa el caso"  pattern="alpha_numeric"></textarea>
             <small class="error">Mensaje no puede ser dejado en blanco</small>
           </div>
         </div>

         <input class="button" type="submit" value="Realizar Denuncia" />

       </fieldset>
     </div>

   </form>

   <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 8 ); } ?>


 </div>
</div>