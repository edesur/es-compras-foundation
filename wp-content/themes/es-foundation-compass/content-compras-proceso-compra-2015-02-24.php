<section class="es-site-section-wrapper small-12 columns" data-equalizer>

  <div class="row">
   <div class="small-12 columns">
    <div class="row collapse">
      <div class="small-12 columns">
        <h3><?php
          if (is_post_type_archive()) {
            post_type_archive_title();
          } else {
            $tax_obj = $wp_query->get_queried_object();
            echo $tax_obj->name;
          }
         ?></h3>
      </div>
    </div>

    <div class="es-site-section-wrapper-background small-12 columns">
      <div class="row">

        <div class="small-7 columns push-5">
            <div class="es-site-file-adobe-reader-copy small-8 medium-8 large-9 columns">
              <small>Si no puede ver los documentos, es posible que no tenga instalado <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader">Adobe Reader</a>. Presione la imagen para instalarlo.</small>
            </div>
            <div class="es-site-file-icon-adobe-reader small-4 medium-4 large-3 columns">
              <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-get-adobe-reader.gif' ?>" alt="Descargue Adobe Reader"></a>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="small-12 columns">



          <ul class="es-site-compra-proceso-list small-block-grid-2" data-equalizer>

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>

               <?php

                $es_proceso_compra_fields = get_field_objects();

                $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];
                $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

                $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
                $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

                foreach ($es_proceso_compra_type_terms as $term) {
                  $es_proceso_compra_type = $term->name;
                }

                $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

                $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
                $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

                foreach ($es_proceso_compra_status_terms as $term) {
                  $es_proceso_compra_status = $term->name;
                }

                 switch ($es_proceso_compra_status) {

                   case 'En proceso':
                     $es_proceso_compra_status_class = 'label';
                     break;

                  case 'Enmendado':
                     $es_proceso_compra_status_class = 'label warning';
                     break;

                   case 'Adjudicado':
                     $es_proceso_compra_status_class = 'label success';
                     break;

                   case 'Desierto':
                     $es_proceso_compra_status_class = 'label alert';
                     break;

                   # default:
                     # code...
                     # break;
                 }


                ?>

               <?php if ($es_proceso_compra_id): ?>

                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns" data-equalizer-watch>
                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-th medium-2 columns text-center">
                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">
                         <h6>
                          <a href="<?php the_permalink(); ?>">
                           <?php the_title(); ?>
                          </a>
                         </h6>
                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>

                       </div>
                     </div>

                     <div class="row">
                       <div class="medium-2 columns text-center" style="padding: 0; margin-top: 0.25rem;">
                         <p>
                          <a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a>
                         </p>

                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">

                         <ul class="es-compra-proceso-type medium-block-grid-4">

                           <?php foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_value): ?>

                              <?php

                                if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                  $es_proceso_compra_type_class = 'label info';
                                } else {
                                  $es_proceso_compra_type_class = 'label secondary';
                                }

                               ?>
                             <li class="text-center">
                              <span class="<?php echo $es_proceso_compra_type_class; ?>"><?php echo $es_proceso_compra_type_value->name ?></span>
                             </li>
                           <?php endforeach ?>

                         </ul>

                       </div>
                     </div>
                   </div>

                 </li>

                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns" data-equalizer-watch>
                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-th medium-2 columns text-center">
                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">
                         <h6>
                          <a href="<?php the_permalink(); ?>">
                           <?php the_title(); ?>
                          </a>
                         </h6>
                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>

                       </div>
                     </div>

                     <div class="row">
                       <div class="medium-2 columns text-center" style="padding: 0; margin-top: 0.25rem;">
                         <p>
                          <a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a>
                         </p>

                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">

                         <ul class="es-compra-proceso-type medium-block-grid-4">

                           <?php /*foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_value):*/ ?>

                              <?php

                                if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                  $es_proceso_compra_type_class = 'label info';
                                } else {
                                  $es_proceso_compra_type_class = 'label secondary';
                                }

                               ?>
                             <li class="text-center">
                              <span class="label info"><?php echo $es_proceso_compra_type ?></span>
                             </li>
                           <?php /*endforeach*/ ?>

                         </ul>

                       </div>
                     </div>
                   </div>

                 </li>

               <?php endif ?>

             <?php endwhile; else: ?>
              <h3>
                No se encontraron
                <?php
                  if (is_post_type_archive()) {
                    post_type_archive_title();
                  } else {
                    $tax_obj = $wp_query->get_queried_object();
                    echo "Procesos de compra: " . $tax_obj->name;
                  }
                ?>
              </h3>
             <?php endif; ?>
          </ul>

          <ul class="es-site-compra-proceso-list small-block-grid-2" data-equalizer>

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>

               <?php

                $es_proceso_compra_fields = get_field_objects();

                $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];
                $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

                $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
                $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

                foreach ($es_proceso_compra_type_terms as $term) {
                  $es_proceso_compra_type = $term->name;
                }

                $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

                $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
                $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

                foreach ($es_proceso_compra_status_terms as $term) {
                  $es_proceso_compra_status = $term->name;
                }

                 switch ($es_proceso_compra_status) {

                   case 'En proceso':
                     $es_proceso_compra_status_class = 'label';
                     break;

                  case 'Enmendado':
                     $es_proceso_compra_status_class = 'label warning';
                     break;

                   case 'Adjudicado':
                     $es_proceso_compra_status_class = 'label success';
                     break;

                   case 'Desierto':
                     $es_proceso_compra_status_class = 'label alert';
                     break;

                   # default:
                     # code...
                     # break;
                 }


                ?>

               <?php if ($es_proceso_compra_id): ?>

                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns" data-equalizer-watch>
                     <!-- <div class="row">
                       <div class="medium-2 columns text-center" style="padding: 0; margin-top: 0.25rem;">
                         <p>
                          <a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a>
                         </p>

                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">
                         <h6>
                          <a href="<?php the_permalink(); ?>">
                           <?php the_title(); ?>
                          </a>
                         </h6>
                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>

                       </div>
                     </div> -->

                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-12 columns">
                          <h6>
                          <a href="<?php the_permalink(); ?>">
                           <?php the_title(); ?>
                          </a>
                         </h6>
                       </div>
                     </div>

                     <div class="row">
                      <div class="es-site-compra-proceso-list-single-wrapper-th medium-2 columns text-center">

                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>

                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">

                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>
                         <p><a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a></p>
                       </div>
                     </div>

                     <div class="row">
                       <div class="small-12 columns">
                         <ul class="es-compra-proceso-type medium-block-grid-5">

                           <?php foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_value): ?>

                              <?php

                                if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                  $es_proceso_compra_type_class = 'label info';
                                } else {
                                  $es_proceso_compra_type_class = 'label secondary';
                                }

                               ?>
                             <li class="text-center">
                              <span class="<?php echo $es_proceso_compra_type_class; ?>"><?php echo $es_proceso_compra_type_value->name ?></span>
                             </li>
                           <?php endforeach ?>

                         </ul>
                       </div>
                     </div>
                   </div>

                 </li>

                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns" data-equalizer-watch>

                    <div class="row">
                      <div class="es-site-compra-proceso-list-single-wrapper-copy medium-12 columns">
                        <h6>
                         <a href="<?php the_permalink(); ?>">
                          <?php the_title(); ?>
                         </a>
                        </h6>

                      </div>
                    </div>
                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-th medium-2 columns text-center">
                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">

                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>

                         <p>
                          <a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a>
                         </p>

                         <ul class="es-compra-proceso-type medium-block-grid-4">

                           <?php /*foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_value):*/ ?>

                              <?php

                                if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                  $es_proceso_compra_type_class = 'label info';
                                } else {
                                  $es_proceso_compra_type_class = 'label secondary';
                                }

                               ?>
                             <li class="text-center">
                              <span class="label info"><?php echo $es_proceso_compra_type ?></span>
                             </li>
                           <?php /*endforeach*/ ?>

                         </ul>

                       </div>
                     </div>

                     <div class="row">
                       <div class="medium-2 columns text-center" style="padding: 0; margin-top: 0.25rem;">


                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">



                       </div>
                     </div>
                   </div>

                 </li>

               <?php endif ?>

             <?php endwhile; else: ?>
              <h3>
                No se encontraron
                <?php
                  if (is_post_type_archive()) {
                    post_type_archive_title();
                  } else {
                    $tax_obj = $wp_query->get_queried_object();
                    echo "Procesos de compra: " . $tax_obj->name;
                  }
                ?>
              </h3>
             <?php endif; ?>
          </ul>

          <ul class="es-site-compra-proceso-list small-block-grid-2" data-equalizer>

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>

               <?php

                $es_proceso_compra_fields = get_field_objects();

                $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];
                $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

                $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
                $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

                foreach ($es_proceso_compra_type_terms as $term) {
                  $es_proceso_compra_type = $term->name;
                }

                $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

                $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
                $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

                foreach ($es_proceso_compra_status_terms as $term) {
                  $es_proceso_compra_status = $term->name;
                }

                 switch ($es_proceso_compra_status) {

                   case 'En proceso':
                     $es_proceso_compra_status_class = 'label';
                     break;

                  case 'Enmendado':
                     $es_proceso_compra_status_class = 'label warning';
                     break;

                   case 'Adjudicado':
                     $es_proceso_compra_status_class = 'label success';
                     break;

                   case 'Desierto':
                     $es_proceso_compra_status_class = 'label alert';
                     break;

                   # default:
                     # code...
                     # break;
                 }


                ?>

               <?php if ($es_proceso_compra_id): ?>



                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns" data-equalizer-watch>

                    <div class="row">
                      <div class="es-site-compra-proceso-list-single-wrapper-copy medium-12 columns">
                        <h6>
                         <a href="<?php the_permalink(); ?>">
                          <?php the_title(); ?>
                         </a>
                        </h6>

                      </div>
                    </div>
                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-th medium-2 columns text-center">
                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">

                         <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>

                         <p>
                          <a href="">
                            <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                          </a>
                         </p>

                         <ul class="es-compra-proceso-type medium-block-grid-4">

                           <?php /*foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_value):*/ ?>

                              <?php

                                if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                  $es_proceso_compra_type_class = 'label info';
                                } else {
                                  $es_proceso_compra_type_class = 'label secondary';
                                }

                               ?>
                             <li class="text-center">
                              <span class="label info"><?php echo $es_proceso_compra_type ?></span>
                             </li>
                           <?php /*endforeach*/ ?>

                         </ul>

                       </div>
                     </div>

                     <div class="row">
                       <div class="medium-2 columns text-center" style="padding: 0; margin-top: 0.25rem;">


                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy medium-10 columns">



                       </div>
                     </div>
                   </div>

                 </li>

               <?php endif ?>

             <?php endwhile; else: ?>
              <h3>
                No se encontraron
                <?php
                  if (is_post_type_archive()) {
                    post_type_archive_title();
                  } else {
                    $tax_obj = $wp_query->get_queried_object();
                    echo "Procesos de compra: " . $tax_obj->name;
                  }
                ?>
              </h3>
             <?php endif; ?>
          </ul>


        </div>
      </div>
    </div>
   </div>
  </div>
</section>