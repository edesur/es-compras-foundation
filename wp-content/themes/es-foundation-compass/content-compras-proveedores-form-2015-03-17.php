     <form action="#" data-abide>
       <div class="row">
         <fieldset class="small-12 medium-4 columns">
           <legend>Datos personales</legend>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-nombre">Nombre completo <small>Requerido</small>
                 <input id="es-site-form-proveedores-nombre" type="text" placeholder="Ingrese su nombre completo" required pattern="alpha" />
               </label>
               <small class="error">Nombre no puede ser dejado en blanco</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-cedula">
                 <span data-tooltip class="item has-tip tip-top" title="Ingrese los once dígitos de la cédula. Los guiones se muestran automáticamente">Cédula <small>Requerido</small>
                 </span>
                 <input id="es-site-form-proveedores-cedula" type="number" placeholder="Ingrese los once (11) dígitos de la cédula" required pattern="number" />
               </label>
               <small class="error">Cédula no puede ser dejado en blanco</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-correo">
                 <span data-tooltip class="item has-tip tip-top" title="Indique una cuenta de correo electrónico activa. Para completar el proceso de inscripción deberá responder un correo electrónico que se enviará a esta cuenta">Correo Electrónico <small>Requerido</small>
                 </span>
                 <input id="es-site-form-proveedores-correo" type="email" placeholder="Ingrese una cuenta de correo electrónico activa" required pattern="email" />
               </label>
               <small class="error">Correo Electrónico no es valido</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-correo-confirmar">
                 <span data-tooltip class="item has-tip tip-top" title="Indique una cuenta de correo electrónico activa. Para completar el proceso de inscripción deberá responder un correo electrónico que se enviará a esta cuenta">Confirmar Correo Electrónico <small>Requerido</small>
                 </span>
                 <input id="es-site-form-proveedores-correo-confirmar" type="email" placeholder="Confirme su cuenta de correo electrónico activa" required pattern="email" data-equalto="es-site-form-proveedores-correo" />
               </label>
               <small class="error">Correo Electrónico no coincide</small>
             </div>
           </div>

         </fieldset>

         <fieldset class="small-12 medium-4 columns end">
           <legend>Datos de la Empresa</legend>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-empresa">Empresa completo <small>Requerido</small>
                 <input id="es-site-form-proveedores-empresa" type="text" placeholder="Ingrese el nombre de su empresa" required pattern="alpha" />
               </label>
               <small class="error">Empresa no puede ser dejado en blanco</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-rnc">
                 <span data-tooltip class="item has-tip tip-top" title="Ingrese los once dígitos de la RNC. Los guiones se muestran automáticamente">RNC <small>Requerido</small>
                 </span>
                 <input id="es-site-form-proveedores-rnc" type="number" placeholder="Ingrese el RNC" required pattern="number" />
               </label>
               <small class="error">RNC no puede ser dejado en blanco</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-telefono">
                 <span data-tooltip class="item has-tip tip-top" title="Ingrese los diez dígitos de su teléfono, los guiones se muestran automáticamente">Teléfono <small>Requerido</small></span>
                 <input id="es-site-form-proveedores-telefono" type="tel" placeholder="Ingrese los dígitos de su teléfono" required pattern="number" />
               </label>
               <small class="error">Teléfono no puede ser dejado en blanco</small>
             </div>
           </div>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <label for="es-site-form-proveedores-provincia">
                 <span data-tooltip class="item has-tip tip-top" title="Ingrese la provincia donde esta localizado">Provincia <small>Requerido</small>
                 </span>
                 <input id="es-site-form-proveedores-provincia" type="text" placeholder="Ingrese la provincia" required pattern="alpha_numeric" />
               </label>
               <small class="error">Provincia no puede ser dejado en blanco</small>
             </div>
           </div>

         </fieldset>

         <fieldset class="small-12 medium-4 columns">
           <legend class="show-for-medium-up">Enviar Solicitud</legend>

           <div class="row">
             <div class="small-12 medium-11 columns">
               <p>Por favor, complete esta solicitud para recibir el formulario de registro & homologación de proveedores por correo electrónico.</p>
               <input class="button" type="submit" value="Enviar Solicitud" />
             </div>
           </div>

         </fieldset>

       </div>

     </form>

     <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 10 ); } ?>