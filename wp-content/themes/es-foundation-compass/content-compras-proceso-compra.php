<?php global $wp_query;

  $es_proceso_compra_obj = get_queried_object();
  $es_proceso_compra_fields = get_field_objects();

  $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
  // $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

  $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

  $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
  // $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

  $es_proceso_compra_status_terms_list = get_terms($es_proceso_compra_status_tax, array('hide_empty' => 1, 'post_status' => 'published'));
?>
<section class="es-site-section-wrapper small-12 columns" data-equalizer>

<div class="row">

  <div class="small-12 columns">
      <div data-alert class="alert-box secondary radius text-center">
        <h3 style="color:#fff; background: rgb(0, 45, 98); padding: 8px; border-bottom: 3px solid #f68934;">COMUNICADO</h3>
        <p style="margin: 16px;">Informamos a todos nuestros suplidores de Bienes y Servicios,  que nuestros almacenes estarán cerrados por motivos de inventario  de fin de año 2017, desde el 15 hasta el 28 de noviembre de 2017 y la fecha límite para la entrega de materiales será hasta el viernes 10 de noviembre de 2017.</p>
        <p style="margin: 16px;">Las operaciones de entrega y recepción de mercancía continuarán de forma ordinaria en nuestros almacenes a partir del 1ero de diciembre del 2017.</p>

      </div>
  </div>

</div>


<div class="row" data-equalizer>
  <div class="large-4 small-12 columns" data-equalizer-watch>
      <div data-alert class="alert-box radius secondary">
        <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
        <h5 style="color:rgb(0, 45, 98);">LPN-BID-01-2017, Adquisición de camiones, para habilitar oficinas móviles</h5>
    <p>Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)</p>
        <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/wp-content/uploads/2017/04/es-site-cdeee-licitacion-lpn-bdi-01-2017-camion.pdf" title="Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)">Mas detalles</a>
      </div>
  </div>

    <div class="large-4 small-12 columns" data-equalizer-watch>
        <div data-alert class="alert-box radius secondary">
          <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
          <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-PU-2017-003</h5>
          <p>Licitación Pública Nacional de plazos reducidos para la Contratación de los Servicios de Gestión Técnico Comercial en za Zona de concesión de EDESUR Dominicana, S.A.</p>
          <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/procesos-de-compras/licitacion-publica-nacional-de-plazos-reducidos-para-la-contratacion-de-los-servicios-de-gestion-tecnico-comercial-en-la-zona-de-concesion-de-edesur-dominicana-s-a-edesur-ccc-pu-201/" title="EDESUR-CCC-PU-2017-003, Licitación Pública Nacional de plazos reducidos para la “Contratación de los Servicios de Gestión Técnico Comercial en za Zona de concesión de EDESUR Dominicana, S.A.”">Mas detalles</a>
        </div>
    </div>

      <div class="large-4 small-12 columns" data-equalizer-watch>
          <div data-alert class="alert-box radius secondary">
            <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
            <h5 style="color:rgb(0, 45, 98);">EDESUR-CCC-LPN-2017-023</h5>
            <p>Rehabilitación y Normalización de Clientes Circuitos: CSAT102, PALA102, HANU101 y HANU102, GRBO102, GRBO102, Santo Domingo y Municipios Nigua y Hatillo, Provincia San Cristóbal</p>
            <a style="margin-bottom: 0;" class="button primary small" href="http://edesur2.edesur.com.do/procesos-de-compras/edesur-ccc-lpn-2017-023-para-la-contratacion-rehabilitacion-y-normalizacion-de-clientes-de-los-circuitos-csat102-pala102-hanu101-y-hanu102-grbo102-nigua-y-grbo102-hatillo-provinci/" title="EDESUR-CCC-LPN-2017-023, Rehabilitación y Normalización de Clientes de los Circuitos: CSAT102, PALA102, HANU101 y HANU102, GRBO102, GRBO102, Provincia Santo Domingo y Municipios Nigua y Hatillo, Provincia San Cristóbal">Mas detalles</a>
          </div>
      </div>
    </div>

  <?php
  /*
  <div class="row">
    <div class="large-6 small-12 columns">
      <div data-alert class="alert-box radius">
        <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
        <h5 style="color:#fff;">Adquisición de camiones, para habilitar oficinas móviles</h5>
        <p style="color:#fff;">Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)</p>
        <a style="margin-bottom: 0;" href="http://edesur2.edesur.com.do/wp-content/uploads/2017/04/es-site-cdeee-licitacion-lpn-bdi-01-2017-camion.pdf" title="Adquisición de cuatro (4) camiones tipo carga, sin cama, para habilitar oficinas móviles de gestion de cobros (1 Edesur, 1 Edeeste y 2 Edenorte)">Mas detalles</a>
      </div>
    </div>

    <div class="large-6 small-12 columns">
        <div data-alert class="alert-box radius">
          <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
          <h5 style="color:#fff;">Licitación Pública Nacional de Plazos Reducidos No. EDESUR-CCC-PU-2016-004, para la Contratación de Servicios de Gestión - Técnico Comercial en la Zona de Concesión de EDESUR DOMINICANA, S.A.</h5>

          <a style="margin-bottom: 0;" href="//edesur2.edesur.com.do/procesos-de-compras/edesur-ccc-pu-2016-004-contratacion-de-servicios-de-gestion-tecnico-comercial-en-la-zona-de-concesion-de-edesur-dominicana-s-a/" class="button primary info small" title="Licitación Pública Nacional de Plazos Reducidos No. EDESUR-CCC-PU-2016-004, para la Contratación de Servicios de Gestión - Técnico Comercial en la Zona de Concesión de EDESUR DOMINICANA, S.A.">Mas detalles</a>
        </div>
    </div>


    </div>

    <div class="row">
      <div class="large-6 small-12 columns">
          <div data-alert class="alert-box radius">
            <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
            <h5 style="color:#fff;">Licitación Pública Nacional de Plazos Reducidos No. EDESUR-CCC-PU-2016-004, para la Contratación de Servicios de Gestión - Técnico Comercial en la Zona de Concesión de EDESUR DOMINICANA, S.A.</h5>

            <a style="margin-bottom: 0;" href="//edesur2.edesur.com.do/procesos-de-compras/edesur-ccc-pu-2016-004-contratacion-de-servicios-de-gestion-tecnico-comercial-en-la-zona-de-concesion-de-edesur-dominicana-s-a/" class="button primary info small" title="Licitación Pública Nacional de Plazos Reducidos No. EDESUR-CCC-PU-2016-004, para la Contratación de Servicios de Gestión - Técnico Comercial en la Zona de Concesión de EDESUR DOMINICANA, S.A.">Mas detalles</a>
          </div>
      </div>
        <div class="large-6 small-12 columns">
            <div data-alert class="alert-box radius">
              <h4 class="text-center" style="color:#fff; background: rgb(0, 45, 98); padding: 8px;">Licitación Pública Nacional</h4>
              <h5 style="color:#fff;">Rehabilitación y normalización de clientes del circuito pala-101</h5>
          <p style="color:#fff;">Rehabilitación y normalización de clientes del circuito pala-101, provincia santo domingo, en la zona de concesión de Edesur Dominicana, S.A. EDESUR-CCC-LPN-2017-001</p>
              <a style="margin-bottom: 0;" href="http://edesur2.edesur.com.do/procesos-de-compras/rehabilitacion-y-normalizacion-de-clientes-del-circuito-pala-101-provincia-santo-domingo-en-la-zona-de-concesion-de-edesur-dominicana-s-a-edesur-ccc-lpn-2017-001/" title="Rehabilitación y normalización de clientes del circuito pala-101, provincia santo domingo, en la zona de concesión de Edesur Dominicana, S.A. EDESUR-CCC-LPN-2017-001">Mas detalles</a>
            </div>
        </div>
      </div>

  */

  ?>


  <div class="row medium-collapse">
    <div class="small-12 columns">
      <h3 class="es-site-section-wrapper-bg-title"><?php
        if (is_post_type_archive()) {
          post_type_archive_title();
        } else {
          echo $es_proceso_compra_obj->name;
        }
       ?></h3>
    </div>
  </div>

  <div class="row">
    <div class="es-site-section-wrapper-background small-12 columns">

    <div class="es-site-compra-proceso-filter-buttons row">
      <div class="large-7 columns">
        <h5>Procesos por modalidad</h5>
        <ul class="button-group radius stack-for-small">

          <?php foreach ($es_proceso_compra_type_terms_list as $es_proceso_compra_type_term): ?>

            <?php $es_proceso_compra_type_term_link = get_term_link($es_proceso_compra_type_term); ?>
            <li>
              <a href="<?php echo esc_url( $es_proceso_compra_type_term_link ); ?>" class="button tiny secondary">
              <?php echo $es_proceso_compra_type_term->name; ?>
              </a>
            </li>
          <?php endforeach ?>

        </ul>
      </div>
      <div class="large-5 columns">
        <h5>Procesos por estado</h5>
        <ul class="button-group radius stack-for-small">

          <?php foreach ($es_proceso_compra_status_terms_list as $es_proceso_compra_status_term): ?>

            <?php $es_proceso_compra_status_term_link = get_term_link($es_proceso_compra_status_term); ?>
            <li>
              <a href="<?php echo esc_url( $es_proceso_compra_status_term_link ); ?>" class="button tiny secondary">
              <?php echo $es_proceso_compra_status_term->name; ?>
              </a>
            </li>

          <?php endforeach ?>

        </ul>
      </div>
    </div>




      <div class="row">

        <div class="es-site-adobe-reader-download small-12 medium-7 columns push-5 show-for-medium-up">
            <div class="es-site-file-adobe-reader-copy small-8 medium-8 large-9 columns">
              <small>Si no puede ver los documentos, es posible que no tenga instalado <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader">Adobe Reader</a>. Presione la imagen para instalarlo.</small>
            </div>
            <div class="es-site-file-icon-adobe-reader small-4 medium-4 large-3 columns">
              <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-get-adobe-reader.gif' ?>" alt="Descargue Adobe Reader"></a>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="small-12 columns">

          <ul class="es-site-compra-proceso-list small-block-grid-1 medium-block-grid-2">

            <?php
              $es_proceso_compra_args = array(
                'post_type' => 'proceso_compra',
                'orderby'   => 'modified',
                'order'     => 'DESC',
                'paged' => get_query_var( 'paged' )
                // 'posts_per_page'=> -1
              );

              $es_proceso_compra_query = new WP_Query($es_proceso_compra_args)
            ?>

            <?php if ( have_posts() ) : while ( $es_proceso_compra_query->have_posts() ) : $es_proceso_compra_query->the_post() ?>

               <?php

                // $es_proceso_compra_fields = get_field_objects();

                $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];
                $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

                $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
                $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

                $es_proceso_compra_files = $es_proceso_compra_fields['es_compras_proceso_compra_files']['name'];

                foreach ($es_proceso_compra_fields['es_compras_proceso_compra_files']['value'] as $es_proceso_compra_file) {

                  $es_proceso_compra_single_file_type = $es_proceso_compra_file['es_compras_proceso_compra_file_type'];

                  switch ($es_proceso_compra_single_file_type) {
                    case 'Pliego':
                      $es_proceso_compra_type_doc = $es_proceso_compra_single_file_type;
                      break;

                  case 'Adjuntos':
                    $es_proceso_compra_type_attachment = $es_proceso_compra_single_file_type;
                    break;

                  case 'Enmiendas/Adendas':
                      $es_proceso_compra_type_amendment = $es_proceso_compra_single_file_type;
                      break;

                  case 'Preguntas y respuestas':
                      $es_proceso_compra_type_faq = $es_proceso_compra_single_file_type;
                      break;

                      case 'Carta adjudicataria':
                      $es_proceso_compra_type_allocation = $es_proceso_compra_single_file_type;
                      break;

                      case 'Acto de cancelación':
                      $es_proceso_compra_type_cancelation = $es_proceso_compra_single_file_type;
                      break;

                    default:
                      # code...
                      break;
                  }
                }

                foreach ($es_proceso_compra_type_terms as $term) {
                  $es_proceso_compra_type = $term->name;
                }

                // $es_proceso_compra_type_terms_list = get_terms($es_proceso_compra_type_tax, array('hide_empty' => 0));

                $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
                $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

                foreach ($es_proceso_compra_status_terms as $term) {
                  $es_proceso_compra_status = $term->name;
                }

                 switch ($es_proceso_compra_status) {

                   case 'En proceso':
                     $es_proceso_compra_status_class = 'label';
                     break;

                  case 'Enmendado':
                     $es_proceso_compra_status_class = 'label warning';
                     break;

                   case 'Adjudicado':
                     $es_proceso_compra_status_class = 'label success';
                     break;

                   case 'Desierto':
                     $es_proceso_compra_status_class = 'label alert';
                     break;

                   # default:
                     # code...
                     # break;
                 }


                ?>



               <?php if ($es_proceso_compra_id): ?>



                 <li class="es-site-compra-proceso-list-single">
                   <div class="es-site-compra-proceso-list-single-wrapper small-12 columns">

                    <div class="row">
                      <div class="es-site-compra-proceso-list-single-wrapper-copy medium-12 columns">
                        <h6>
                         <a href="<?php the_permalink(); ?>">
                          <?php the_title(); ?>
                         </a>
                        </h6>

                      </div>
                    </div>
                     <div class="row">
                       <div class="es-site-compra-proceso-list-single-wrapper-th small-3 medium-2 columns text-center">
                         <a href="<?php the_permalink(); ?>" class="th">
                           <img src="<?php echo get_template_directory_uri() . '/stylesheets/images/es-compras-proceso-pliego-icon.jpg' ?>"/>
                         </a>
                       </div>
                       <div class="es-site-compra-proceso-list-single-wrapper-copy small-9 medium-10 columns">

                         <div class="row collapse">
                           <p class="es-site-compra-proceso-list-single-id"><?php echo $es_proceso_compra_id_name; ?></p>
                         </div>

                         <div class="row collapse">
                           <div class="small-12 medium-9 columns">
                             <p>
                                <span class="<?php echo $es_proceso_compra_status_class; ?>"><?php echo $es_proceso_compra_status; ?></span>
                             </p>

                             <ul class="es-compra-proceso-type medium-block-grid-4">

                                  <?php

                                    // if ($es_proceso_compra_type_value->name == $es_proceso_compra_type) {
                                    //   $es_proceso_compra_type_class = 'label info';
                                    // } else {
                                    //   $es_proceso_compra_type_class = 'label secondary';
                                    // }

                                   ?>
                                 <li class="text-center">
                                  <span class="label info"><?php echo $es_proceso_compra_type ?></span>
                                 </li>
                               <?php /*endforeach*/ ?>

                             </ul>
                           </div>

                           <?php while( have_rows($es_proceso_compra_files) ): the_row();

                             $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                             $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];

                             $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );



                             $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);


                           ?>


                           <?php endwhile; ?>

                           <div class="es-site-compra-proceso-list-single-download small-3 columns end show-for-medium-up">
                             <a class="es-site-compra-proceso-list-single-download-button button tiny info expand" href="<?php the_permalink(); ?>" class="button tiny info">Mas Detalles</a>

                           </div>


                         </div>

                       </div>

                     </div>

                   </div>

                 </li>

               <?php endif ?>



             <?php endwhile; ?>

             <?php wp_reset_query(); ?>

             </ul>

             <?php

             $big = 999999999; // need an unlikely integer

             echo paginate_links( array(
               'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
               'format' => '?paged=%#%',
               'current' => max( 1, get_query_var('paged') ),
               'total' => $wp_query->max_num_pages,
               'prev_text'          => __('« Anterior'),
               'next_text'          => __('Siguiente »')
             ) );

             ?>

           <?php   else: ?>
              <h3>
                No se encontraron
                <?php
                  if (is_post_type_archive()) {
                    post_type_archive_title();
                  } else {
                    $es_proceso_compra_obj = $wp_query->get_queried_object();
                    echo "Procesos de compra: " . $es_proceso_compra_obj->name;
                  }
                ?>
              </h3>
             <?php endif; ?>



        </div>
      </div>


    </div>
  </div>
</section>