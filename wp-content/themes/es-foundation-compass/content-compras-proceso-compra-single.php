<section class="es-site-section-wrapper small-12 columns" >

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>

    <?php

      $es_proceso_compra_fields = get_field_objects();

      $es_proceso_compra_id = $es_proceso_compra_fields['es_compras_proceso_compra_id'];

      $es_proceso_compra_id_name = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_id']['name']);

      $es_proceso_compra_type_tax = $es_proceso_compra_fields['es_compras_proceso_compra_type_tax']['taxonomy'];
      $es_proceso_compra_type_terms = get_the_terms($post->ID, $es_proceso_compra_type_tax);

      foreach ($es_proceso_compra_type_terms as $term) {
        $es_proceso_compra_type = $term->name;
      }

      $es_proceso_compra_status_tax = $es_proceso_compra_fields['es_compras_proceso_compra_status_tax']['taxonomy'];
      $es_proceso_compra_status_terms = get_the_terms($post->ID, $es_proceso_compra_status_tax);

      foreach ($es_proceso_compra_status_terms as $term) {
        $es_proceso_compra_status = $term->name;
      }


      $es_proceso_compra_desc = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_description']['name'] );
      $es_proceso_compra_contact = get_field( $es_proceso_compra_fields['es_compras_proceso_compra_contact']['name'] );

      $es_proceso_compra_files = $es_proceso_compra_fields['es_compras_proceso_compra_files']['name'];

      $es_proceso_compra_allocation_obj = get_field_object('es_compras_proceso_compra_allocation_letter');


      // $es_proceso_compra_date_published = DateTime::createFromFormat('Ymd', get_field('es_compras_proceso_compra_date_published'));
      $es_proceso_compra_date_published = get_field('es_compras_proceso_compra_date_published');

      $es_compras_proceso_compra_period = $es_proceso_compra_fields['es_compras_proceso_compra_period']['name'];

      foreach ($es_proceso_compra_fields['es_compras_proceso_compra_files']['value'] as $es_proceso_compra_file) {

        $es_proceso_compra_single_file_type = $es_proceso_compra_file['es_compras_proceso_compra_file_type'];

        switch ($es_proceso_compra_single_file_type) {
          case 'Pliego':
            $es_proceso_compra_type_doc = $es_proceso_compra_single_file_type;
            break;

        case 'Adjuntos':
          $es_proceso_compra_type_attachment = $es_proceso_compra_single_file_type;
          break;

        case 'Enmiendas/Adendas':
            $es_proceso_compra_type_amendment = $es_proceso_compra_single_file_type;
            break;

        case 'Prórroga':
            $es_proceso_compra_type_extension = $es_proceso_compra_single_file_type;
            break;

        case 'Preguntas':
            $es_proceso_compra_type_faq = $es_proceso_compra_single_file_type;
            break;

        case 'Carta adjudicataria':
            $es_proceso_compra_type_allocation = $es_proceso_compra_single_file_type;
            break;

        case 'Acto de desestimación':
        $es_proceso_compra_type_cancelation = $es_proceso_compra_single_file_type;
            break;

          default:
            # code...
            break;
        }
      }

      switch ($es_proceso_compra_status) {

        case 'En proceso':
          $es_proceso_compra_status_class = 'es_proceso_compra_status_label button expand small radius';
          $es_proceso_compra_status_tab = "";
          break;

       case 'Enmendado':
          $es_proceso_compra_status_id = 'es_proceso_compra_status_label button expand small radius warning';
          $es_proceso_compra_status_class = 'es_proceso_compra_status_label button expand small radius warning';
          $es_proceso_compra_status_tab = "Enmiendas/Adendas";
          break;

        case 'Adjudicado':
          $es_proceso_compra_status_class = 'es_proceso_compra_status_label button expand small radius success';
          $es_proceso_compra_status_tab = "Adjudicación";
          break;

        case 'Desierto':
          $es_proceso_compra_status_class = 'es_proceso_compra_status_label button expand small radius alert';
          $es_proceso_compra_status_tab = "Desestimación";
          break;

      }

     ?>

    <div class="row">
     <div class="small-12 columns">
       <div class="row collapse">
         <div class="small-12 columns">
           <h3><?php the_title(); ?></h3>
         </div>
       </div>

     <div class="es-site-compra-proceso-single es-site-section-wrapper-background small-12 columns">

       <div class="row">

        <?php if ($es_proceso_compra_id): ?>

          <div class="small-12 large-3 columns large-push-9 small-only-text-center">


            <span class="<?php echo $es_proceso_compra_status_class; ?>">
            <?php echo $es_proceso_compra_status; ?>
            </span>

            <div class="row">
              <div class="medium-6 large-12 columns">
                <ul class="no-bullet">
                  <li>
                    <h5><?php echo $es_proceso_compra_id_name; ?></h5>
                  </li>

                  <?php $cnt = 0;
                        $max = 1;
                  ?>


                  <?php while( have_rows($es_proceso_compra_files) ): the_row();

                    // $es_proceso_compra_date_allocation = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_allocation'));
                    $es_proceso_compra_date_allocation = get_sub_field( 'es_compras_proceso_compra_date_allocation');
                    // $es_proceso_compra_date_cancel = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_cancel' ));
                    $es_proceso_compra_date_cancel = get_sub_field( 'es_compras_proceso_compra_date_cancel' );
                    ?>

                    <?php if ($es_proceso_compra_status == 'Adjudicado' && $es_proceso_compra_date_allocation): ?>


                      <?php while( $cnt < 1 ): ?>
                        <li>Fecha de adjudicación: <?php echo $es_proceso_compra_date_allocation;
                         ?></li>
                        <?php $cnt++; ?>
                      <?php endwhile; ?>

                    <?php elseif ($es_proceso_compra_status == 'Desierto' && $es_proceso_compra_date_cancel): ?>
                      <li>Fecha de desestimación: <?php echo $es_proceso_compra_date_cancel; ?></li>

                    <?php endif ?>

                  <?php endwhile; ?>

                </ul>
              </div>
              <div class="medium-6 large-12 columns hide-for-small">
                <div class="row">
                  <div class="es-site-file-adobe-reader-copy medium-12 columns">
                    <small>Si no puede ver los documentos, es posible que no tenga instalado <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader">Adobe Reader</a>. Presione la imagen para instalarlo.</small>
                  </div>
                  <div class="es-site-file-icon-adobe-reader medium-12 columns large-text-right">
                    <a href="http://get.adobe.com/es/reader/" target="_blank" title="Descargue Adobe Reader"><img src="<?php echo get_template_directory_uri() . '/images/es-site-icon-get-adobe-reader.gif' ?>" alt="Descargue Adobe Reader"></a>
                  </div>
                </div>
              </div>
            </div>



            <div class="row hide-for-small">

            </div>

          </div>

          <div class="small-12 large-9 large-pull-3 columns">

            <ul class="es-site-compra-proceso-single-tabs tabs radius" data-tab>

            <!-- <pre><?php var_dump($es_proceso_compra_type_doc)?></pre> -->

            

              <?php if (isset($es_proceso_compra_type_doc)): ?>
                <li class="tab-title active"><a class="button small" href="#es_proceso_compra_type_doc_id"><?php  echo $es_proceso_compra_type_doc; ?></a></li>
              <?php else: ?>
              <li class="tab-title active"><a class="button small" href="#es_proceso_compra_type_doc_id">Detalles</a></li>
              <?php endif ?>
              

              <?php if (isset($es_proceso_compra_type_attachment)): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_type_attachment_id"><?php  echo $es_proceso_compra_type_attachment; ?></a></li>
              <?php endif ?>

              <?php if (isset($es_proceso_compra_type_faq)): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_type_faq_id"><?php  echo $es_proceso_compra_type_faq; ?></a></li>
              <?php endif ?>

              <?php if (isset($es_proceso_compra_type_extension)): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_type_extension_id"><?php  echo $es_proceso_compra_type_extension; ?></a></li>
              <?php endif ?>

              <?php /*if (isset($es_proceso_compra_type_amendment) && $es_proceso_compra_status != 'En proceso'): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_type_amendment_id"><?php  echo $es_proceso_compra_type_amendment; ?></a></li>
              <?php endif*/ ?>

              <?php if (isset($es_proceso_compra_type_amendment) && ($es_proceso_compra_status == 'Enmendado' || $es_proceso_compra_status == 'En proceso')): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_type_amendment_id"><?php  echo $es_proceso_compra_type_amendment; ?></a></li>
              <?php endif ?>



              <?php if ($es_proceso_compra_status != "En proceso" && $es_proceso_compra_status != "Enmendado"): ?>
                <li class="tab-title"><a class="button small" href="#es_proceso_compra_status_id">
                  <?php echo $es_proceso_compra_status_tab; ?>
                </a></li>
              <?php endif ?>

            </ul>

            <div class="es-site-compra-proceso-tabs-content tabs-content">
              <div class="es-proceso-compra-type-doc content active" id="es_proceso_compra_type_doc_id" >

                <div class="row">
                  <div class="large-3 columns">
                    <h5>Código de proceso</h5>
                  </div>
                  <div class="large-9 columns">
                    <p><?php echo $es_proceso_compra_id_name; ?></p>
                  </div>
                </div>

                <div class="row">
                  <div class="large-3 columns">
                    <h5>Descripción detallada</h5>
                  </div>
                  <div class="large-9 columns">

                    <p><?php echo $es_proceso_compra_desc ?></p>
                  </div>
                </div>

                <div class="row">
                  <div class="large-3 columns">
                    <h5>Fecha de publicación</h5>
                  </div>
                  <div class="large-9 columns">
                    <p>
                      <?php echo $es_proceso_compra_date_published; ?>
                    </p>
                  </div>
                </div>

                <div class="row">

                  <?php while( have_rows($es_compras_proceso_compra_period) ): the_row();

                  // $es_proceso_compra_period_begins = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_period_begins'));
                  $es_proceso_compra_period_begins = get_sub_field( 'es_compras_proceso_compra_period_begins');
                  $es_proceso_compra_period_ends = get_sub_field( 'es_compras_proceso_compra_period_ends' ); ?>


                  <?php if ($es_proceso_compra_period_begins ): ?>
                    <div class="large-3 columns">
                      <h5>Recepción de ofertas</h5>
                    </div>
                    <div class="large-2 columns">

                      <p><?php echo $es_proceso_compra_period_begins; ?></p>
                    </div>
                  <?php endif ?>

                  <?php if ($es_proceso_compra_period_ends ): ?>
                    <div class="large-2 columns"><h5>Hasta</h5></div>
                    <div class="large-5 columns">
                      <p><?php echo $es_proceso_compra_period_ends; ?></p>
                    </div>
                  <?php endif ?>


                <?php endwhile; ?>
                </div>


                <?php $cnt = 0; ?>
                <?php while( have_rows($es_proceso_compra_files) ): the_row();

                  $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );
                  // $es_proceso_compra_date_allocation = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_allocation'));
                  $es_proceso_compra_date_allocation = get_sub_field( 'es_compras_proceso_compra_date_allocation');
                  // $es_proceso_compra_date_cancel = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_cancel' ));
                  $es_proceso_compra_date_cancel =  get_sub_field( 'es_compras_proceso_compra_date_cancel' );

                  // $es_proceso_compra_date_extension = get_sub_field( 'es_compras_proceso_compra_date_extension' );


                  $es_proceso_compra_documents = $es_proceso_compra_fields['es_compras_proceso_compra_files']['value'];
                  ?>


                  <?php if (isset($es_proceso_compra_type_extension) && $es_proceso_compra_type_extension == $es_proceso_compra_file_type): ?>

                    <?php
                      // get repeater field data
                      $repeater = get_field($es_proceso_compra_files);

                      // vars
                      $order = array();

                      // populate order
                      foreach( $repeater as $i => $row ) {

                        $order[ $i ] = $row['es_compras_proceso_compra_date_extension'];

                        // echo '<pre>' . var_dump($row['es_compras_proceso_compra_date_extension']) . '</pre>';
                      }

                      // multisort
                      array_multisort( $order, SORT_DESC, $repeater );
                    ?>

                    <ul>

                      <?php foreach( $repeater as $i => $row ): ?>

                        <!-- <li>Here: <?php /*echo $row['es_compras_proceso_compra_date_extension'];*/ ?></li> -->

                      <?php endforeach; ?>

                      <li>Here: <?php echo the_sub_field('es_compras_proceso_compra_date_extension'); ?></li>

                      </ul>

                    <pre><?php /*var_dump($repeater);*/ ?></pre>

                    <?php
                    // $es_proceso_compra_date_extension = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_extension' ));
                    $es_proceso_compra_date_extension = get_sub_field( 'es_compras_proceso_compra_date_extension' );
                    ?>


                    <?php while( $cnt < 1 ): ?>

                      <div class="row">
                        <div class="large-3 columns">
                          <h5>Fecha de prórroga</h5>
                        </div>
                        <div class="large-9 columns">
                          <p>
                            <?php echo $es_proceso_compra_date_extension; ?>
                          </p>
                        </div>
                      </div>

                      <?php $cnt++; ?>

                    <?php endwhile; ?>

                    


                  <?php elseif ($es_proceso_compra_status == 'Adjudicado' && $es_proceso_compra_date_allocation ): ?>

                    <?php while( $cnt < 1 ): ?>

                      <div class="row">
                        <div class="large-3 columns">
                          <h5>Fecha de adjudicación</h5>
                        </div>
                        <div class="large-9 columns">
                          <p>
                            <?php echo $es_proceso_compra_date_allocation; ?>
                          </p>
                        </div>
                      </div>
                      <?php $cnt++; ?>

                    <?php endwhile; ?>

                  <?php elseif ($es_proceso_compra_status == 'Desierto' && $es_proceso_compra_date_cancel): ?>
                    <div class="row">
                      <div class="large-3 columns">
                        <h5>Fecha de desestimación</h5>
                      </div>
                      <div class="large-9 columns">
                        <p>
                          <?php echo $es_proceso_compra_date_cancel; ?>
                        </p>
                      </div>
                    </div>
                  <?php endif ?>

                <?php endwhile; ?>

                <div class="row">
                  <div class="large-3 columns">
                    <h5>Contacto</h5>
                  </div>
                  <div class="large-9 columns">
                    <p><?php echo $es_proceso_compra_contact ?></p>
                  </div>
                </div>

                <div class="row">
                  <div class="large-3 columns">
                    <h5>Email</h5>
                  </div>
                  <div class="large-9 columns">
                    <?php if( have_rows('es_compras_proceso_compra_email') ): ?>

                      <ul class="inline-list">

                      <?php while( have_rows('es_compras_proceso_compra_email') ): the_row();

                        $es_proceso_compra_contact_email_address = get_sub_field( 'es_compras_proceso_compra_email_address' );
                        ?>

                        <li >
                          <a class="button small secondary" href="mailto:<?php echo antispambot($es_proceso_compra_contact_email_address ); ?>"><?php echo antispambot($es_proceso_compra_contact_email_address ); ?></a>
                        </li>

                      <?php endwhile; ?>

                      </ul>

                      <ul class="es-site-compra-proceso-list small-block-grid-1" >
                        <?php while( have_rows($es_proceso_compra_files) ): the_row();

                          $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                          $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];

                          $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );



                          $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);


                        ?>


                          <?php if (isset($es_proceso_compra_type_doc) && 
                          ($es_proceso_compra_file_type == $es_proceso_compra_type_doc)): ?>

                            <li class="es-proceso-compra-file-single" >
                                <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button small info">Descargar</a>

                            </li>

                          <?php endif ?>

                        <?php endwhile; ?>
                      </ul>

                    <?php endif; ?>

                  </div>
                </div>


              </div>
              <div class="content" id="es_proceso_compra_type_attachment_id" >

                <?php if( have_rows($es_proceso_compra_files) ): ?>
                  <ul class="es-site-compra-proceso-list small-block-grid-1" >
                    <?php while( have_rows($es_proceso_compra_files) ): the_row();

                      $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                      $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
                      $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
                      $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                      $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

                      $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                      $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

                      $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');
                    ?>


                      <?php if (
                      (isset($es_proceso_compra_type_doc) && $es_proceso_compra_file_type == $es_proceso_compra_type_doc) ||
                      ($es_proceso_compra_type_attachment && $es_proceso_compra_file_type == $es_proceso_compra_type_attachment) ||
                      (isset($es_proceso_compra_type_faq) && $es_proceso_compra_file_type == $es_proceso_compra_type_faq) ||
                      (isset($es_proceso_compra_type_extension) && $es_proceso_compra_file_type == $es_proceso_compra_type_extension) ||
                      (( isset($es_proceso_compra_type_amendment) && $es_proceso_compra_file_type == $es_proceso_compra_type_amendment) && ($es_proceso_compra_status == 'En proceso' || $es_proceso_compra_status == 'Enmendado')) ||
                      ((isset($es_proceso_compra_type_allocation) && $es_proceso_compra_file_type == $es_proceso_compra_type_allocation && $es_proceso_compra_status == 'Adjudicado') || isset($es_proceso_compra_type_cancelation) && ($es_proceso_compra_file_type == $es_proceso_compra_type_cancelation && $es_proceso_compra_status == 'Desierto'))
                      ): ?>

                        <li class="es-proceso-compra-file-single" >

                          <div class="row">
                            <div class="small-12 columns">
                              <h6><?php echo $es_proceso_compra_file_link; ?></h6>
                            </div>
                          </div>

                          <div class="row">
                            <div class="es-proceso-compra-file-single-icon small-3 medium-1 columns">
                              <a href="<?php echo $es_proceso_compra_file_url; ?>" class="th">
                                <img src="<?php echo $es_proceso_compra_file_single_icon; ?>" alt="">
                              </a>
                            </div>
                            <div class="small-9 medium-11 columns">
                              <div class="row collapse">
                                <div class="small-6 medium-4 columns">
                                  <p><span class="label secondary"><?php echo $es_proceso_compra_file_type; ?></span></p>
                                </div>

                                <div class="small-6 medium-4 columns show-for-medium-up">
                                  <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button tiny info">Descargar</a>
                                </div>
                              </div>


                            </div>
                          </div>

                        </li>

                      <?php endif ?>

                    <?php endwhile; ?>
                  </ul>



                <?php endif; ?>

              </div>
              <div class="content" id="es_proceso_compra_type_faq_id" >

                <?php if( have_rows($es_proceso_compra_files) ): ?>
                  <ul class="es-site-compra-proceso-list small-block-grid-1" >
                    <?php while( have_rows($es_proceso_compra_files) ): the_row();

                      $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                      $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
                      $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
                      $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                      $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

                      $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                      $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

                      $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');
                    ?>


                      <?php if (isset($es_proceso_compra_type_faq) && ($es_proceso_compra_file_type == $es_proceso_compra_type_faq) ): ?>

                        <li class="es-proceso-compra-file-single" >

                          <div class="row">
                            <div class="small-12 columns">
                              <h6><?php echo $es_proceso_compra_file_link; ?></h6>
                            </div>
                          </div>

                          <div class="row">
                            <div class="es-proceso-compra-file-single-icon small-3 medium-1 columns">
                              <a href="<?php echo $es_proceso_compra_file_url; ?>" class="th">
                                <img src="<?php echo $es_proceso_compra_file_single_icon; ?>" alt="">
                              </a>
                            </div>
                            <div class="small-9 medium-11 columns">
                              <div class="row collapse">
                                <div class="small-6 medium-4 columns">
                                  <p><span class="label secondary"><?php echo $es_proceso_compra_file_type; ?></span></p>
                                </div>

                                <div class="small-6 medium-4 columns show-for-medium-up">
                                  <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button tiny info">Descargar</a>
                                </div>
                              </div>


                            </div>
                          </div>

                        </li>

                      <?php endif ?>

                    <?php endwhile; ?>
                  </ul>

                <?php endif; ?>

              </div>

              <div class="content" id="es_proceso_compra_type_extension_id" >

                <?php if( have_rows($es_proceso_compra_files) ): ?>
                  <ul class="es-site-compra-proceso-list small-block-grid-1" >
                    <?php while( have_rows($es_proceso_compra_files) ): the_row();

                      $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                      $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
                      $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
                      $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                      // $es_proceso_compra_date_extension = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_extension' ));
                      $es_proceso_compra_date_extension = get_sub_field( 'es_compras_proceso_compra_date_extension');

                      $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

                      $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                      $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

                      $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');
                    ?>

                      <?php if ( isset($es_proceso_compra_type_extension) && ($es_proceso_compra_file_type == $es_proceso_compra_type_extension) ): ?>

                        <li class="es-proceso-compra-file-single" >

                          <div class="row">
                            <div class="small-12 columns">
                              <h6><?php echo $es_proceso_compra_file_link; ?></h6>
                            </div>
                          </div>

                          <div class="row">
                            <div class="es-proceso-compra-file-single-icon small-3 medium-1 columns">
                              <a href="<?php echo $es_proceso_compra_file_url; ?>" class="th">
                                <img src="<?php echo $es_proceso_compra_file_single_icon; ?>" alt="">
                              </a>
                            </div>
                            <div class="small-9 medium-11 columns">
                              <div class="row collapse">
                                <div class="small-6 medium-4 columns">
                                  <p><span class="label secondary"><?php echo $es_proceso_compra_file_type; ?></span></p>
                                </div>

                                <div class="small-6 medium-4 columns show-for-medium-up">
                                  <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button tiny info">Descargar</a>
                                </div>
                              </div>

                              <?php if ($es_proceso_compra_date_extension): ?>
                                <div class="row collapse">

                                  <div class="small-12">
                                    <ul class="es-proceso-compra-file-single-date inline-list">
                                      <li class="show-for-medium-up">Fecha de prórroga:</li>
                                      <li><?php echo $es_proceso_compra_date_extension;?></li>
                                    </ul>
                                  </div>
                                </div>
                              <?php endif ?>


                            </div>
                          </div>

                        </li>

                      <?php endif ?>

                    <?php endwhile; ?>
                  </ul>

                <?php endif; ?>

              </div>

              <div class="content" id="es_proceso_compra_type_amendment_id" >

                <?php if( have_rows($es_proceso_compra_files) ): ?>
                  <ul class="es-site-compra-proceso-list small-block-grid-1" >
                    <?php while( have_rows($es_proceso_compra_files) ): the_row();

                      $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                      $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
                      $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
                      $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                      $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

                      $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                      $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

                      $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');
                    ?>


                      <?php if ( (isset($es_proceso_compra_type_amendment) && ($es_proceso_compra_file_type == $es_proceso_compra_type_amendment) ) && ($es_proceso_compra_status == 'En proceso' || $es_proceso_compra_status == 'Enmendado') ): ?>

                        <li class="es-proceso-compra-file-single" >

                          <div class="row">
                            <div class="small-12 columns">
                              <h6><?php echo $es_proceso_compra_file_link; ?></h6>
                            </div>
                          </div>

                          <div class="row">
                            <div class="es-proceso-compra-file-single-icon small-3 medium-1 columns">
                              <a href="<?php echo $es_proceso_compra_file_url; ?>" class="th">
                                <img src="<?php echo $es_proceso_compra_file_single_icon; ?>" alt="">
                              </a>
                            </div>
                            <div class="small-9 medium-11 columns">
                              <div class="row collapse">
                                <div class="small-6 medium-4 columns">
                                  <p><span class="label secondary"><?php echo $es_proceso_compra_file_type; ?></span></p>
                                </div>

                                <div class="small-6 medium-4 columns show-for-medium-up">
                                  <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button tiny info">Descargar</a>
                                </div>
                              </div>


                            </div>
                          </div>

                        </li>

                      <?php endif ?>

                    <?php endwhile; ?>
                  </ul>

                <?php endif; ?>

              </div>
              <div class="content" id="es_proceso_compra_status_id" >

                <?php if( have_rows($es_proceso_compra_files) ): ?>

                  <ul class="es-site-compra-proceso-list small-block-grid-1" >
                    <?php while( have_rows($es_proceso_compra_files) ): the_row();

                      $es_proceso_compra_file_single = get_sub_field( 'es_compras_proceso_compra_file_single' );
                      $es_proceso_compra_file_single_id = $es_proceso_compra_file_single['id'];
                      $es_proceso_compra_file_single_title = $es_proceso_compra_file_single['title'];
                      $es_proceso_compra_file_type = get_sub_field( 'es_compras_proceso_compra_file_type' );

                      // $es_proceso_compra_date_allocation = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_allocation'));
                      $es_proceso_compra_date_allocation =  get_sub_field( 'es_compras_proceso_compra_date_allocation');

                      // $es_proceso_compra_date_cancel = DateTime::createFromFormat('Ymd', get_sub_field( 'es_compras_proceso_compra_date_cancel' ));
                      $es_proceso_compra_date_cancel = get_sub_field( 'es_compras_proceso_compra_date_cancel');

                      $es_proceso_compra_file_single_icon = wp_mime_type_icon(get_post_mime_type($es_proceso_compra_file_single_id));

                      $es_proceso_compra_file_url = wp_get_attachment_url($es_proceso_compra_file_single_id);

                      $es_proceso_compra_file_link = wp_get_attachment_link($es_proceso_compra_file_single_id, '', false, true, $es_proceso_compra_file_single_title);

                      $es_proceso_compra_file_icon_link = wp_get_attachment_link($es_proceso_compra_file_single_id,'', false, false, '<img src="' .  $es_proceso_compra_file_single_icon . '" alt="">');
                       ?>

                       <?php if (
                        (isset($es_proceso_compra_type_allocation) && $es_proceso_compra_file_type == $es_proceso_compra_type_allocation && $es_proceso_compra_status == 'Adjudicado') || isset($es_proceso_compra_type_cancelation) && ($es_proceso_compra_file_type == $es_proceso_compra_type_cancelation && $es_proceso_compra_status == 'Desierto')):
                        ?>

                          <li class="es-proceso-compra-file-single" >

                            <div class="row">
                              <div class="small-12 columns">
                                <h6><?php echo $es_proceso_compra_file_link; ?></h6>
                              </div>
                            </div>

                            <div class="row">
                              <div class="es-proceso-compra-file-single-icon small-3 medium-1 columns">
                                <a href="<?php echo $es_proceso_compra_file_url; ?>" class="th">
                                  <img src="<?php echo $es_proceso_compra_file_single_icon; ?>" alt="">
                                </a>
                              </div>
                              <div class="small-9 medium-11 columns">
                                <div class="row collapse">
                                  <div class="small-6 medium-4 columns">
                                    <p><span class="label secondary"><?php echo $es_proceso_compra_file_type; ?></span></p>
                                  </div>

                                  <div class="small-6 medium-4 columns show-for-medium-up">
                                    <a href="<?php echo $es_proceso_compra_file_url; ?>" class="button tiny info">Descargar</a>
                                  </div>
                                </div>


                                  <div class="row collapse">

                                    <div class="small-12">
                                      <ul class="es-proceso-compra-file-single-date inline-list">

                                        <?php if ($es_proceso_compra_date_allocation): ?>
                                          <li class="show-for-medium-up">Fecha de adjudicación:</li>
                                          <li><?php echo $es_proceso_compra_date_allocation;?></li>
                                        <?php elseif ($es_proceso_compra_date_cancel): ?>
                                          <li class="show-for-medium-up">Fecha de desestimación:</li>
                                          <li><?php echo $es_proceso_compra_date_cancel;?></li>
                                        <?php endif ?>
                                      </ul>
                                    </div>
                                  </div>


                              </div>
                            </div>

                          </li>
                       <?php endif; ?>

                    <?php endwhile; ?>
                  </ul>

                <?php endif; ?>

              </div>

            </div>

          </div>

        <?php endif ?>


         </div>
       </div>
     </div>
    </div>

  <?php endwhile; else: ?>


  <?php endif; ?>
</section>