<section class="es-site-section-wrapper small-12 medium-12 large-12 columns">

  <div class="row">
    <div class="es-site-form-confirmation es-site-section-wrapper-background small-12 medium-12 columns text-center">

      <div class="row">
        <div class="small-12 columns">
          <h5 class="subheader">Muchas gracias por haber actualizado sus datos, esto nos permitirá brindarle un servicio más personalizado.</h5>
          <h4>Si desea realizar su pago en este momento, puede hacerlo a través de cualquiera de estos dos canales.</h4>
        </div>
      </div>

      <div class="row">
        <div class="small-12 medium-4 columns medium-offset-2">
          <a href="" class="es-site-form-confirmation-button-ov button large expand"><img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-pago-factura-form.png' ?>"> <span>Oficina Virtual</span></a>
        </div>
        <div class="small-12 medium-4 columns medium-pull-2">
        <a class="button large expand info show-for-small-only" href="tel://+18096839393">
          <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center-form.png' ?>"> Call Center
        </a>
        <a  class="es-site-form-confirmation-button-cc button large expand info show-for-medium-up" href="tel://+18096839393" data-reveal-id="es-call-center-promo">
        <img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center-form.png' ?>"> (809) 683-9393
        </a>
          <!-- <a href="" class="button large expand info"><img src="<?php echo get_template_directory_uri() . '/images/es-site-app-icon-call-center-form.png' ?>"> <span>CALL CENTER</span></a> -->
        </div>
      </div>

      <div class="row">
        <div class="small-12 columns">
          <h4>Para estar conectado e informado, le invitamos a seguirnos en nuestras redes sociales</h4>
        </div>
      </div>

      <div class="row">
        <div class="small-12 medium-4 columns large-offset-4">
          <ul class="small-block-grid-2 medium-block-grid-6">
            <li><a href="https://www.facebook.com/EdesurDominicana" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-facebook.png' ?>">
            </a></li>
            <li><a href="https://twitter.com/EdesurRD" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-twitter.png' ?>">
            </a></li>
            <li><a href="https://www.instagram.com/edesurrd/" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-instagram.png' ?>">
            </a></li>
            <li><a href="https://www.youtube.com/user/EdesurDominicana" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-youtube.png' ?>">
            </a></li>
            <li><a href="https://www.pinterest.com/edesurrd" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-pinterest.png' ?>">
            </a></li>
            <li><a href="https://foursquare.com/v/edesur/4c27ca3997d00f474eec3eea" class="button large" target="_blank">
              <img src="<?php echo get_template_directory_uri() . '/images/es-site-social-icon-foursquare.png' ?>">
            </a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>

</section>